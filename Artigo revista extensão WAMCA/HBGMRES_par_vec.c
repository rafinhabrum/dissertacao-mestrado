 /*Algoritmo pseudocodigo HBGMRES
função HBGMRES(Matriz de coeficientes: A; Vetor de valores independentes: b; Chute inicial: x0)
  $l$ ← 1
  enquanto solução $x_m$ não convergir faça
  	$r_0$ ← $b - A \times x_0$
  	$e_1$ ← $\|r_0\|_2$
  	$v_1$ ← $\frac{r_0}{e_1}$
  	tol ← $e_1$
  	para j ← 1 até m faça
  		$w_j$ ← $A \times v_j$
  		para i ← 1 até j faça
  			$h_{i,j}$ ← $ \langle w_j, v_i \rangle $
  			$w_j$ ← $w_j - h_{i,j} \times v_i$
  		$h_{j+1,j}$ ← $\|w_j\|_2$
  		$v_{j+1}$ ← $\frac{w_j}{h_{j+1,j}}$
  		para k ← 1 até j−1 faça
  			aux ← $h_{k,j}$
  			$h_{k,j}$ ← $c_k \times \textnormal{aux} + s_k \times h_{k+1,j}$
  			$h_{k+1,j}$ ← $-s_k \times \textnormal{aux} + c_k \times h_{k+1,j}$
  		$ra_j$ ← $\sqrt{h_{j,j}^2 + h_{j+1,j}^2}$
  		$c_j$ ← $\frac{h_{j,j}}{ra_j}$
  		$s_j$ ← $\frac{h_{j+1,j}}{ra_j}$
  		$h_{j,j}$ ← $ra_j$
  		$h_{j+1,j}$ ← 0
  		$e_{j+1}$ ← $-s_j \times e_j$
  		$e_j$ ← $c_j \times e_j$
  		tol ← $|e_j|$
  		se tol < TOLERANCIA então
  			m ← j
  			//dar break
  	se l igual a 1 então
  		$d$ ← $x_0^{l}$
  	senão
		$d$ ← $x_0^{l} - x_0^{l-1}$
	$\tilde{d}$ ← $V_m^T \times d$
    $g$ ← $d - V_m \times \tilde{d}$
    se g != 0 então
		$\tilde{v}_{m+1}$ ← $\frac{g}{\|g\|_2}$
		$\tilde{V}_{m+1}$ ← $[V_k$  $\tilde{v}_{m+1}]$
		$a$ ← $A \times \tilde{v}_{m+1}$
		$\hat{a}$ ← $a$
  		para i ← 1 até m+1 faça
			$h_{i,m+1}$ ← $ \langle \hat{a}, v_i \rangle $
			$\hat{a}$ ← $\hat{a} - h_{i,m+1} \times v_i$
  		$h_{m+2,m+1}$ ← $\|\hat{a}\|_2$
		$he$ ← $a - \tilde{V}_{m+1} \times \hat{a}$
		se he != 0 então
			para k ← 1 até m faça
				aux ← $h_{k,{m+1}}$
				$h_{k,{m+1}}$ ← $c_k \times \textnormal{aux} + s_k \times h_{k+1,{m+1}}$
				$h_{k+1,{m+1}}$ ← $-s_k \times \textnormal{aux} + c_k \times h_{k+1,{m+1}}$
			$r_{m+1}$ ← $\sqrt{h_{{m+1},{m+1}}^2 + h_{{m+2},{m+1}}^2}$
			$c_{m+1}$ ← $\frac{h_{{m+1},{m+1}}}{r_{m+1}}$
			$s_{m+1}$ ← $\frac{h_{{m+2},{m+1}}}{r_{m+1}}$
			$h_{{m+1},{m+1}}$ ← $ra_j$
			$h_{{m+2},{m+1}}$ ← 0
			$e_{{m+2}}$ ← $-s_{m+1} \times e_{m+1}$
			$e_{m+1}$ ← $c_{m+1} \times e_{m+1}$
  		para j ← m+1 até 1 passo −1 faça
			$y_j$ ← $\frac{e_j - \sum_{t = j+1}^{m+1} (h_{j,t} \times y_t)}{h_{j,j}}$
		$x_m$ ← $x_0 + \tilde{V}_{m+1} \times y$
	senão
		para j ← m até 1 passo −1 faça
			$y_j$ ← $\frac{e_j - \sum_{t = j+1}^{m} (h_{j,t} \times y_t)}{h_{j,j}}$
		$x_m$ ← $x_0 + V_m \times y$
    $x_0^{l+1}$ ← $x_m$
    $l$ ← $l+1$
  retorna $x_m$
*/

/* Formato Compressed Sparse Row - CSR (p. 113 do Saad)
Usa 3 arrays para armazenar a matriz esparsa:
-> o AA para os valores numéricos (não-nulos) da matriz A na ordem por linha, os valores da primeira
linha seguidos dos valores da segunda linha... (a princípio do tipo double) -> tamanho Nz (qtde de valores não nulos)
-> o JA para armazenar o índice da coluna de cada um dos valores de AA (sempre do tipo inteiro) -> tamanho Nz
-> o IA, de tamanho N+1, para armazenar onde cada linha da matriz começa nos dois arrays acima. A
posição N+1 aponta para Nz+1 (sempre do tipo inteiro)
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mytime.h"

//#define TOLERANCIA 1e-20
#define TOLERANCIA 1e-15

double produtoInternoVetorial(double *a, double *b, int size);
void produtoMatrizEsparsaVetor(double *AA, int *IA, int *JA, double *X, double **Y); //Y e alterado dentro da funcao
double normaVetorial(double *vec, int size);
void imprimirVetorD(double *, int size, char *nome);
void imprimirVetorI(int *a, int size, char *nome);
void resolverSistemaTriangular(double *H, int rowsH, int colsH, double *vec, int size, double **vecSol); //vecSol e alterado dentro da funcao
void HBGMRES(double *AA, int *IA, int *JA, double *b, double *x0_atual, double **x_sol); //x_sol e alterado dentro da funcao

int nz, nmatriz, max_iter;

double tempo;

int main(){
    double *AA;
    int *JA, *IA;
    double *B, *x0, *x_sol;

    int i;

    scanf("%d %d %d\n", &nmatriz, &nz, &max_iter);
    max_iter--;

    AA = (double *)malloc(nz*sizeof(double));
    JA = (int *)malloc(nz*sizeof(int));
    IA = (int *)malloc((nmatriz+1)*sizeof(int));
    B = (double *)malloc(nmatriz*sizeof(double));
    x0 = (double *)calloc(nmatriz,sizeof(double));
    x_sol = (double *)calloc(nmatriz,sizeof(double));

    if((!AA) || (!JA) || (!IA) || (!B) || (!x0) || (!x_sol))
    {
		fprintf(stderr, "Erro ao alocar\n");
		exit(2);
	}

    for(i = 0; i < nz; ++i)
    {
        scanf("%lg\n", (AA+i));
    }
    for(i = 0; i < nz; ++i)
    {
        scanf("%d\n", (JA+i));
    }
    for(i = 0; i <= nmatriz; ++i)
    {
        scanf("%d\n", (IA+i));
    }
    for(i = 0; i < nmatriz; ++i)
    {
        scanf("%lg\n", (B+i));
    }

    HBGMRES(AA, IA, JA, B, x0, &x_sol);

    //imprimirVetorD(x_sol, nmatriz, "solução");

    printf("%lf s;\n", tempo);

    free(AA);
    free(JA);
    free(IA);
    free(B);
    free(x0);
    free(x_sol);

    return 0;
}

#pragma omp declare simd
double produtoInternoVetorial(double *a, double *b, int size)
{
    double result = 0;
    for(int i = 0; i < size; ++i)
    {
        result += ((*(a+i))*(*(b+i)));
    }
    return result;
}

void imprimirVetorD(double *a, int size, char *nome)
{
	printf("Vetor %s\n", nome);
    for(int i = 0; i < size; ++i)
    {
        printf("%lg\n", *(a+i));
    }
    printf("\n");
}

void imprimirVetorI(int *a, int size, char *nome)
{
	printf("Vetor %s\n", nome);
    for(int i = 0; i < size; ++i)
    {
        printf("%d\n", *(a+i));
    }
    printf("\n");
}

void produtoMatrizEsparsaVetor(double *AA, int *IA, int *JA, double *X, double **Y) //Y e alterado dentro da funcao
{
    int k1, k2, i;
    int j, aux_j;
    double auxAA[nmatriz], auxX[nmatriz];

	#pragma omp parallel for private(auxAA, auxX, j, k1, k2, aux_j)
	for(i = 0; i < nmatriz; ++i)
	{
		k1 = *(IA+i);
		k2 = *(IA+i+1);
		for(j = k1, aux_j = 0; j < k2; ++j, ++aux_j)
		{
			*(auxAA+aux_j) = *(AA+j);
			*(auxX+aux_j) = *(X+(*(JA+j)));
		}
		*((*Y)+i) = produtoInternoVetorial(auxAA, auxX, k2-k1);
	}
}

double normaVetorial(double *vec, int size)
{
	int i;
	double norma = 0;
	for(i = 0; i < size; ++i)
	{
		norma += ((*(vec+i))*(*(vec+i)));
	}
	norma = sqrt(norma);
	return norma;
}

void resolverSistemaTriangular(double *H, int rowsH, int colsH, double *vec, int size, double **vecSol) //vecSol e alterado dentro da funcao
{
    int i, j;

    for(i = size; i >= 0; --i)
    {
        *((*vecSol)+i) = *(vec+i);
        for(j = i+1; j <= size; ++j)
        {
            *((*vecSol)+i) -= ((*(H+(i*colsH+j))) * (*((*vecSol)+j)));
        }
		*((*vecSol)+i) /= *(H+(i*colsH+i));
    }
}

void HBGMRES(double *AA, int *IA, int *JA, double *b, double *x0_atual, double **x_sol) //x_sol e alterado dentro da funcao
{
	double *r0, *e, *w, *c, *s;
	double *x0_anterior, *d, *dTil, *g;
	double *a, *aHat, *he;
	double tol, ra, aux;
	double *Vt;
	double *V;
	double *H;
	double *y, *b_aux;
	int rowsH, colsH, rowsV, colsV, rowsVt, colsVt;
	int convergiu = 0;
	int count;

	int i, j, k, i_Aux, m = max_iter;

	r0 = (double *)malloc(nmatriz*sizeof(double));
	e = (double *)malloc((max_iter+3)*sizeof(double));
	w = (double *)malloc(nmatriz*sizeof(double));
	c = (double *)malloc((max_iter+2)*sizeof(double));
	s = (double *)malloc((max_iter+2)*sizeof(double));
	x0_anterior = (double *)malloc(nmatriz*sizeof(double));
	d = (double *)malloc(nmatriz*sizeof(double));
	dTil = (double *)malloc((max_iter+1)*sizeof(double));
	g = (double *)malloc(nmatriz*sizeof(double));
	a = (double *)malloc(nmatriz*sizeof(double));
	aHat = (double *)malloc(nmatriz*sizeof(double));
	he = (double *)malloc(nmatriz*sizeof(double));
	rowsVt = max_iter+2;
	colsVt = nmatriz;
	Vt = (double *)malloc(rowsVt*colsVt*sizeof(double));
	rowsV = nmatriz;
	colsV = max_iter+2;
	V = (double *)malloc(rowsV*colsV*sizeof(double));
	rowsH = max_iter+3;
	colsH = max_iter+2;
	H = (double *)malloc(rowsH*colsH*sizeof(double));
	y = (double *)malloc((max_iter+2)*sizeof(double));
	b_aux = (double *)malloc(nmatriz*sizeof(double));
	int iter = 1;
	int l = 1;

	if((!r0) || (!e) || (!w) || (!c) || (!s) || (!Vt) || (!V) || (!H) || (!y) || (!b_aux) || (!x0_anterior) || (!d) || (!dTil) || (!g) || (!a) || (!aHat) || (!he))
	{
		fprintf(stderr, "Erro ao alocar\n");
		exit(2);
	}

	//fprintf(stderr, "sizeof double = %ld\n", sizeof(double));
	tic(&tempo, TIME_s);

	while(!convergiu)
	{
		m = max_iter;
		//fprintf(stderr, "iter = %d\n", iter);
		produtoMatrizEsparsaVetor(AA, IA, JA, x0_atual, &r0);
		for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			*(r0+i_Aux) *= (-1);
			*(r0+i_Aux) += *(b+i_Aux);
		}
		*e = normaVetorial(r0, nmatriz);
		for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			*(Vt+i_Aux) = *(V+(i_Aux*colsV)) = *(r0+i_Aux)/(*e);
		}
		tol = *e;
		for(j = 0; j <= max_iter; ++j)
		{
			produtoMatrizEsparsaVetor(AA, IA, JA, (Vt+(j*colsVt)), &w);
			for(i = 0; i <= j; ++i)
			{
				*(H+(i*colsH+j)) = produtoInternoVetorial(w, (Vt+(i*colsVt)), nmatriz);
				for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
				{
					*(w+i_Aux) = *(w+i_Aux) - (*(H+(i*colsH+j))) * (*(Vt+(i*colsVt+i_Aux)));
				}
			}
			*(H+((j+1)*colsH+j)) = normaVetorial(w, nmatriz);
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*(Vt+((j+1)*colsVt+i_Aux)) = *(V+(i_Aux*colsV+(j+1))) = (*(w+i_Aux))/(*(H+((j+1)*colsH+j)));
			}

			for(k = 0; k < j; ++k)
			{
				aux = *(H+(k*colsH+j));
				*(H+(k*colsH+j)) = (*(c+k))*aux + (*(s+k))*(*(H+((k+1)*colsH+j)));
				*(H+((k+1)*colsH+j)) = (-(*(s+k)))*aux + (*(c+k))*(*(H+((k+1)*colsH+j)));
			}
			ra = sqrt(((*(H+(j*colsH+j)))*(*(H+(j*colsH+j)))) + ((*(H+((j+1)*colsH+j)))*(*(H+((j+1)*colsH+j)))));
			*(c+j) = (*(H+(j*colsH+j)))/ra;
			*(s+j) = (*(H+((j+1)*colsH+j)))/ra;
			*(H+(j*colsH+j)) = ra;
			*(H+((j+1)*colsH+j)) = 0;
			*(e+(j+1)) = -(*(s+j))*(*(e+j));
			*(e+j) = (*(c+j))*(*(e+j));
			tol = fabs(*(e+j+1));
			if(tol < TOLERANCIA)
			{
				m = j;
				break;
			}
		}
		if(l == 1)
		{
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*(d+i_Aux) = *(x0_atual+i_Aux);
			}
			l++;
		}
		else
		{
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*(d+i_Aux) = *(x0_atual+i_Aux) - *(x0_anterior+i_Aux);
			}
		}
		for(i_Aux = 0; i_Aux <= m; ++i_Aux)
		{
			*(dTil+i_Aux) = produtoInternoVetorial((Vt+(i_Aux*colsVt)), d, nmatriz+1);
		}
		for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			*(g+i_Aux) = *(d+i_Aux) - produtoInternoVetorial((V+(i_Aux*colsV)), dTil, m+1);
		}
		double norma = normaVetorial(g, nmatriz);
		if(fabs(norma) > TOLERANCIA)
		{
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*(Vt+((m+1)*colsVt+i_Aux)) = *(V+(i_Aux*colsV+(m+1))) = (*(g+i_Aux))/norma;
			}
			produtoMatrizEsparsaVetor(AA, IA, JA, (Vt+((m+1)*colsVt)), &a);
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*(aHat+i_Aux) = *(a+i_Aux);
			}
			for(i = 0; i <= (m+1); ++i)
			{
				*(H+(i*colsH+(m+1))) = produtoInternoVetorial(aHat, (Vt+(i*colsVt)), nmatriz);
				for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
				{
					*(aHat+i_Aux) = *(aHat+i_Aux) - (*(H+(i*colsH+(m+1)))) * (*(Vt+(i*colsVt+i_Aux)));
				}
			}
			*(H+((m+2)*colsH+(m+1))) = normaVetorial(aHat, nmatriz);
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*(he+i_Aux) = *(a+i_Aux) - produtoInternoVetorial((V+(i_Aux*colsV)), aHat, m+2);
			}
			if(fabs(normaVetorial(he, nmatriz)) > TOLERANCIA)
			{
				for(k = 0; k <= m; ++k)
				{
					aux = *(H+(k*colsH+(m+1)));
					*(H+(k*colsH+(m+1))) = (*(c+k))*aux + (*(s+k))*(*(H+((k+1)*colsH+(m+1))));
					*(H+((k+1)*colsH+(m+1))) = (-(*(s+k)))*aux + (*(c+k))*(*(H+((k+1)*colsH+(m+1))));
				}
				ra = sqrt(((*(H+((m+1)*colsH+(m+1))))*(*(H+((m+1)*colsH+(m+1))))) + (*(H+(((m+2)*colsH+(m+1))))*(*(H+(((m+2)*colsH+(m+1)))))));
				*(c+(m+1)) = (*(H+((m+1)*colsH+(m+1))))/ra;
				*(s+(m+1)) = (*(H+((m+2)*colsH+(m+1))))/ra;
				*(H+((m+1)*colsH+(m+1))) = ra;
				*(H+((m+2)*colsH+(m+1))) = 0;
				*(e+(m+2)) = -(*(s+(m+1)))*(*(e+(m+1)));
				*(e+(m+1)) = (*(c+(m+1)))*(*(e+(m+1)));
			}
			resolverSistemaTriangular(H, rowsH, colsH, e, m+1, &y);
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*((*x_sol) + i_Aux) = *(x0_atual + i_Aux) + produtoInternoVetorial((V+(i_Aux*colsV)), y, m+2);
			}
		}
		else
		{
			resolverSistemaTriangular(H, rowsH, colsH, e, m, &y);
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*((*x_sol) + i_Aux) = *(x0_atual + i_Aux) + produtoInternoVetorial((V+(i_Aux*colsV)), y, m+1);
			}
		}
		for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			*(x0_anterior + i_Aux) = *(x0_atual + i_Aux);
			*(x0_atual + i_Aux) = *((*x_sol) + i_Aux);
		}

		count = 0;
		produtoMatrizEsparsaVetor(AA, IA, JA, (*x_sol), &b_aux);
		for (i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			if((fabs((*(b_aux+i_Aux)) - (*(b+i_Aux))) < TOLERANCIA) && (fabs((*(b+i_Aux)) - (*(b_aux+i_Aux))) < TOLERANCIA))
			{
				count++;
			}
			*(b_aux+i_Aux) -= *(b+i_Aux);
		}
		//imprimirVetorD(b_aux, nmatriz, "b_aux");
		double norma_b_aux = normaVetorial(b_aux, nmatriz);
		//printf("normaVetorial = %.32lf\n", norma_b_aux);
		if((count == nmatriz) || (norma_b_aux < TOLERANCIA))
		{
			convergiu = 1;
		}
		if(iter == 3000)
		{
			//printf("Limite de iteracoes\n");
			break;
		}
		iter++;
	}
	//printf("count = %d\n", count);
	//printf("iter = %d\n m = %d\n", iter, m);

	tac(&tempo, TIME_s);

	free(r0);
	free(e);
	free(w);
	free(c);
	free(s);
	free(x0_anterior);
	free(d);
	free(dTil);
	free(g);
	free(a);
	free(aHat);
	free(he);
	free(Vt);
	free(V);
	free(H);
	free(y);
	free(b_aux);
}
