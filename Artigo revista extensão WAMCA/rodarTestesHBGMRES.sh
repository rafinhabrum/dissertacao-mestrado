#!/bin/bash
gcc mytime.c HBGMRES.c -o HBGMRES -lm -O2
gcc mytime.c HBGMRES_par.c -o HBGMRES_par -lm -O2 -fopenmp
gcc mytime.c HBGMRES_par_static.c -o HBGMRES_par_static -lm -O2 -fopenmp
gcc mytime.c HBGMRES_par_dinamic.c -o HBGMRES_par_dinamic -lm -O2 -fopenmp
gcc mytime.c HBGMRES_vec.c -o HBGMRES_vec -lm -O2 -fopenmp -ftree-vectorize -mavx
gcc mytime.c HBGMRES_par_vec.c -o HBGMRES_par_vec -lm -O2 -fopenmp -ftree-vectorize -mavx
gcc mytime.c HBGMRES_par_static_vec.c -o HBGMRES_par_static_vec -lm -O2 -fopenmp -ftree-vectorize -mavx
gcc mytime.c HBGMRES_par_dinamic_vec.c -o HBGMRES_par_dinamic_vec -lm -O2 -fopenmp -ftree-vectorize -mavx
echo "Trocou o teste - add20"
echo "HBGMRES" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < add20.dat >> tempos_add20.dat
done
echo "Trocou o teste - cavity05"
echo "HBGMRES" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < cavity05.dat >> tempos_cavity05.dat
done
echo "Trocou o teste - cavity10"
echo "HBGMRES" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < cavity10.dat >> tempos_cavity10.dat
done
echo "Trocou o teste - circuit_2"
echo "HBGMRES" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < circuit_2.dat >> tempos_circuit_2.dat
done
echo "Trocou o teste - comsol"
echo "HBGMRES" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < comsol.dat >> tempos_comsol.dat
done
echo "Trocou o teste - flowmeter5"
echo "HBGMRES" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "Trocou o teste - fpga_trans_01"
echo "HBGMRES" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "Trocou o teste - fpga_trans_02"
echo "HBGMRES" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "Trocou o teste - raefsky1"
echo "HBGMRES" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < raefsky1.dat >> tempos_raefsky1.dat
done
echo "Trocou o teste - raefsky2"
echo "HBGMRES" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < raefsky2.dat >> tempos_raefsky2.dat
done
echo "Trocou o teste - sherman4"
echo "HBGMRES" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < sherman4.dat >> tempos_sherman4.dat
done
echo "Trocou o teste - chipcool0"
echo "HBGMRES" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < chipcool0.dat >> tempos_chipcool0.dat
done
echo "Trocou o teste - memplus"
echo "HBGMRES" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < memplus.dat >> tempos_memplus.dat
done
echo "Trocou o teste - wang3"
echo "HBGMRES" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < wang3.dat >> tempos_wang3.dat
done
echo "Trocou o teste - matrix-new_3"
echo "HBGMRES" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
./HBGMRES < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
echo "Trocou o teste - add20"
echo "HBGMRES_par" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < add20.dat >> tempos_add20.dat
done
echo "Trocou o teste - cavity05"
echo "HBGMRES_par" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < cavity05.dat >> tempos_cavity05.dat
done
echo "Trocou o teste - cavity10"
echo "HBGMRES_par" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < cavity10.dat >> tempos_cavity10.dat
done
echo "Trocou o teste - circuit_2"
echo "HBGMRES_par" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < circuit_2.dat >> tempos_circuit_2.dat
done
echo "Trocou o teste - comsol"
echo "HBGMRES_par" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < comsol.dat >> tempos_comsol.dat
done
echo "Trocou o teste - flowmeter5"
echo "HBGMRES_par" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "Trocou o teste - fpga_trans_01"
echo "HBGMRES_par" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "Trocou o teste - fpga_trans_02"
echo "HBGMRES_par" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "Trocou o teste - raefsky1"
echo "HBGMRES_par" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < raefsky1.dat >> tempos_raefsky1.dat
done
echo "Trocou o teste - raefsky2"
echo "HBGMRES_par" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < raefsky2.dat >> tempos_raefsky2.dat
done
echo "Trocou o teste - sherman4"
echo "HBGMRES_par" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < sherman4.dat >> tempos_sherman4.dat
done
echo "Trocou o teste - chipcool0"
echo "HBGMRES_par" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < chipcool0.dat >> tempos_chipcool0.dat
done
echo "Trocou o teste - memplus"
echo "HBGMRES_par" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < memplus.dat >> tempos_memplus.dat
done
echo "Trocou o teste - wang3"
echo "HBGMRES_par" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < wang3.dat >> tempos_wang3.dat
done
echo "Trocou o teste - matrix-new_3"
echo "HBGMRES_par" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
echo "Trocou o teste - add20"
echo "HBGMRES_par_static" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < add20.dat >> tempos_add20.dat
done
echo "Trocou o teste - cavity05"
echo "HBGMRES_par_static" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < cavity05.dat >> tempos_cavity05.dat
done
echo "Trocou o teste - cavity10"
echo "HBGMRES_par_static" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < cavity10.dat >> tempos_cavity10.dat
done
echo "Trocou o teste - circuit_2"
echo "HBGMRES_par_static" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < circuit_2.dat >> tempos_circuit_2.dat
done
echo "Trocou o teste - comsol"
echo "HBGMRES_par_static" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < comsol.dat >> tempos_comsol.dat
done
echo "Trocou o teste - flowmeter5"
echo "HBGMRES_par_static" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "Trocou o teste - fpga_trans_01"
echo "HBGMRES_par_static" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "Trocou o teste - fpga_trans_02"
echo "HBGMRES_par_static" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "Trocou o teste - raefsky1"
echo "HBGMRES_par_static" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < raefsky1.dat >> tempos_raefsky1.dat
done
echo "Trocou o teste - raefsky2"
echo "HBGMRES_par_static" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < raefsky2.dat >> tempos_raefsky2.dat
done
echo "Trocou o teste - sherman4"
echo "HBGMRES_par_static" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < sherman4.dat >> tempos_sherman4.dat
done
echo "Trocou o teste - chipcool0"
echo "HBGMRES_par_static" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < chipcool0.dat >> tempos_chipcool0.dat
done
echo "Trocou o teste - memplus"
echo "HBGMRES_par_static" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < memplus.dat >> tempos_memplus.dat
done
echo "Trocou o teste - wang3"
echo "HBGMRES_par_static" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < wang3.dat >> tempos_wang3.dat
done
echo "Trocou o teste - matrix-new_3"
echo "HBGMRES_par_static" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
echo "Trocou o teste - add20"
echo "HBGMRES_par_dinamic" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < add20.dat >> tempos_add20.dat
done
echo "Trocou o teste - cavity05"
echo "HBGMRES_par_dinamic" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < cavity05.dat >> tempos_cavity05.dat
done
echo "Trocou o teste - cavity10"
echo "HBGMRES_par_dinamic" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < cavity10.dat >> tempos_cavity10.dat
done
echo "Trocou o teste - circuit_2"
echo "HBGMRES_par_dinamic" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < circuit_2.dat >> tempos_circuit_2.dat
done
echo "Trocou o teste - comsol"
echo "HBGMRES_par_dinamic" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < comsol.dat >> tempos_comsol.dat
done
echo "Trocou o teste - flowmeter5"
echo "HBGMRES_par_dinamic" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "Trocou o teste - fpga_trans_01"
echo "HBGMRES_par_dinamic" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "Trocou o teste - fpga_trans_02"
echo "HBGMRES_par_dinamic" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "Trocou o teste - raefsky1"
echo "HBGMRES_par_dinamic" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < raefsky1.dat >> tempos_raefsky1.dat
done
echo "Trocou o teste - raefsky2"
echo "HBGMRES_par_dinamic" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < raefsky2.dat >> tempos_raefsky2.dat
done
echo "Trocou o teste - sherman4"
echo "HBGMRES_par_dinamic" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < sherman4.dat >> tempos_sherman4.dat
done
echo "Trocou o teste - chipcool0"
echo "HBGMRES_par_dinamic" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < chipcool0.dat >> tempos_chipcool0.dat
done
echo "Trocou o teste - memplus"
echo "HBGMRES_par_dinamic" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < memplus.dat >> tempos_memplus.dat
done
echo "Trocou o teste - wang3"
echo "HBGMRES_par_dinamic" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic < wang3.dat >> tempos_wang3.dat
done
echo "Trocou o teste - matrix-new_3"
echo "HBGMRES_par_dinamic" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par_dinamic < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
echo "Trocou o teste - add20"
echo "HBGMRES_vec" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < add20.dat >> tempos_add20.dat
done
echo "Trocou o teste - cavity05"
echo "HBGMRES_vec" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < cavity05.dat >> tempos_cavity05.dat
done
echo "Trocou o teste - cavity10"
echo "HBGMRES_vec" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < cavity10.dat >> tempos_cavity10.dat
done
echo "Trocou o teste - circuit_2"
echo "HBGMRES_vec" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < circuit_2.dat >> tempos_circuit_2.dat
done
echo "Trocou o teste - comsol"
echo "HBGMRES_vec" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < comsol.dat >> tempos_comsol.dat
done
echo "Trocou o teste - flowmeter5"
echo "HBGMRES_vec" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "Trocou o teste - fpga_trans_01"
echo "HBGMRES_vec" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "Trocou o teste - fpga_trans_02"
echo "HBGMRES_vec" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "Trocou o teste - raefsky1"
echo "HBGMRES_vec" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < raefsky1.dat >> tempos_raefsky1.dat
done
echo "Trocou o teste - raefsky2"
echo "HBGMRES_vec" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < raefsky2.dat >> tempos_raefsky2.dat
done
echo "Trocou o teste - sherman4"
echo "HBGMRES_vec" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < sherman4.dat >> tempos_sherman4.dat
done
echo "Trocou o teste - chipcool0"
echo "HBGMRES_vec" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < chipcool0.dat >> tempos_chipcool0.dat
done
echo "Trocou o teste - memplus"
echo "HBGMRES_vec" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < memplus.dat >> tempos_memplus.dat
done
echo "Trocou o teste - wang3"
echo "HBGMRES_vec" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < wang3.dat >> tempos_wang3.dat
done
echo "Trocou o teste - matrix-new_3"
echo "HBGMRES_vec" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_vec < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
echo "Trocou o teste - add20"
echo "HBGMRES_par_vec" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par_vec< add20.dat >> tempos_add20.dat
done
echo "Trocou o teste - cavity05"
echo "HBGMRES_par_vec" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par_vec< cavity05.dat >> tempos_cavity05.dat
done
echo "Trocou o teste - cavity10"
echo "HBGMRES_par_vec" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par_vec< cavity10.dat >> tempos_cavity10.dat
done
echo "Trocou o teste - circuit_2"
echo "HBGMRES_par_vec" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par_vec< circuit_2.dat >> tempos_circuit_2.dat
done
echo "Trocou o teste - comsol"
echo "HBGMRES_par_vec" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_vec< comsol.dat >> tempos_comsol.dat
done
echo "Trocou o teste - flowmeter5"
echo "HBGMRES_par_vec" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_vec< flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "Trocou o teste - fpga_trans_01"
echo "HBGMRES_par_vec" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_vec< fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "Trocou o teste - fpga_trans_02"
echo "HBGMRES_par_vec" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_vec< fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "Trocou o teste - raefsky1"
echo "HBGMRES_par_vec" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_vec< raefsky1.dat >> tempos_raefsky1.dat
done
echo "Trocou o teste - raefsky2"
echo "HBGMRES_par_vec" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_vec< raefsky2.dat >> tempos_raefsky2.dat
done
echo "Trocou o teste - sherman4"
echo "HBGMRES_par_vec" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_vec< sherman4.dat >> tempos_sherman4.dat
done
echo "Trocou o teste - chipcool0"
echo "HBGMRES_par_vec" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_vec< chipcool0.dat >> tempos_chipcool0.dat
done
echo "Trocou o teste - memplus"
echo "HBGMRES_par_vec" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_vec< memplus.dat >> tempos_memplus.dat
done
echo "Trocou o teste - wang3"
echo "HBGMRES_par_vec" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_vec< wang3.dat >> tempos_wang3.dat
done
echo "Trocou o teste - matrix-new_3"
echo "HBGMRES_par_vec" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_vec< matrix-new_3.dat >> tempos_matrix-new_3.dat
done
echo "Trocou o teste - add20"
echo "HBGMRES_par_static_vec" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < add20.dat >> tempos_add20.dat
done
echo "Trocou o teste - cavity05"
echo "HBGMRES_par_static_vec" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < cavity05.dat >> tempos_cavity05.dat
done
echo "Trocou o teste - cavity10"
echo "HBGMRES_par_static_vec" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < cavity10.dat >> tempos_cavity10.dat
done
echo "Trocou o teste - circuit_2"
echo "HBGMRES_par_static_vec" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < circuit_2.dat >> tempos_circuit_2.dat
done
echo "Trocou o teste - comsol"
echo "HBGMRES_par_static_vec" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < comsol.dat >> tempos_comsol.dat
done
echo "Trocou o teste - flowmeter5"
echo "HBGMRES_par_static_vec" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "Trocou o teste - fpga_trans_01"
echo "HBGMRES_par_static_vec" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "Trocou o teste - fpga_trans_02"
echo "HBGMRES_par_static_vec" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "Trocou o teste - raefsky1"
echo "HBGMRES_par_static_vec" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < raefsky1.dat >> tempos_raefsky1.dat
done
echo "Trocou o teste - raefsky2"
echo "HBGMRES_par_static_vec" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < raefsky2.dat >> tempos_raefsky2.dat
done
echo "Trocou o teste - sherman4"
echo "HBGMRES_par_static_vec" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < sherman4.dat >> tempos_sherman4.dat
done
echo "Trocou o teste - chipcool0"
echo "HBGMRES_par_static_vec" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < chipcool0.dat >> tempos_chipcool0.dat
done
echo "Trocou o teste - memplus"
echo "HBGMRES_par_static_vec" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < memplus.dat >> tempos_memplus.dat
done
echo "Trocou o teste - wang3"
echo "HBGMRES_par_static_vec" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < wang3.dat >> tempos_wang3.dat
done
echo "Trocou o teste - matrix-new_3"
echo "HBGMRES_par_static_vec" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_static_vec < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
echo "Trocou o teste - add20"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < add20.dat >> tempos_add20.dat
done
echo "Trocou o teste - cavity05"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < cavity05.dat >> tempos_cavity05.dat
done
echo "Trocou o teste - cavity10"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < cavity10.dat >> tempos_cavity10.dat
done
echo "Trocou o teste - circuit_2"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < circuit_2.dat >> tempos_circuit_2.dat
done
echo "Trocou o teste - comsol"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < comsol.dat >> tempos_comsol.dat
done
echo "Trocou o teste - flowmeter5"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "Trocou o teste - fpga_trans_01"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "Trocou o teste - fpga_trans_02"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "Trocou o teste - raefsky1"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < raefsky1.dat >> tempos_raefsky1.dat
done
echo "Trocou o teste - raefsky2"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < raefsky2.dat >> tempos_raefsky2.dat
done
echo "Trocou o teste - sherman4"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < sherman4.dat >> tempos_sherman4.dat
done
echo "Trocou o teste - chipcool0"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < chipcool0.dat >> tempos_chipcool0.dat
done
echo "Trocou o teste - memplus"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < memplus.dat >> tempos_memplus.dat
done
echo "Trocou o teste - wang3"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
./HBGMRES_par_dinamic_vec < wang3.dat >> tempos_wang3.dat
done
echo "Trocou o teste - matrix-new_3"
echo "HBGMRES_par_dinamic_vec" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par_dinamic_vec < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
