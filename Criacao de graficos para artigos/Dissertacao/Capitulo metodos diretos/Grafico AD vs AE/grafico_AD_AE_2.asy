import plot;
import math;

    Graph gr;

    gr.set_style(PaperStyle);

//    gr.plot("valores.con", 0, 1, "$ \textrm{Elimina\c c\~ao Gaussiana - AD} $");
//    gr.plot("valores.con", 0, 2, "$ \textrm{Elimina\c c\~ao Gaussiana - AE} $");
//    gr.plot("valores.con", 0, 3, "$ \textrm{Decomposi\c c\~ao LDU - AD} $");
//    gr.plot("valores.con", 0, 4, "$ \textrm{Decomposi\c c\~ao LDU - AE} $");
//    gr.plot("valores.con", 0, 5, "$ \textrm{Decomposi\c c\~ao QR - AD} $");
//    gr.plot("valores.con", 0, 6, "$ \textrm{Decomposi\c c\~ao QR - AE} $");

    gr.plot("valores2.con", 0, 1, "$ \textrm{Gauss-Jordan - AD} $");
    gr.plot("valores2.con", 0, 2, "$ \textrm{Gauss-Jordan - AE} $");
    gr.plot("valores2.con", 0, 3, "$ \textrm{Decomposi\c c\~ao LU - AD} $");
    gr.plot("valores2.con", 0, 4, "$ \textrm{Decomposi\c c\~ao LU - AE} $");
    gr.plot("valores2.con", 0, 5, "$ \textrm{Decomposi\c c\~ao Cholesky - AD} $");
    gr.plot("valores2.con", 0, 6, "$ \textrm{Decomposi\c c\~ao Cholesky - AE} $");

 		gr.xlabel("$ \textrm{Ordem da matriz} $");
    gr.ylabel("$ \textrm{Tempo m\' edio de execu\c c\~ao (s)} $");

    gr.logscale(false, false);
//    gr.size(80mm, 70mm);

    gr.setymax( 1.1);

    add(gr.finish());

    shipout("grafico_AD_AE_2_windows_2", "eps");
