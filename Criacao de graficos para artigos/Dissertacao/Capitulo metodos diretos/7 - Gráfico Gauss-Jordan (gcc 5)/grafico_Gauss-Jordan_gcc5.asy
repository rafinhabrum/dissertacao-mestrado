import plot;
import math;

    Graph gr;

    gr.set_style(PaperStyle);

    gr.plot("valores.con", 0, 1, "$ \textrm{Precis\~ao simples} $");
    gr.plot("valores.con", 0, 2, "$ \textrm{Precis\~ao dupla} $");

    gr.xlabel("$ \textrm{Ordem da matriz} $");
    gr.ylabel("$ \textrm{Tempo m\' edio de execu\c c\~ ao (s)} $");

    gr.logscale(false, false);
//    gr.size(80mm, 70mm);

//    gr.setymax( 1.0);

    add(gr.finish());

    shipout("grafico_Gauss-Jordan_gcc5_windows_2", "eps");
