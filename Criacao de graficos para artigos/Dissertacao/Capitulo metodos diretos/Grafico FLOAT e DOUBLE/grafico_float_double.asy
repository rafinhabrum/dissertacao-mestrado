import plot;
import math;

    Graph gr;

    gr.set_style(PaperStyle);

    gr.plot("valores.con", 0, 1, "$ \textrm{Elimina\c c\~ao Gaussiana - Simples} $");
    gr.plot("valores.con", 0, 2, "$ \textrm{Elimina\c c\~ao Gaussiana - Dupla} $");
    gr.plot("valores.con", 0, 3, "$ \textrm{Decomposi\c c\~ao LDU - Simples} $");
    gr.plot("valores.con", 0, 4, "$ \textrm{Decomposi\c c\~ao LDU - Dupla} $");
    gr.plot("valores.con", 0, 5, "$ \textrm{Decomposi\c c\~ao QR - Simples} $");
    gr.plot("valores.con", 0, 6, "$ \textrm{Decomposi\c c\~ao QR - Dupla} $");

//    gr.plot("valores2.con", 0, 1, "$ \textrm{Gauss-Jordan - Simples} $");
//    gr.plot("valores2.con", 0, 2, "$ \textrm{Gauss-Jordan - Dupla} $");
//    gr.plot("valores2.con", 0, 3, "$ \textrm{Decomposi\c c\~ao LU - Simples} $");
//    gr.plot("valores2.con", 0, 4, "$ \textrm{Decomposi\c c\~ao LU - Dupla} $");
//    gr.plot("valores2.con", 0, 5, "$ \textrm{Decomposi\c c\~ao Cholesky - Simples} $");
//    gr.plot("valores2.con", 0, 6, "$ \textrm{Decomposi\c c\~ao Cholesky - Dupla} $");


    gr.xlabel("$ \textrm{Ordem da matriz} $");
    gr.ylabel("$ \textrm{Tempo m\' edio de execu\c c\~ao (s)} $");

    gr.logscale(false, false);
//    gr.size(80mm, 70mm);

    gr.setymax( 205);

    add(gr.finish());

    shipout("grafico_float_double_windows_2", "eps");
