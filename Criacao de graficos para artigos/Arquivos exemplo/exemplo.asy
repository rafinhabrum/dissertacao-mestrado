import plot;
import math;

    Graph gr;
    
    gr.set_style(PaperStyle);
 
    gr.plot("errofem-le.con", 0, 4, "$ \textrm{SHDG} $"); 
    gr.plot("errofem-ns-c.con", 0, 4, "$ \textrm{LDGC} $"); 
    gr.plot("erro.local.proj.con", 0, 4, "$ {\textrm{LP}} $"); 
    gr.plot("valores.con", 0, 4, "$ {\textrm{Interpolant}} $");
    

//    gr.plot("ldgherrol-ns-c.con", 0, 4, "$ LDGC_{LP}$"); 
//    gr.plot("errointer-c.con", 0, 4, "$ LDGC_{I}$"); 
    


//    gr.xlabel("$-\log( xel )$");
    gr.xlabel("$-\log( \textrm{h} )$");
    gr.ylabel("$\log(\textrm{error})$");
   
    gr.logscale(false, false);
    gr.size(60mm, 80mm);
    
    gr.setymax( 1.0);

    add(gr.finish());

    shipout("exemplo", "eps");

