import plot;
import math;

    Graph gr;
    
    gr.set_style(PaperStyle);
 
    gr.plot("valores.con", 0, 1, "$ \textrm{JR} $");
    gr.plot("valores.con", 0, 2, "$ \textrm{JOR} $");
    gr.plot("valores.con", 0, 3, "$ \textrm{DOR} $");
    gr.plot("valores.con", 0, 4, "$ \textrm{GS} $");
//    gr.plot("valores.con", 0, 5, "$ \textrm{Gauss-Seidel distribuído} $");
    gr.plot("valores.con", 0, 6, "$ \textrm{SOR} $");    

    gr.xlabel("$ \textrm{Graus de liberdade} $");
    gr.ylabel("$ \textrm{Tempo m\'edio de execu\c c\~ao (s)} $");
   
    gr.logscale(false, false);
    gr.size(80mm, 70mm);
    
//    gr.setymax( 1.0);

    add(gr.finish());

    shipout("grafico_1", "png");

