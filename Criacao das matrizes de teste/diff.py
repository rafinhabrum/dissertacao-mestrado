import os
import sys
import argparse

def read_large_file(file_object):
    """
    Uses a generator to read a large file lazily
    """
    while True:
        data = file_object.readline()
        if not data:
            break
        yield data


def compare(filename1, filename2):

	if os.path.isfile(filename1) and os.path.isfile(filename2):
		# read first file
		file1 = open(filename1, 'r')
		# read second file
		file2 = open(filename2, 'r')
		# create generator 1
		gen_file1 = read_large_file(file1)
		gen_file2 = read_large_file(file2)
		
		line_count = 0
		while gen_file1 and gen_file2:
			line1 = gen_file1.__next__()
			line2 = gen_file2.__next__()
			line_count += 1
			if line_count % 1000 == 0:
				print(line_count)
				
			if line1 != line2:
				print('First difference in line: ', line_count)
				print('Line file 1: ', line1)
				print('Line file 2: ', line2)
				return False
	else:
		print('Error opening file...')
		sys.exit(1)
	return True


if __name__ == '__main__':
		
	if len(sys.argv) != 3:
		print('Incorrect usage: please select 2 files as parameters')
		sys.exit(1)
	else:
		print('Processing files:', sys.argv[1], 'and', sys.argv[2])
		try:
			if not compare(sys.argv[1], sys.argv[2]):
				print("Fuck it, it's all fucking different")
		except Exception:
			print('Files are fucking equal')
