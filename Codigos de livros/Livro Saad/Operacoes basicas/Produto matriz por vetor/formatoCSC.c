/* Algoritmo em FORTRAN-90 para produto de matriz por vetor usando o formato CSR de armazenamento
DO J=1, N
    K1 = IA(J)
    K2 = IA(J+1)-1
    Y(JA(K1:K2)) = Y(JA(K1:K2))+X(J)*A(K1:K2)
ENDDO
*/
/* Formato Compressed Sparse Column - CSC (p. 113 do Saad)
Usa 3 arrays para armazenar a matriz esparsa:
-> o AA para os valores numéricos (não-nulos) da matriz A na ordem por linha, os valores da primeira
linha seguidos dos valores da segunda linha... (a princípio do tipo double) -> tamanho Nz (qtde de valores não nulos)
-> o JA para armazenar o índice da linha de cada um dos valores de AA (sempre do tipo inteiro) -> tamanho Nz
-> o IA, de tamanho N+1, para armazenar onde cada coluna da matriz começa nos dois arrays acima. A
posição N+1 aponta para Nz+1 (sempre do tipo inteiro)
*/

#include <stdio.h>
#include <string.h>

#define NZ 13
#define NMATRIZ 6

double produtoVetorial(double *a, double *b, int size)
{
    int result = 0;
    for(int i = 1; i <= size; ++i)
    {
        result += (a[i]*b[i]);
    }
    return result;
}

void imprimirVetor(double *a, int size)
{
    for(int i = 1; i <= size; ++i)
    {
        printf("%.0lf ", a[i]);
    }
    printf("\n");
}

int main(){
    double AA[NZ];
    int JA[NZ], IA[NMATRIZ+1];
    double X[NMATRIZ], Y[NMATRIZ];

    int i, j;
    int k1, k2;

    double mat[NMATRIZ][NMATRIZ];
    int row, indexIA;

    //ler vetor AA
    for(i = 1; i < NZ; ++i)
    {
        scanf("%lf", &AA[i]);
    }
    //ler vetor JR
    for(i = 1; i < NZ; ++i)
    {
        scanf("%d", &JA[i]);
    }
    //ler vetor JC
    for(i = 1; i <= NMATRIZ; ++i)
    {
        scanf("%d", &IA[i]);
    }

    //ler vetor X
    for(i = 1; i < NMATRIZ; ++i)
    {
        scanf("%lf", &X[i]);
        Y[i] = 0;
    }

    for(j = 1; j < NMATRIZ; ++j)
    {
        k1 = IA[j];
        k2 = IA[j+1];
        //printf("k1 = %d | k2 = %d\n", k1, k2);
        for(int i = k1; i < k2; ++i)
        {
            Y[JA[i]] += X[j]*AA[i];
        }
    }

    printf("Vetor Y:\n");

    for(i = 1; i < NMATRIZ; ++i)
    {
        printf("%.0lf ", Y[i]);
    }

    printf("\n");
}
