!gfortran formatoDiagonais_fortran.f90 -o formatoDiagonais_fortran
program hello
	implicit none
	integer :: I, J, JOFF, NDIAG, DIAG(5,3), IOFF(3), N
	integer    :: X(5), Y(5)
	DIAG = reshape((/0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0, 11, 12, 0/), (/5, 3/), order=(/2, 1/))
	!write (*,*) DIAG
	IOFF = (/-1, 0, 2/)
	X = (/1, 1, 1, 1, 1/)
	Y = (/0, 0, 0, 0, 0/)
	N = 5
	NDIAG = 3
	DO J=1, NDIAG
		JOFF = IOFF(J)
		write (*,*) "JOFF", JOFF
		DO I=1, N
			IF (JOFF+I > 0 .AND. JOFF+I < 6) THEN
				write (*,*) "X(", (JOFF+I), ") = ", (JOFF+I)
				!write (*,*) "DIAG(", I, ",", J, ") = ", DIAG(I,J)
				Y(I) = Y(I) + DIAG(I,J) * X(JOFF+I)
				!write (*,*) "Y(", I, ") = ", Y(I)
			ENDIF
		ENDDO
	ENDDO
	write (*,*) "Y final = ", Y
end program hello
