/* Algoritmo em FORTRAN-90 para produto de matriz por vetor usando o formato CSR de armazenamento
DO J=1, NDIAG
  JOFF = IOFF(J)
  DO I=1, N
    IF (JOFF+I > 0 .AND. JOFF+I < 6) THEN
      Y(I) = Y(I) + DIAG(I,J) * X(JOFF+I)
    ENDIF
  ENDDO
ENDDO
*/
/* Formato para matrizes diagonais (p. 113 do Saad)
Usa 2 arrays (1 bidimensional) para armazenar a matriz esparsa:
-> a matriz DIAG, de tamanho NxNd (onde Nd representa o número de diagonais), que armazena cada
diagonal com valores não nulos da matriz A nas colunas.
-> o array IOFF, de tamanho Nd, armazena o deslocamento (offset) dos valores da coluna i do DIAG
em relação a diagonal principal. Então, o elemento da posição (i,j) da matriz DIAG está na posição
i,i+ioff(j) da matriz A.
*/

#include <stdio.h>
#include <string.h>

#define ND 4
#define NMATRIZ 6
//#define VALOR_ZERO -100

double produtoVetorial(double *a, double *b, int size)
{
    int result = 0;
    for(int i = 1; i <= size; ++i)
    {
        result += (a[i]*b[i]);
    }
    return result;
}

void imprimirVetor(double *a, int size)
{
    for(int i = 1; i <= size; ++i)
    {
        printf("%.0lf ", a[i]);
    }
    printf("\n");
}

int main(){
    double DIAG[NMATRIZ+1][ND+1];
    int IOFF[ND+1];

    double X[NMATRIZ], Y[NMATRIZ];
    double auxAA[NMATRIZ], auxX[NMATRIZ];

    int i, j;
    int joff;

    double mat[NMATRIZ][NMATRIZ];
    int row, indexIA;

    //ler vetor DIAG
    for(i = 1; i < NMATRIZ; ++i)
    {
        for(j = 1; j < ND; ++j)
        {
            scanf("%lf", &DIAG[i][j]);
        }
    }
    //ler vetor IOFF
    for(i = 1; i < ND; ++i)
    {
        scanf("%d", &IOFF[i]);
    }

    //ler vetor X
    for(i = 1; i < NMATRIZ; ++i)
    {
        scanf("%lf", &X[i]);
        Y[i] = 0;
    }

    /*
    DO J=1, NDIAG
      JOFF = IOFF(J)
      DO I=1, N
        IF (JOFF+I > 0 .AND. JOFF+I < 6) THEN
          Y(I) = Y(I) + DIAG(I,J) * X(JOFF+I)
        ENDIF
      ENDDO
    ENDDO
    */
    for(j = 1; j < ND; ++j)
    {
        joff = IOFF[j];
        for(int i = 1; i < NMATRIZ; ++i)
        {
            if(joff + i > 0 && joff + i < NMATRIZ)
            {
                Y[i] += (DIAG[i][j] * X[joff + i]);
            }
        }
    }

    printf("Vetor Y:\n");

    for(i = 1; i < NMATRIZ; ++i)
    {
        printf("%.0lf ", Y[i]);
    }

    printf("\n");
}
