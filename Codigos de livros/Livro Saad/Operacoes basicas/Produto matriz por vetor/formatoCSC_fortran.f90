!gfortran formatoCSC_fortran.f90 -o formatoCSC_fortran
program hello
implicit none
integer :: J, N, k1, IA(6), JA(12), k2
integer    :: A(12), X(5), Y(5)
A = (/1.0, 3.0, 6.0, 4.0, 7.0, 10.0, 2.0, 5.0, 8.0, 11.0, 9.0, 12.0 /)
JA = (/1, 2, 3, 2, 3, 4, 1, 2, 3, 4, 3, 5 /)
IA = (/1, 4, 5, 7, 11, 13 /)
X = (/1, 1, 1, 1, 1 /)
Y = (/0, 0, 0, 0, 0 /)
N = 5
    DO J=1, N
        K1 = IA(J)
        K2 = IA(J+1)-1
        !write (*,*) "K1 = ", K1, " K2 = ", K2
        write (*,*) "A(K1:K2) = ", A(K1:K2)
        write (*,*) "JA(K1:K2) = ", JA(K1:K2)
        !write (*,*) "X(J) = ", X(J)
        Y(JA(K1:K2)) = Y(JA(K1:K2))+X(J)*A(K1:K2)
        write (*,*) "Y(", JA(K1:K2) , ") = ", Y(JA(K1:K2))
    ENDDO
    write (*,*) "Y final = ", Y
end program hello
