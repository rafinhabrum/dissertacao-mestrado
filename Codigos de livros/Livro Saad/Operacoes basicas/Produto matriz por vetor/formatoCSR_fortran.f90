!gfortran formatoCSR_fortran.f90 -o formatoCSR_fortran
program hello
implicit none
integer :: I, N, k1, IA(6), JA(12), k2
integer    :: A(12), X(5), Y(6)
A = (/1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0/)
JA = (/1,4,1,2,4,1,3,4,5,3,4,5/)
IA = (/1,3,6,10,12,13/)
X = (/1,1,1,1,1/)
N = 5
    DO I=1, N
        K1 = IA(I)
        K2 = IA(I+1)-1
        !write (*,*) "K1 = ", K1, " K2 = ", K2
        write (*,*) "A(K1:K2) = ", A(K1:K2)
        write (*,*) "JA(K1:K2) = ", JA(K1:K2)
        write (*,*) "X(JA(K1:K2)) = ", X(JA(K1:K2))
        Y(I) = dot_product(A(K1:K2),X(JA(K1:K2)))
        write (*,*) "Y(", I, ") = ", Y(I)
    ENDDO
    write (*,*) "Y final = ", Y
end program hello
