!gfortran formatoCSR_fortran.f90 -o formatoCSR_fortran
!neste programa a matriz L é uma matriz triangular inferior com diagonal 1!
program hello
implicit none
integer :: I, N, k1, IAL(6), JAL(15), k2
integer    :: AL(15), X(5), Y(5)
AL = (/1.0,2.0,1.0,4.0,5.0,1.0,7.0,8.0,9.0,1.0,11.0,12.0,13.0,14.0,1.0/)
JAL = (/1,1,2,1,2,3,1,2,3,4,1,2,3,4,5/)
IAL = (/1,2,4,7,11,16/)
Y = (/1,3,10,25,51/)
X = (/0,0,0,0,0/)
N = 5
X(1) = Y(1)
	DO I = 2, N
		K1 = IAL(I)
		K2 = IAL(I+1)-1
        !write (*,*) "K1 = ", K1, " K2 = ", K2
        write (*,*) "AL(K1:K2) = ", AL(K1:K2)
        !write (*,*) "JAL(K1:K2) = ", JAL(K1:K2)
        write (*,*) "X(JAL(K1:K2)) = ", X(JAL(K1:K2))
        write (*,*) "Y(", I, ") = ", Y(I)
        write (*,*) "dot_product = ", dot_product(AL(K1:K2),X(JAL(K1:K2)))
		X(I)=Y(I) - dot_product(AL(K1:K2),X(JAL(K1:K2)))
        write (*,*) "X(", I, ") = ", X(I)
	ENDDO
    write (*,*) "X final = ", X
end program hello
