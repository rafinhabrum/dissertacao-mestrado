/* Algoritmo em FORTRAN-90 para resolução de um sistema linear do tipo Lx=y usando o formato CSR de armazenamento
X(1) = Y(1)
DO I = 2, N
	K1 = IAL(I)
	K2 = IAL(I+1)-1
  X(I)=Y(I) - dot_product(AL(K1:K2),X(JAL(K1:K2)))
ENDDO
*/
/* Formato Compressed Sparse Row - CSR (p. 113 do Saad)
Usa 3 arrays para armazenar a matriz esparsa:
-> o AA para os valores numéricos (não-nulos) da matriz A na ordem por linha, os valores da primeira
linha seguidos dos valores da segunda linha... (a princípio do tipo double) -> tamanho Nz (qtde de valores não nulos)
-> o JA para armazenar o índice da coluna de cada um dos valores de AA (sempre do tipo inteiro) -> tamanho Nz
-> o IA, de tamanho N+1, para armazenar onde cada linha da matriz começa nos dois arrays acima. A
posição N+1 aponta para Nz+1 (sempre do tipo inteiro)
*/

#include <stdio.h>
#include <string.h>

#define NZ 16
#define NMATRIZ 6

double produtoVetorial(double *a, double *b, int size)
{
    int result = 0;
    for(int i = 1; i <= size; ++i)
    {
        result += (a[i]*b[i]);
    }
    return result;
}

void imprimirVetor(double *a, int size)
{
    for(int i = 1; i <= size; ++i)
    {
        printf("%.0lf ", a[i]);
    }
    printf("\n");
}

int main(){
    double AAL[NZ];
    int JAL[NZ], IAL[NMATRIZ+1];
    double X[NMATRIZ], Y[NMATRIZ];
    double auxAAL[NMATRIZ], auxX[NMATRIZ];

    int i;
    int k1, k2;

    double mat[NMATRIZ][NMATRIZ];
    int row, indexIA;

    //ler vetor AAL
    for(i = 1; i < NZ; ++i)
    {
        scanf("%lf", &AAL[i]);
    }
    //ler vetor JAL
    for(i = 1; i < NZ; ++i)
    {
        scanf("%d", &JAL[i]);
    }
    //ler vetor IAL
    for(i = 1; i <= NMATRIZ; ++i)
    {
        scanf("%d", &IAL[i]);
    }

    //ler vetor Y
    for(i = 1; i < NMATRIZ; ++i)
    {
        scanf("%lf", &Y[i]);
    }

    /*
    X(1) = Y(1)
    DO I = 2, N
    	K1 = IAL(I)
    	K2 = IAL(I+1)-1
      X(I)=Y(I) - dot_product(AL(K1:K2),X(JAL(K1:K2)))
    ENDDO
    */

    memset(X, 0, sizeof(X));

    X[1] = Y[1];

    for(i = 2; i < NMATRIZ; ++i)
    {
        k1 = IAL[i];
        k2 = IAL[i+1];
        //printf("k1 = %d | k2 = %d\n", k1, k2);
        for(int j = k1; j < k2; ++j)
        {
            auxAAL[j-k1+1] = AAL[j];
            auxX[j-k1+1] = X[JAL[j]];
        }
        //imprimirVetor(auxAA, k2-k1);
        //imprimirVetor(auxX, k2-k1);
        X[i] = Y[i] - produtoVetorial(auxAAL, auxX, k2-k1);
    }

    printf("Vetor X:\n");

    for(i = 1; i < NMATRIZ; ++i)
    {
        printf("%.0lf ", X[i]);
    }

    printf("\n");
}
