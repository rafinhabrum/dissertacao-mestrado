/* Formato Compressed Sparse Row - CSR (p. 113 do Saad)
Usa 3 arrays para armazenar a matriz esparsa:
-> o AA para os valores numéricos (não-nulos) da matriz A na ordem por linha, os valores da primeira
linha seguidos dos valores da segunda linha... (a princípio do tipo double) -> tamanho Nz (qtde de valores não nulos)
-> o JA para armazenar o índice da coluna de cada um dos valores de AA (sempre do tipo inteiro) -> tamanho Nz
-> o IA, de tamanho N+1, para armazenar onde cada linha da matriz começa nos dois arrays acima. A
posição N+1 aponta para Nz+1 (sempre do tipo inteiro)
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define NZ 32748
#define NMATRIZ 1183

void imprimirMatrizCheia(double **mat)
{
    int i, j;
    for(i = 1; i < NMATRIZ; ++i)
    {
        for(j = 1; j < NMATRIZ; ++j)
        {
            printf("%lf ", mat[i][j]);
        }
        printf("\n");
    }
}

 int main(){
    double *AA = (double *)malloc(NZ*sizeof(double));
    int *JA = (int *)malloc(NZ*sizeof(int));
    int *IA = (int *)malloc((NMATRIZ+1)*sizeof(int));
	

    int i;

    double **mat = (double**)malloc(NMATRIZ*sizeof(double *));
    for(i = 0; i < NMATRIZ; i++)
    {
      mat[i] = (double*)calloc(NMATRIZ,sizeof(double));
    }
    int row, indexIA;

    //ler vetor AA
    for(i = 1; i < NZ; ++i)
    {
        scanf("%lf", &AA[i]);
    }
    //ler vetor JR
    for(i = 1; i < NZ; ++i)
    {
        scanf("%d", &JA[i]);
    }
    //ler vetor JC
    for(i = 1; i <= NMATRIZ; ++i)
    {
        scanf("%d", &IA[i]);
    }

    //criar matriz
    memset(mat, 0, sizeof(mat));
    row = 1;
    indexIA = 2;
    for(i = 1; i < NZ; ++i)
    {
        if(IA[indexIA] == i)
        {
            row++;
            indexIA++;
        }
        mat[row][JA[i]] = AA[i];
    }

    imprimirMatrizCheia(mat);

    return 0;
 }
