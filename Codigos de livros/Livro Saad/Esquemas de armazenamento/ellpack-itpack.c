/* Formato Ellpack-Itpack (p. 114 do Saad) --> assume que tem no máximo Nd valores não nulos por linha
Usa 2 matrizes para armazenar a matriz esparsa:
-> a matriz COEF, de tamanho NxNd, que armazena os valores não nulos da matriz A por linha.
-> a matriz JCOEF, de tamanho NxNd, armazena a coluna de cada valor armazenado em COEF.
*/
#include <stdio.h>
#include <string.h>

#define ND 4
#define NMATRIZ 6
#define VALOR_ZERO -1

void imprimirMatrizCheia(double mat[NMATRIZ][NMATRIZ])
{
    int i, j;
    for(i = 1; i < NMATRIZ; ++i)
    {
        for(j = 1; j < NMATRIZ; ++j)
        {
            printf("%lf ", mat[i][j]);
        }
        printf("\n");
    }
}

 int main(){
    double COEF[NMATRIZ][ND];
    int JCOEF[NMATRIZ][ND];

    int i, j;

    double mat[NMATRIZ][NMATRIZ];

    //ler vetor COEF
    for(i = 1; i < NMATRIZ; ++i)
    {
        for(j = 1; j < ND; ++j)
        {
            scanf("%lf", &COEF[i][j]);
        }
    }
    //ler vetor JCOEF
    for(i = 1; i < NMATRIZ; ++i)
    {
        for(j = 1; j < ND; ++j)
        {
            scanf("%d", &JCOEF[i][j]);
        }
    }

    //criar matriz
    memset(mat, 0, sizeof(mat));
    for(i = 1; i < NMATRIZ; ++i)
    {
        for(j = 1; j < ND; ++j)
        {
            if(JCOEF[i][j] != VALOR_ZERO)
            {
                mat[i][JCOEF[i][j]] = COEF[i][j];
            }
        }
    }

    imprimirMatrizCheia(mat);

    return 0;
 }
