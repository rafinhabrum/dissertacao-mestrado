/* Formato coordenadas (p. 112 do Saad)
Usa 3 arrays de tamanho Nz (qtde de valores não nulos da matriz) para armazenar a matriz esparsa:
-> o AA para os valores numéricos (não-nulos) da matriz A em qualquer ordem (a princípio do tipo double)
-> o JR para armazenar o índice da linha de cada um dos valores de AA (sempre do tipo inteiro)
-> o JC para armazenar o índice da coluna de cada um dos valores de AA (sempre do tipo inteiro)
*/

#include <stdio.h>
#include <string.h>

#define NZ 13
#define NMATRIZ 6

void imprimirMatrizCheia(double mat[NMATRIZ][NMATRIZ])
{
    int i, j;
    for(i = 1; i < NMATRIZ; ++i)
    {
        for(j = 1; j < NMATRIZ; ++j)
        {
            printf("%lf ", mat[i][j]);
        }
        printf("\n");
    }
}

 int main(){
    double AA[NZ];
    int JR[NZ], JC[NZ];

    int i;

    double mat[NMATRIZ][NMATRIZ];

    //ler vetor AA
    for(i = 1; i < NZ; ++i)
    {
        scanf("%lf", &AA[i]);
    }
    //ler vetor JR
    for(i = 1; i < NZ; ++i)
    {
        scanf("%d", &JR[i]);
    }
    //ler vetor JC
    for(i = 1; i < NZ; ++i)
    {
        scanf("%d", &JC[i]);
    }

    //criar matriz
    memset(mat, 0, sizeof(mat));
    for(i = 1; i < NZ; ++i)
    {
        mat[JR[i]][JC[i]] = AA[i];
    }

    imprimirMatrizCheia(mat);

    return 0;
 }
