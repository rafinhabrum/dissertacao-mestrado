/* Formato para matrizes diagonais (p. 113 do Saad)
Usa 2 arrays (1 bidimensional) para armazenar a matriz esparsa:
-> a matriz DIAG, de tamanho NxNd (onde Nd representa o número de diagonais), que armazena cada
diagonal com valores não nulos da matriz A nas colunas.
-> o array IOFF, de tamanho Nd, armazena o deslocamento (offset) dos valores da coluna i do DIAG
em relação a diagonal principal. Então, o elemento da posição (i,j) da matriz DIAG está na posição
i,i+ioff(j) da matriz A.
*/
#include <stdio.h>
#include <string.h>

#define ND 4
#define NMATRIZ 6
#define VALOR_ZERO -100

void imprimirMatrizCheia(double mat[NMATRIZ][NMATRIZ])
{
    int i, j;
    for(i = 1; i < NMATRIZ; ++i)
    {
        for(j = 1; j < NMATRIZ; ++j)
        {
            printf("%lf ", mat[i][j]);
        }
        printf("\n");
    }
}

 int main(){
    double DIAG[NMATRIZ+1][ND+1];
    int IOFF[ND+1];

    int i, j;

    double mat[NMATRIZ][NMATRIZ];

    //ler vetor DIAG
    for(i = 1; i < NMATRIZ; ++i)
    {
        for(j = 1; j < ND; ++j)
        {
            scanf("%lf", &DIAG[i][j]);
        }
    }
    //ler vetor IOFF
    for(i = 1; i < ND; ++i)
    {
        scanf("%d", &IOFF[i]);
    }

    //criar matriz
    memset(mat, 0, sizeof(mat));
    for(i = 1; i < NMATRIZ; ++i)
    {
        for(j = 1; j < ND; ++j)
        {
            if(DIAG[i][j] != -VALOR_ZERO)
            {
                mat[i][i+IOFF[j]] = DIAG[i][j];
            }
        }
    }

    imprimirMatrizCheia(mat);

    return 0;
 }
