/* Formato Modified Sparse Row - MSR (p. 113 do Saad)
Usa 2 arrays para armazenar a matriz esparsa:
-> o AA para os valores numéricos (não-nulos) da matriz A com os elementos da diagonal principal nas primeiras N
posicoes e, a partir da posicao N+2, os outros valores não-nulos ordenados pela linha (a princípio do tipo
double) -> tamanho Nz+1 (qtde de valores não nulos)
-> o JA, de tamanho Nz+1 para armazenar os índices dos valores de A. As primeiras N posições armazenam o indice
de AA que começa cada linha da matriz e a partir da posicao N+2 temos os valores da coluna de cada elemento na
mesma posicao i de AA. A posição N+1 aponta para Nz+2 (sempre do tipo inteiro)
*/

#include <stdio.h>
#include <string.h>

#define NZ 13 //NZ = 12
#define NMATRIZ 6

void imprimirMatrizCheia(double mat[NMATRIZ][NMATRIZ])
{
    int i, j;
    for(i = 1; i < NMATRIZ; ++i)
    {
        for(j = 1; j < NMATRIZ; ++j)
        {
            printf("%lf ", mat[i][j]);
        }
        printf("\n");
    }
}

 int main(){
    double AA[NZ+1];
    int JA[NZ+1];

    int i;

    double mat[NMATRIZ][NMATRIZ];
    int row, indexJA;

    //ler vetor AA
    for(i = 1; i <= NZ; ++i)
    {
        scanf("%lf", &AA[i]);
    }
    //ler vetor JA
    for(i = 1; i <= NZ; ++i)
    {
        scanf("%d", &JA[i]);
    }

    //criar matriz
    memset(mat, 0, sizeof(mat));
    for(i = 1; i < NMATRIZ; ++i)
    {
        mat[i][i] = AA[i];
    }

    indexJA = 2;
    row = 1;
    for(i = NMATRIZ+1; i <= NZ; ++i)
    {
        if(JA[indexJA] == i)
        {
            row++;
            indexJA++;
        }
        mat[row][JA[i]] = AA[i];
    }

    imprimirMatrizCheia(mat);

    return 0;
 }
