/* Algoritmo FOM (Full Orthogonalization Method) para sistemas lineares
Compute r_0 = b − A*x_0 , beta := ||r_0||_2 , and v_1 := r_0/beta
Define the m×m matrix H_m = {h_{ij}}_{i,j=1,...,m}; Set H_m = 0
For j = 1, 2, ... , m Do:
	Compute w_j := A*v_j
	For i = 1, ... , j Do:
		h_{ij} = (w_j,v_i)
		w_j := w_j − h_{ij}*v_i
	EndDo
	Compute h_{j+1,j} = ||w_j||_2. If h_{j+1,j} = 0 set m := j and break
	Compute v_{j+1} = w_j/h_{j+1,j}
EndDo
Compute y_m = H_m^{-1}*(beta*e_1) and x_m = x_0 + V_m*y_m
*/


#include <stdio.h>
#include <string.h>

#define MAX 6 //Só pode aceitar até 4

double mat[MAX][MAX]; //armazenar transposta a matriz

double calcularProdutoInterno(double *vec1, double *vec2, int tam) //testar
{
    int i;
    double soma = 0;
    for(i = 1; i <= tam; ++i)
    {
        soma += ((*(vec1 + i)) * (*(vec2 + i)));
    }
    return soma;
}

void multiplicarMatrizPorVetor(int tam, double *vec, double *pVecResul) //testar
{
    int i, j, k;
    double *result;

    result = (double *)malloc(tam*sizeof(double));
    for(i = 1; i <= tam; ++i)
    {
        *(result + i) = 0;
        for(k = 1; k <= tam; ++k)
        {
            *(result + i) += (mat[i][k])*(*(vec+k));
        }
        //printf("result[%d] = %lf\n", i, result[i]);
    }
    for(i = 1; i <= tam; i++)
    {
        *((*pVecResul) + i) = *(result + i);
    }
}

void FOM(int tam)
{
	int i, j, k;
	double w[MAX], v[MAX][MAX], r[MAX], beta;
  double *matH, aux;

  r = b; //considerando x_0 = 0
  beta = sqrt(calcularProdutoInterno(r, r, tam));
  for(i = 1; i <= tam; ++i)
  {
    v[1][i] = r[i]/beta;
  }

  matH = (double*)calloc(sizeof(tam*tam));

  for(j = 1; j <= tam; ++j)
  {
    multiplicarMatrizPorVetor(tam, v[1], &w);
    for(i = 1; i <= j; ++i)
    {
      aux = calcularProdutoInterno(w, v[i], tam);
      for(k = 1; k <= tam; ++k)
      {
        w[k] -= (aux*v[i][k]);
      }
      matH[(i-1)*tam+(j-1)] = aux;
    }
    aux = matH[(j)*tam+(j-1)] = sqrt(calcularProdutoInterno(w, w, tam));
    if(aux == 0)
    {
      tam = j;
      break;
    }
    for(k = 1; k <= tam; ++k)
    {
      v[j+1][k] = w[k]/aux;
    }
  }
  //Compute y_m = H_m^{-1}*(beta*e_1) and x_m = x_0 + V_m*y_m
}


int main(){

    return 0;
 }
