/* Algoritmo de Arnoldi com Householder
Select a nonzero vector v; Set z_1 = v
For j = 1,...,m,m + 1 Do:
  Compute the Householder unit vector w_j such that (w_j)_i = 0,i = 1,...,j − 1 and (P_j*z_j)_i = 0,i = j + 1,...,n, where P_j = I − 2*w_j*w_j^T
  h_j−1 = P_j*z_j
  v_j = P_1*P_2 ...P_j*e_j
  If j ≤ m compute z_{j+1} := P_j*P_{j−1} ...P_1*Av_j
EndDo

Algoritmo da ortogonalizacao Householder (para decomposicao QR)
Define X = [x_1 ,...,x_m]
For k = 1,...,m Do:
  If k > 1 compute r_k := P_(k−1)*P_(k−2) ... P_1*x_k
  Compute w_k using (1.24), (1.25), (1.26)
  Compute r_k := P_k*r_k with P_k = I − 2*w_k*w_k^T
  Compute q_k = P_1*P_2 ... P_k*e_k
EndDo
(1.24)
w_k = z / ||z||
(1.25)
z_i = 0, se i < k
    = beta + x_{ii}, se i = k
    = x_{ik}, se i > k
(1.26)
beta = sinal(x+{kk} * sqrt(sum_{i=k}^{n}{x_{ik}^2}))
*/

#include <stdio.h>
#include <string.h>

#define MAX 6 //Só pode aceitar até 4

double mat[MAX][MAX]; //armazenar transposta a matriz

double calcularNorma(double *vec1, double *vec2, int tam) //testar
{
    int i;
    double soma = 0;
    for(i = 1; i <= tam; ++i)
    {
        soma += ((*(vec1 + i)) * (*(vec2 + i)));
    }
    return sqrt(soma);
}

void multiplicacaoMatrizes(double **A, double **B, double ***M, int tam) //conferir se pode ser matriz nao quadrada
{
    int i, j, k;

    for(i = 1; i <= tam; ++i)
    {
        for(j = 1; j <= tam; ++j)
        {
            (*M)[i][j] = 0;
            for(k = 1; k <= tam; ++k)
            {
                (*M)[i][j] += (A[i][k]*B[k][j]);
            }
        }
    }
}

void multiplicarMatrizPorVetor(int tam, double *vec, double *pVecResul) //testar
{
    int i, j, k;
    double *result;

    result = (double *)malloc(tam*sizeof(double));
    for(i = 1; i <= tam; ++i)
    {
        *(result + i) = 0;
        for(k = 1; k <= tam; ++k)
        {
            *(result + i) += (mat[i][k])*(*(vec+k));
        }
        //printf("result[%d] = %lf\n", i, result[i]);
    }
    for(i = 1; i <= tam; i++)
    {
        *((*pVecResul) + i) = *(result + i);
    }
}

void ArnoldiComHouseholder(int tam)  //TESTAR
{/* Algoritmo de Arnoldi com Householder
Select a nonzero vector v; Set z_1 = v
For j = 1,...,m,m + 1 Do:
  Compute the Householder unit vector w_j such that (w_j)_i = 0,i = 1,...,j − 1 and (P_j*z_j)_i = 0,i = j + 1,...,n, where P_j = I − 2*w_j*w_j^T
  h_j−1 = P_j*z_j
  v_j = P_1*P_2 ...P_j*e_j
  If j ≤ m compute z_{j+1} := P_j*P_{j−1} ...P_1*Av_j
EndDo

Algoritmo da ortogonalizacao Householder (para decomposicao QR)
Define X = [x_1 ,...,x_m]
For k = 1,...,m Do:
  If k > 1 compute r_k := P_(k−1)*P_(k−2) ... P_1*x_k
  Compute w_k using (1.24), (1.25), (1.26)
  Compute r_k := P_k*r_k with P_k = I − 2*w_k*w_k^T
  Compute q_k = P_1*P_2 ... P_k*e_k
EndDo
(1.24)
w_k = z / ||z||
(1.25)
z_i = 0, se i < k
    = beta + x_{ii}, se i = k
    = x_{ik}, se i > k
(1.26)
beta = sinal(x+{kk} * sqrt(sum_{i=k}^{n}{x_{ik}^2}))
*/
	int i, j;
	double z[MAX] //AQUI
	double w[MAX], h;
	
	//~ norma = sqrt(calcularProdutoInterno(mat[1], mat[1], tam));
	//~ if(norma != 1)
	//~ {
		//~ for(i = 1; i <= tam; ++i)
		//~ {
			//~ mat[1][i] /= norma;
		//~ }
	//~ }
	//~ for(j = 1; j <= tam; ++j)
	//~ {
		//~ multiplicarMatrizPorVetor(tam, mat[j], &w);
		//~ for(i = 1; i <= tam; ++i)
		//~ {
			//~ h = calcularProdutoInterno(w, mat[i], tam);
			//~ for(int k = 1; k <= tam; ++k)
			//~ {
				//~ w[k] -= (h*mat[i][k]);
			//~ }
		//~ }
		//~ h = sqrt(calcularProdutoInterno(w, w, tam));
		//~ if(h == 0) break;
		//~ for(int k = 1; k <= tam; ++k)
		//~ {
			//~ mat[j+][w] = w[k]/h;
		//~ }
	//~ }
	//~ return j;
}


int main(){

    return 0;
 }
