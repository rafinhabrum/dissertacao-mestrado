/* Algoritmo de Arnoldi com Gram-Schimdt Modificado
Choose a vector v 1 of norm 1
For j = 1, 2, . . . , m Do:
    Compute w[j] := A*v[j]
    For i = 1, . . . , j Do:
        h[i][j] = (w[j] , v[i])
        w[j] := w[j] − h[i][j]*v[i]
    EndDo
    h[j+1][j] = ||w[j]||2
    If h[j+1][j] = 0 Stop
    v[j+1] = w[j] /h[j+1][j]
EndDo
*/

#include <stdio.h>
#include <string.h>

#define MAX 6 //Só pode aceitar até 4

double mat[MAX][MAX]; //armazenar transposta a matriz

double calcularProdutoInterno(double *vec1, double *vec2, int tam) //testar
{
    int i;
    double soma = 0;
    for(i = 1; i <= tam; ++i)
    {
        soma += ((*(vec1 + i)) * (*(vec2 + i)));
    }
    return soma;
}

void multiplicarMatrizPorVetor(int tam, double *vec, double *pVecResul) //testar
{
    int i, j, k;
    double *result;

    result = (double *)malloc(tam*sizeof(double));
    for(i = 1; i <= tam; ++i)
    {
        *(result + i) = 0;
        for(k = 1; k <= tam; ++k)
        {
            *(result + i) += (mat[i][k])*(*(vec+k));
        }
        //printf("result[%d] = %lf\n", i, result[i]);
    }
    for(i = 1; i <= tam; i++)
    {
        *((*pVecResul) + i) = *(result + i);
    }
}

int ArnoldiComGSModificado(int tam) //retorna o valor de j //TESTAR
{
	int i, j;
	double w[MAX], h;
	norma = sqrt(calcularProdutoInterno(mat[1], mat[1], tam));
	if(norma != 1)
	{
		for(i = 1; i <= tam; ++i)
		{
			mat[1][i] /= norma;
		}
	}
	for(j = 1; j <= tam; ++j)
	{
		multiplicarMatrizPorVetor(tam, mat[j], &w);
		for(i = 1; i <= tam; ++i)
		{
			h = calcularProdutoInterno(w, mat[i], tam);
			for(int k = 1; k <= tam; ++k)
			{
				w[k] -= (h*mat[i][k]);
			}
		}
		h = sqrt(calcularProdutoInterno(w, w, tam));
		if(h == 0) break;
		for(int k = 1; k <= tam; ++k)
		{
			mat[j+][w] = w[k]/h;
		}
	}
	return j;
}


int main(){
    
    return 0;
 }
