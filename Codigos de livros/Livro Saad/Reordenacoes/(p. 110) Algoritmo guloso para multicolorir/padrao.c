/* Algoritmo guloso para multicolorir (pag. 110 do Saad):
For i = 1,...,n Do: set Color(i) = 0.
For i = 1,2,...,n Do:
  Set Color (i) = min {k > 0 | k != Color (j),∀ j ∈ Adj(i))}
EndDo
*/

#include <stdio.h>
#include <string.h>

#define MAX 51
double mat[MAX][MAX];
int tam;

void imprimirMatriz(double mat[MAX][MAX], int tam)
{
    int i, j;
    for(i = 1; i <= tam; ++i)
    {
        for(j = 1; j <= tam; ++j)
        {
            printf("%.0lf ", mat[i][j]);
        }
        printf("\n");
    }
}

void imprimirVetor(int vec[MAX], char *nome)
{
    printf("Vetor %s:\n", nome);
    for(int i = 1; i <= tam; ++i)
    {
        printf("%d ", vec[i]);
    }
    printf("\n");
}

void Guloso_Multicolorir() //mat como parametro
{
    int color[MAX];
    int corPossivel[MAX];
    int aux;
    int k;
    int corAtual;
    memset(color, 0, sizeof(color));
    for(int i = 1; i <= tam; ++i)
    {
        memset(corPossivel, 0, sizeof(corPossivel));
        for(int w = 1; w <= tam; ++w)
        {
            if(mat[i][w])
            {
                corPossivel[color[w]] = 1;
            }
        }
        for(k = 1; k <= MAX; ++k)
        {
            if(!corPossivel[k])
            {
                break;
            }
        }
        color[i] = k;
    }
    imprimirVetor(color, "color");
}

int main(){
    int i, j, k;
    int arestas;
    int soma;
    double aux[MAX][MAX];

    memset(mat, 0, sizeof(mat));

    scanf("%d %d\n", &tam, &arestas);
    for(i = 1; i <= tam; ++i)
    {
        mat[i][i] = 1;
    }
    for(k = 0; k < arestas; ++k)
    {
        scanf("%d %d\n", &i, &j);
        mat[i][j] = mat[j][i] = 1;
    }

    printf("Antes da reorganizacao: \n");
    imprimirMatriz(mat, tam);

    Guloso_Multicolorir();

    return 0;
 }
