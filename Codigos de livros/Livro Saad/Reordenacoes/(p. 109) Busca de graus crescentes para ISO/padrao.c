/* Algoritmo de graus crescentes para ISO (pag. 109 do Saad):
Set S = ∅.
Find an ordering i_1,...,i n of the nodes by increasing degree.
For j = 1,2,...n , Do:
  If node i_j is not marked then
    S = S ∪ {i_j}
    Mark i_j and all its nearest neighbors
    EndIf
EndDo
*/

#include <stdio.h>
#include <string.h>

#define MAX 51
double mat[MAX][MAX];
int tam;
int graus[MAX];

void imprimirMatriz(double mat[MAX][MAX], int tam)
{
    int i, j;
    for(i = 1; i <= tam; ++i)
    {
        for(j = 1; j <= tam; ++j)
        {
            printf("%.0lf ", mat[i][j]);
        }
        printf("\n");
    }
}

void imprimirVetor(int vec[MAX], char *nome)
{
    printf("Vetor %s:\n", nome);
    for(int i = 1; i <= tam; ++i)
    {
        printf("%d ", vec[i]);
    }
    printf("\n");
}

void reordenarGraus()
{
    int qtdeGraus[MAX], aux[MAX];
    int w;
    int soma;
    int i, j;
    int count[MAX], aux2[MAX];

    memset(qtdeGraus, 0, sizeof(qtdeGraus));

    for(i = 1; i <= tam; ++i)
    {
        soma = 0;
        for(j = 1; j <= tam; ++j)
        {
            soma += (mat[i][j])?1:0;
        }
        graus[i] = i;
        qtdeGraus[i] = soma-1;
    }

    //imprimirVetor(graus, "graus");
    //imprimirVetor(qtdeGraus, "qtdeGraus");

    //Distribution counting no qtdeGraus
    memset(count, 0, sizeof(count));

    for(i = 1; i <= tam; ++i)
    {
        count[qtdeGraus[i]]++;
    }
    for(i = 1; i < tam; ++i)
    {
        count[i] += count[i-1];
    }
    for(i = tam; i > 0; --i)
    {
        aux[count[qtdeGraus[i]]] = graus[i];
        aux2[count[qtdeGraus[i]]--] = qtdeGraus[i];
    }
    for(i = 1; i <= tam; ++i)
    {
        graus[i] = aux[i];
        qtdeGraus[i] = aux2[i];
    }

    //imprimirVetor(qtdeGraus, "qtdeGraus");
    //imprimirVetor(graus, "graus");

}

  void GrausCrescentes_ISO() //mat como parametro
{
    int S[MAX];
    int tamS = 0;
    int pre[MAX];
    int j;
    memset(pre, 0, sizeof(pre));
    reordenarGraus();
    for(int i = 1; i <= tam; ++i)
    {
        j = graus[i];
        if(!pre[j])
        {
            S[++tamS] = j;
            pre[j] = 1;
            for(int w = 1; w <= tam; ++w)
            {
                if(mat[j][w])
                {
                    pre[w] = 1;
                }
            }
        }
    }
    int aux = tam;
    tam = tamS;
    imprimirVetor(S, "S");
    tam = aux;
}

int main(){
    int i, j, k;
    int arestas;
    int soma;
    double aux[MAX][MAX];

    memset(mat, 0, sizeof(mat));

    scanf("%d %d\n", &tam, &arestas);
    for(i = 1; i <= tam; ++i)
    {
        mat[i][i] = 1;
    }
    for(k = 0; k < arestas; ++k)
    {
        scanf("%d %d\n", &i, &j);
        mat[i][j] = mat[j][i] = 1;
    }

    printf("Antes da reorganizacao: \n");
    imprimirMatriz(mat, tam);

    GrausCrescentes_ISO();

    return 0;
 }
