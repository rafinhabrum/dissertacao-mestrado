/* Algoritmo de Cuthill-McKee (pag. 104 do Saad):
Find an intial node v for the traversal
Initialize S = {v} , seen = 1 , π(seen) = v ; Mark v ;
While seen < n Do
  S_new = ∅ ;
  For each node v Do:
    π(++seen) = v ;
    For each unmarked w in adj( v ), going from lowest to highest degree Do:
      Add w to S_new ;
      Mark w ;
    EndDo
    S := S_new
  EndDo
EndWhile
*/

#include <stdio.h>
#include <string.h>

#define MAX 51
double mat[MAX][MAX];
int tam;
int pi[MAX];
int graus[MAX];

void imprimirMatriz(double mat[MAX][MAX], int tam)
{
    int i, j;
    for(i = 1; i <= tam; ++i)
    {
        for(j = 1; j <= tam; ++j)
        {
            printf("%.0lf ", mat[i][j]);
        }
        printf("\n");
    }
}

void imprimirVetor(int vec[MAX], char *nome)
{
    printf("Vetor %s:\n", nome);
    for(int i = 1; i <= tam; ++i)
    {
        printf("%d ", vec[i]);
    }
    printf("\n");
}

void reordenarGraus()
{
    int qtdeGraus[MAX], aux[MAX];
    int w;
    int soma;
    int i, j;
    int count[MAX], aux2[MAX];

    memset(qtdeGraus, 0, sizeof(qtdeGraus));

    for(i = 1; i <= tam; ++i)
    {
        soma = 0;
        for(j = 1; j <= tam; ++j)
        {
            soma += (mat[i][j])?1:0;
        }
        graus[i] = i;
        qtdeGraus[i] = soma-1;
    }

    //imprimirVetor(graus, "graus");
    //imprimirVetor(qtdeGraus, "qtdeGraus");

    //Distribution counting no qtdeGraus
    memset(count, 0, sizeof(count));

    for(i = 0; i <= tam; ++i)
    {
        count[qtdeGraus[i]]++;
    }
    for(i = 0; i < tam; ++i)
    {
        count[i]+=count[i-1];
    }
    for(i = tam; i > 0; --i)
    {
        aux[--count[qtdeGraus[i]]] = graus[i];
        aux2[count[qtdeGraus[i]]] = qtdeGraus[i];
    }
    for(i = 1; i <= tam; ++i)
    {
        graus[i] = aux[i];
        qtdeGraus[i] = aux2[i];
    }

    //imprimirVetor(qtdeGraus, "qtdeGraus");
    //imprimirVetor(graus, "graus");

}

void Cuthill_McKee() //mat como parametro
{
    int seen = 1;
    int S[MAX], S_new[MAX], indexS, tamSnew;
    int pre[MAX];
    int i, w;
    int v = 3; //encontrar um no inicial
    memset(pre, 0, sizeof(pre));
    indexS = 1;
    S[0] = v;
    pi[seen] = v;
    pre[v] = 1;
    reordenarGraus();
    while(seen < tam)
    {
        tamSnew = 0;
        i = 0;
        for(int k = 0; k < tam; ++k)
        {
            v = S[k];
            for(int j = 1; j <= tam; ++j)
            {
                w = graus[j];
                if((mat[v][w]) && (!pre[w]))
                {
                    S_new[tamSnew++] = w;
                    pre[w] = 1;
                    pi[++seen] = w;
                }
            }
            //S := S_new
            for(; i < tamSnew; ++i)
            {
                S[indexS++] = S_new[i];
            }
        }
    }
}

int main(){
    int i, j, k;
    int arestas;
    int soma;
    double PI[MAX][MAX];
    double aux[MAX][MAX];

    memset(mat, 0, sizeof(mat));

    scanf("%d %d\n", &tam, &arestas);
    for(i = 1; i <= tam; ++i)
    {
        mat[i][i] = 1;
    }
    for(k = 0; k < arestas; ++k)
    {
        scanf("%d %d\n", &i, &j);
        mat[i][j] = mat[j][i] = 1;
    }

    printf("Antes da reorganizacao: \n");
    imprimirMatriz(mat, tam);

    Cuthill_McKee(mat);

    imprimirVetor(pi, "pi");

    memset(PI, 0, sizeof(PI));
    for(i = 1; i <= tam; ++i)
    {
        PI[i][pi[i]] = 1;
    }

    printf("Matriz PI: \n");
    imprimirMatriz(PI, tam);

    //multiplicar PI*mat*PI_Transposta
    for(int i = 1; i <= tam; ++i)
    {
        for(int j = 1; j <= tam; ++j)
        {
            soma = 0;
            for(int k = 1; k <= tam; ++k)
            {
                soma += (PI[i][k]*mat[k][j]);
            }
            aux[i][j] = soma;
        }
    }
    for(int i = 1; i <= tam; ++i)
    {
        for(int j = 1; j <= tam; ++j)
        {
            mat[i][j] = aux[i][j];
        }
    }
    for(int i = 1; i <= tam; ++i)
    {
        for(int j = 1; j <= tam; ++j)
        {
            soma = 0;
            for(int k = 1; k <= tam; ++k)
            {
                soma += (mat[i][k]*PI[j][k]);
            }
            aux[i][j] = soma;
        }
    }
    for(int i = 1; i <= tam; ++i)
    {
        for(int j = 1; j <= tam; ++j)
        {
            mat[i][j] = aux[i][j];
        }
    }

    printf("Depois da reorganizacao: \n");
    imprimirMatriz(mat, tam);

    return 0;
 }
