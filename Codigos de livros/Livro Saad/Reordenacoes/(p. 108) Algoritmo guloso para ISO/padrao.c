/* Algoritmo guloso para ISO (pag. 108 do Saad):
Set S = ∅ .
For j = 1,2,...,n Do:
  If node j is not marked then
    S = S ∪ {j}
    Mark j and all its nearest neighbors
  EndIf
EndDo
*/

#include <stdio.h>
#include <string.h>

#define MAX 51
double mat[MAX][MAX];
int tam;

void imprimirMatriz(double mat[MAX][MAX], int tam)
{
    int i, j;
    for(i = 1; i <= tam; ++i)
    {
        for(j = 1; j <= tam; ++j)
        {
            printf("%.0lf ", mat[i][j]);
        }
        printf("\n");
    }
}

void imprimirVetor(int vec[MAX], char *nome)
{
    printf("Vetor %s:\n", nome);
    for(int i = 1; i <= tam; ++i)
    {
        printf("%d ", vec[i]);
    }
    printf("\n");
}

void Guloso_ISO() //mat como parametro
{
    int S[MAX];
    int tamS = 0;
    int pre[MAX];
    memset(pre, 0, sizeof(pre));
    for(int j = 1; j <= tam; ++j)
    {
        if(!pre[j])
        {
            S[++tamS] = j;
            pre[j] = 1;
            for(int w = 1; w <= tam; ++w)
            {
                if(mat[j][w])
                {
                    pre[w] = 1;
                }
            }
        }
    }
    int aux = tam;
    tam = tamS;
    imprimirVetor(S, "S");
    tam = aux;
}

int main(){
    int i, j, k;
    int arestas;
    int soma;
    double aux[MAX][MAX];

    memset(mat, 0, sizeof(mat));

    scanf("%d %d\n", &tam, &arestas);
    for(i = 1; i <= tam; ++i)
    {
        mat[i][i] = 1;
    }
    for(k = 0; k < arestas; ++k)
    {
        scanf("%d %d\n", &i, &j);
        mat[i][j] = mat[j][i] = 1;
    }

    printf("Antes da reorganizacao: \n");
    imprimirMatriz(mat, tam);

    Guloso_ISO();

    return 0;
 }
