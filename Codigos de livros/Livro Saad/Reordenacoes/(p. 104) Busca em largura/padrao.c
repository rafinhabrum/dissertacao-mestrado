/* Algoritmo de busca em largura (pag. 104 do Saad):
Initialize S = {v} , seen = 1 , π(seen) = v ; Mark v ;
While seen < n Do
  S_new = ∅ ;
  For each node v in S do
    For each unmarked w in adj( v ) do
      Add w to S new ;
      Mark w ;
      π(++seen) = w ;
    EndDo
    S := S new
  EndDo
EndWhile
*/

#include <stdio.h>
#include <string.h>

#define MAX 51
double mat[MAX][MAX];
int tam;
int pi[MAX];

void imprimirMatriz() //mat e tam como parametro
{
    int i, j;
    for(i = 1; i <= tam; ++i)
    {
        for(j = 1; j <= tam; ++j)
        {
            printf("%.0lf ", mat[i][j]);
        }
        printf("\n");
    }
}

void BFS(double mat[MAX][MAX], int v)
{
    int seen = 1;
    int S[MAX], S_new[MAX], indexS, tamSnew;
    int pre[MAX];
    int i;
    memset(pre, 0, sizeof(pre));
    indexS = 1;
    S[0] = v;
    pi[seen] = v;
    pre[v] = 1;
    while(seen < tam)
    {
        tamSnew = 0;
        i = 0;
        for(int k = 0; k < tam; ++k)
        {
            v = S[k];
            for(int w = 1; w <= tam; ++w)
            {
                if((mat[v][w]) && (!pre[w]))
                {
                    S_new[tamSnew++] = w;
                    pre[w] = 1;
                    pi[++seen] = w;
                }
            }
            //S := S_new
            for(; i < tamSnew; ++i)
            {
                S[indexS++] = S_new[i];
            }
        }
    }
}

void imprimirVetorPi()
{
    printf("Vetor pi:\n");
    for(int i = 1; i <= tam; ++i)
    {
        printf("%d ", pi[i]);
    }
    printf("\n");
}

int main(){
    int i, j, k;
    int arestas;
    double PI[MAX][MAX];
    int soma;
    double aux[MAX][MAX];

    memset(mat, 0, sizeof(mat));

    scanf("%d %d\n", &tam, &arestas);
    for(i = 1; i <= tam; ++i)
    {
        mat[i][i] = 1;
    }
    for(k = 0; k < arestas; ++k)
    {
        scanf("%d %d\n", &i, &j);
        mat[i][j] = mat[j][i] = 1;
    }

    printf("Antes da reorganizacao: \n");
    imprimirMatriz(mat, tam);

    BFS(mat, 1);

    imprimirVetorPi();

    memset(PI, 0, sizeof(PI));
    for(i = 1; i <= tam; ++i)
    {
        PI[i][pi[i]] = 1;
    }

    printf("Matriz PI: \n");
    imprimirMatriz(PI, tam);

    //multiplicar PI*mat*PI_Transposta
    for(int i = 1; i <= tam; ++i)
    {
        for(int j = 1; j <= tam; ++j)
        {
            soma = 0;
            for(int k = 1; k <= tam; ++k)
            {
                soma += (PI[i][k]*mat[k][j]);
            }
            aux[i][j] = soma;
        }
    }
    for(int i = 1; i <= tam; ++i)
    {
        for(int j = 1; j <= tam; ++j)
        {
            mat[i][j] = aux[i][j];
        }
    }
    for(int i = 1; i <= tam; ++i)
    {
        for(int j = 1; j <= tam; ++j)
        {
            soma = 0;
            for(int k = 1; k <= tam; ++k)
            {
                soma += (mat[i][k]*PI[j][k]);
            }
            aux[i][j] = soma;
        }
    }
    for(int i = 1; i <= tam; ++i)
    {
        for(int j = 1; j <= tam; ++j)
        {
            mat[i][j] = aux[i][j];
        }
    }

    printf("Depois da reorganizacao: \n");
    imprimirMatriz(mat, tam);

    return 0;
 }
