#!/bin/bash
gcc JOR_5000.c -o JOR_5000
echo "Jacobi Richardson"
echo "N = 5000" | tee jor.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a jor.txt
	COUNTER=$((COUNTER+1))
	./JOR_5000 | tee -a jor.txt
done
gcc JOR_10000.c -o JOR_10000
echo "Jacobi Richardson"
echo "N = 10000" | tee jor.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a jor.txt
	COUNTER=$((COUNTER+1))
	./JOR_10000 | tee -a jor.txt
done
gcc JOR_15000.c -o JOR_15000
echo "Jacobi Richardson"
echo "N = 15000" | tee jor.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a jor.txt
	COUNTER=$((COUNTER+1))
	./JOR_15000 | tee -a jor.txt
done
gcc JOR_20000.c -o JOR_20000
echo "Jacobi Richardson"
echo "N = 20000" | tee jor.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a jor.txt
	COUNTER=$((COUNTER+1))
	./JOR_20000 | tee -a jor.txt
done
echo "Resultado"
echo "N = 2000" | tee jor_resul.txt
gcc JOR_resul.c -o JOR_resul
./JOR_resul | tee -a jor_resul.txt
