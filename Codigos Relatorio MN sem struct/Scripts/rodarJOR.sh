#!/bin/bash
gcc JOR.c -o JOR
echo "JOR"
echo "N = 2000" | tee JOR.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a JOR.txt
	COUNTER=$((COUNTER+1))
	./JOR | tee -a JOR.txt
done
echo "Resultado"
echo "N = 2000" | tee JOR_resul.txt
gcc JOR_resul.c -o JOR_resul
./JOR_resul | tee -a JOR_resul.txt
