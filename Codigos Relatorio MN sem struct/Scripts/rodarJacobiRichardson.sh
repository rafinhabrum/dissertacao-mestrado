#!/bin/bash
gcc JacobiRichardson.c -o JacobiRichardson
echo "Jacobi Richardson"
echo "N = 2000" | tee jacobiRichardson.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a jacobiRichardson.txt
	COUNTER=$((COUNTER+1))
	./JacobiRichardson | tee -a jacobiRichardson.txt
done
echo "Resultado"
echo "N = 2000" | tee jacobiRichardson_resul.txt
gcc JacobiRichardson_resul.c -o JacobiRichardson_resul
./JacobiRichardson_resul | tee -a jacobiRichardson_resul.txt
