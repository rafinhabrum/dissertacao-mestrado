#!/bin/bash
gcc SOR.c -o SOR
echo "SOR"
echo "N = 2000" | tee sor.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a sor.txt
	COUNTER=$((COUNTER+1))
	./SOR | tee -a sor.txt
done
echo "Resultado"
echo "N = 2000" | tee sor_resul.txt
gcc SOR_resul.c -o SOR_resul
./SOR_resul | tee -a sor_resul.txt
