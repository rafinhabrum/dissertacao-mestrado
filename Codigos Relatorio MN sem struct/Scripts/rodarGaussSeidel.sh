#!/bin/bash
mpicc.mpich GaussSeidel_distribuido.c -o GaussSeidel_distribuido
echo "Gauss Seidel distribuido"
echo "N = 2000" | tee gaussSeidel_distribuido.txt
COUNTER=1
while [  $COUNTER -lt 41 ]; do
	echo The counter is $COUNTER | tee -a gaussSeidel_distribuido.txt
	COUNTER=$((COUNTER+1))
	mpirun -n 4 -machines 10.15.1.132 ./GaussSeidel_distribuido | tee -a gaussSeidel_distribuido.txt
done
echo "Resultado"
echo "N = 2000" | tee gaussSeidel_distribuido_resul.txt
mpicc.mpich GaussSeidel_distribuido_resul.c -o GaussSeidel_distribuido_resul
mpirun -n 4 -machines 10.15.1.132 ./GaussSeidel_distribuido_resul | tee -a gaussSeidel_distribuido_resul.txt
gcc GaussSeidel.c -o GaussSeidel
echo "Gauss Seidel"
echo "N = 2000" | tee gaussSeidel.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a gaussSeidel.txt
	COUNTER=$((COUNTER+1))
	./GaussSeidel | tee -a gaussSeidel.txt
done
echo "Resultado"
echo "N = 2000" | tee gaussSeidel_resul.txt
gcc GaussSeidel_resul.c -o GaussSeidel_resul
./GaussSeidel_resul | tee -a gaussSeidel_resul.txt
