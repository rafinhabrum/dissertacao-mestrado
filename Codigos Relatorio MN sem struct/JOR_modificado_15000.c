/*
Matriz que converge:
4.00x1 + 0.24x2 - 0.08x3 = 8.00
0.09x1 + 3.00x2 - 0.15x3 = 9.00
0.04x1 - 0.08x2 + 4.00x3 = 20.00
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sys/time.h>
#define TOL 0.0000000001
#define TRUE -1
#define FALSE 0

typedef struct timeval sTime;

void imprimirMatriz(char *nome, double *A, int tam)
{
	int i, j;
	printf("%s\n", nome);
	for(i = 0; i < tam; i++)
	{
		for(j = 0; j < tam; j++)
		{
			printf("%lf ", A[i*tam + j]);
		}
		printf("\n");
	}
}

void imprimirVetor(char *nome, double *A, int tam)
{
	int i, j;
	printf("%s\n", nome);
	for(i = 0; i < tam; i++)
	{
        printf("%lf\n", A[i]);
	}
}

double calcularTempoGasto(sTime tI, sTime tF)
{
    double result = (tF.tv_sec - tI.tv_sec);
    result += ((tF.tv_usec - tI.tv_usec) / 1000000.0 ); //us para segundos;
    return result;
}

int main()
{
    int i, j, k;
    int tam;
    double *A, *B, *x_ant, *x, *x_prox, *x_jr;
    double *ptAuxA, *ptAuxB, *ptAuxXAnt, *ptAuxX, *ptAuxXProx, *ptAuxXJR;
    double soma, aux, w = 1.831;
    sTime t1, t2;
    int retScan;
    int sign;
    FILE *arquivo;

    gettimeofday(&t1, NULL);

    //retScan = scanf("%d", &(A.m));
    arquivo = fopen("matriz15000x15000.dat", "r");
    if(!arquivo)
    {
        return 0;
    }
    retScan = fscanf(arquivo, "%d", &(tam));

    A = (double*)malloc(tam*tam*sizeof(double));
    B = (double*)malloc(tam*sizeof(double));
    x = (double*)malloc(tam*sizeof(double));
    x_ant = (double*)malloc(tam*sizeof(double));
    x_prox = (double*)malloc(tam*sizeof(double));
    x_jr = (double*)malloc(tam*sizeof(double));

    ptAuxA = A;
    ptAuxB = B;
    ptAuxX = x;
    for(i = 0; i < tam; ++i, ++ptAuxB, ++ptAuxX)
    {
        for(j = 0; j < tam; ++j, ++ptAuxA)
        {
            retScan = fscanf(arquivo, "%lf", (ptAuxA));
            if(i != j)
            	*ptAuxA = -(*ptAuxA);
        }
        retScan = fscanf(arquivo, "%lf", (ptAuxB));
        *ptAuxX = 0;
    }

    fclose(arquivo);

    //imprimirMatriz("Matriz A", A, tam);
    //x(k+1) = D^-1*B + D^-1*L*x(k+1) + D^-1*U*x(k)

		sign = 0;
		k = 1;
		ptAuxXJR = x_prox;
		ptAuxB = B;
		for(i = 0; i < tam; ++i, ++ptAuxXJR, ++ptAuxB)
		{
				//ti
				*ptAuxXJR = *ptAuxB;
				soma = 0;
				ptAuxA = (A+i*tam+i+1);
				ptAuxX = (x+i+1);
				for(j = i+1; j < tam; ++j, ++ptAuxA, ++ptAuxX)
				{
						soma += ((*ptAuxA) * (*ptAuxX));
				}
				*ptAuxXJR += soma;

				//zi
				soma = 0;
				ptAuxA = (A+i*tam);
				ptAuxX = (x);
				for(j = 0; j < i; ++j, ++ptAuxA, ++ptAuxX)
				{
						soma += ((*ptAuxA) * (*ptAuxX));
				}
				*ptAuxXJR += soma;

				//x = 1/A[i][i]*x
				*ptAuxXJR /= *ptAuxA;
		}

		/*ptAuxXProx = x_prox;
		ptAuxXJR = x_jr;
		ptAuxX = x;
		for(i = 0; i < tam; ++i, ++ptAuxX, ++ptAuxXProx, ++ptAuxXJR)
		{
//				*ptAuxXProx = (1-w)*(*ptAuxX) + w*(*ptAuxXJR);
				*ptAuxXProx = (*ptAuxXJR);
		}*/

		sign = 0;
		ptAuxXProx = x_prox;
		ptAuxX = x;
		for(i = 0; i < tam; ++i, ++ptAuxXProx, ++ptAuxX)
		{
				aux = *ptAuxXProx - *ptAuxX;
				//printf("i: %d\nx_prox-x = %lf\n", i, aux);
				//if((aux > TOL) || (aux < -TOL))
				if(!sign)
				{
						if(fabs(aux) > TOL)
						{
								sign = 1;
						}
				}
		}

		ptAuxX = x;
		ptAuxXProx = x_prox;
		ptAuxXAnt = x_ant;
		for(i = 0; i < tam; ++i, ++ptAuxX, ++ptAuxXProx, ++ptAuxXAnt)
		{
				*ptAuxXAnt = *ptAuxX;
				*ptAuxX = *ptAuxXProx;
		}

		if(sign)
		{
			for(k = 2; k < 1000000; ++k)
	    {
	    		//printf("k = %d\n", k);
	        sign = 0;
	        ptAuxXJR = x_jr;
	        ptAuxB = B;
	        for(i = 0; i < tam; ++i, ++ptAuxXJR, ++ptAuxB)
	        {
	            //ti
	            *ptAuxXJR = *ptAuxB;
	            soma = 0;
	            ptAuxA = (A+i*tam+i+1);
	            ptAuxX = (x+i+1);
	            for(j = i+1; j < tam; ++j, ++ptAuxA, ++ptAuxX)
	            {
	                soma += ((*ptAuxA) * (*ptAuxX));
	            }
	            *ptAuxXJR += soma;

	            //zi
	            soma = 0;
	            ptAuxA = (A+i*tam);
	            ptAuxX = (x);
	            for(j = 0; j < i; ++j, ++ptAuxA, ++ptAuxX)
	            {
	                soma += ((*ptAuxA) * (*ptAuxX));
	            }
	            *ptAuxXJR += soma;

	            //x = 1/A[i][i]*x
	            *ptAuxXJR /= *ptAuxA;
	        }

	        ptAuxXProx = x_prox;
	        ptAuxXAnt = x_ant;
					ptAuxXJR = x_jr;
	        ptAuxX = x;
	        for(i = 0; i < tam; ++i, ++ptAuxX, ++ptAuxXProx, ++ptAuxXJR, ++ptAuxXAnt)
	        {
	            *ptAuxXProx = (1-w)*(*ptAuxXAnt) + w*(*ptAuxXJR);
	        }

	        sign = 0;
	        ptAuxXProx = x_prox;
	        ptAuxX = x;
	        for(i = 0; i < tam; ++i, ++ptAuxXProx, ++ptAuxX)
	        {
	            aux = *ptAuxXProx - *ptAuxX;
	            if(!i) printf("k = %d\ni: %d\nx_prox-x = %.10lf\n", k, i, aux);
	            //if((aux > TOL) || (aux < -TOL))
	            if(!sign)
	            {
	                if(fabs(aux) > TOL)
	                {
	                    sign = 1;
	                }
	            }
	        }

	        ptAuxX = x;
	        ptAuxXProx = x_prox;
					ptAuxXAnt = x_ant;
	        for(i = 0; i < tam; ++i, ++ptAuxX, ++ptAuxXProx, ++ptAuxXAnt)
	        {
							*ptAuxXAnt = *ptAuxX;
	            *ptAuxX = *ptAuxXProx;
	        }

	        if(!sign)
	        {
	            break;
	        }
	    }
		}

    gettimeofday(&t2, NULL);
    printf("K = %d\n", k);
    //imprimirVetor("Vetor x final", x, tam);
    printf("\ntime = %lf s\n\n", calcularTempoGasto(t1, t2));


    free(A);
    free(B);
    free(x);
		free(x_ant);
    free(x_prox);
		free(x_jr);

    return 0;
}
