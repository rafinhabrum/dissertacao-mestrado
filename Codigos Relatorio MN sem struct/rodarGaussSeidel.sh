#!/bin/bash
mpicc.mpich GaussSeidel_distribuido_5000.c -o GaussSeidel_distribuido_5000
echo "Gauss Seidel distribuido"
echo "N = 5000" | tee gaussSeidel_distribuido.txt
COUNTER=1
while [  $COUNTER -lt 41 ]; do
	echo The counter is $COUNTER | tee -a gaussSeidel_distribuido.txt
	COUNTER=$((COUNTER+1))
	mpirun -n 4 -machines 10.15.1.132 ./GaussSeidel_distribuido_5000 | tee -a gaussSeidel_distribuido.txt
done
mpicc.mpich GaussSeidel_distribuido_10000.c -o GaussSeidel_distribuido_10000
echo "Gauss Seidel distribuido"
echo "N = 10000" | tee gaussSeidel_distribuido.txt
COUNTER=1
while [  $COUNTER -lt 41 ]; do
	echo The counter is $COUNTER | tee -a gaussSeidel_distribuido.txt
	COUNTER=$((COUNTER+1))
	mpirun -n 4 -machines 10.15.1.132 ./GaussSeidel_distribuido_10000 | tee -a gaussSeidel_distribuido.txt
done
mpicc.mpich GaussSeidel_distribuido_15000.c -o GaussSeidel_distribuido_15000
echo "Gauss Seidel distribuido"
echo "N = 15000" | tee gaussSeidel_distribuido.txt
COUNTER=1
while [  $COUNTER -lt 41 ]; do
	echo The counter is $COUNTER | tee -a gaussSeidel_distribuido.txt
	COUNTER=$((COUNTER+1))
	mpirun -n 4 -machines 10.15.1.132 ./GaussSeidel_distribuido_15000 | tee -a gaussSeidel_distribuido.txt
done
mpicc.mpich GaussSeidel_distribuido_20000.c -o GaussSeidel_distribuido_20000
echo "Gauss Seidel distribuido"
echo "N = 20000" | tee gaussSeidel_distribuido.txt
COUNTER=1
while [  $COUNTER -lt 41 ]; do
	echo The counter is $COUNTER | tee -a gaussSeidel_distribuido.txt
	COUNTER=$((COUNTER+1))
	mpirun -n 4 -machines 10.15.1.132 ./GaussSeidel_distribuido_20000 | tee -a gaussSeidel_distribuido.txt
done
echo "Resultado"
echo "N = 2000" | tee gaussSeidel_distribuido_resul.txt
mpicc.mpich GaussSeidel_distribuido_resul.c -o GaussSeidel_distribuido_resul
mpirun -n 4 -machines 10.15.1.132 ./GaussSeidel_distribuido_resul | tee -a gaussSeidel_distribuido_resul.txt
#!/bin/bash
gcc GaussSeidel_5000.c -o GaussSeidel_5000
echo "Jacobi Richardson"
echo "N = 5000" | tee gaussSeidel.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a gaussSeidel.txt
	COUNTER=$((COUNTER+1))
	./GaussSeidel_5000 | tee -a gaussSeidel.txt
done
gcc GaussSeidel_10000.c -o GaussSeidel_10000
echo "Jacobi Richardson"
echo "N = 10000" | tee gaussSeidel.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a gaussSeidel.txt
	COUNTER=$((COUNTER+1))
	./GaussSeidel_10000 | tee -a gaussSeidel.txt
done
gcc GaussSeidel_15000.c -o GaussSeidel_15000
echo "Jacobi Richardson"
echo "N = 15000" | tee gaussSeidel.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a gaussSeidel.txt
	COUNTER=$((COUNTER+1))
	./GaussSeidel_15000 | tee -a gaussSeidel.txt
done
gcc GaussSeidel_20000.c -o GaussSeidel_20000
echo "Jacobi Richardson"
echo "N = 20000" | tee gaussSeidel.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a gaussSeidel.txt
	COUNTER=$((COUNTER+1))
	./GaussSeidel_20000 | tee -a gaussSeidel.txt
done
echo "Resultado"
echo "N = 2000" | tee gaussSeidel_resul.txt
gcc GaussSeidel_resul.c -o GaussSeidel_resul
./GaussSeidel_resul | tee -a gaussSeidel_resul.txt
