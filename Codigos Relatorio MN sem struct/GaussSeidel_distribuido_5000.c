/*
Matriz que converge:
4.00x1 + 0.24x2 - 0.08x3 = 8.00
0.09x1 + 3.00x2 - 0.15x3 = 9.00
0.04x1 - 0.08x2 + 4.00x3 = 20.00
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sys/time.h>
#include "mpi.h"
//#define TOL 0.0000000001
#define TOL 0.01
#define TRUE -1
#define FALSE 0

typedef struct timeval sTime;

void imprimirMatriz(char *nome, double *A, int tam)
{
	int i, j;
	printf("%s\n", nome);
	for(i = 0; i < tam; i++)
	{
		for(j = 0; j < tam; j++)
		{
			printf("%lf ", A[i*tam + j]);
		}
		printf("\n");
	}
}

void imprimirVetor(char *nome, double *A, int tam)
{
	int i, j;
	printf("%s\n", nome);
	for(i = 0; i < tam; i++)
	{
        printf("%lf\n", A[i]);
	}
}

double calcularTempoGasto(sTime tI, sTime tF)
{
    double result = (tF.tv_sec - tI.tv_sec);
    result += ((tF.tv_usec - tI.tv_usec) / 1000000.0 ); //us para segundos;
    return result;
}

int main(int argc, char *argv[])
{
    int i, j, k;
    int tam;
    double *A, *B, *x, *x_prox;
    double *ptAuxA, *ptAuxB, *ptAuxX, *ptAuxXProx;
    double soma, aux;
    sTime t1, t2;
    double t1MPI, t2MPI;
    int numTasks, rank, rc, inicio, fim, bloco, g;
    int tag = 1, retScan;
    int sign;
    MPI_Status Stat;
    FILE *arquivo;

    rc = MPI_Init(&argc, &argv);
    if(rc != MPI_SUCCESS)
    {
        fprintf(stderr, "Erro ao iniciar programa MPI.\n");
        MPI_Abort(MPI_COMM_WORLD, rc);
    }

    MPI_Comm_size(MPI_COMM_WORLD, &numTasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    gettimeofday(&t1, NULL);
    t1MPI = MPI_Wtime();

    if(rank == 0)
    {
        //retScan = scanf("%d", &(A.m));
        arquivo = fopen("matriz5000x5000.dat", "r");
        if(!arquivo)
        {
            return 0;
        }
        retScan = fscanf(arquivo, "%d", &(tam));

        A = (double*)malloc(tam*tam*sizeof(double));
        B = (double*)malloc(tam*sizeof(double));
        x = (double*)malloc(tam*sizeof(double));
        //x_prox = (double*)malloc(tam*sizeof(double));

        ptAuxA = A;
        ptAuxB = B;
        ptAuxX = x;
        for(i = 0; i < tam; ++i, ++ptAuxB, ++ptAuxX)
        {
            for(j = 0; j < tam; ++j, ++ptAuxA)
            {
                retScan = fscanf(arquivo, "%lf", (ptAuxA));
            }
            retScan = fscanf(arquivo, "%lf", (ptAuxB));
            *ptAuxX = 0;
        }

        fclose(arquivo);

        //printf("Li o arquivo todo!\n");
        ////fflush(stdout);

        g = numTasks-1;
        bloco = tam/g;
        for(i = 1; i < numTasks; ++i)
        {
            //printf("Enviando para o processo %d\n", i);
            ////fflush(stdout);
            rc = MPI_Send(&(tam), 1, MPI_INT, i, tag, MPI_COMM_WORLD);
            inicio = bloco * (i-1);
            fim = (i+1 == numTasks)?tam:inicio+bloco;
            rc = MPI_Send(&(inicio), 1, MPI_INT, i, tag, MPI_COMM_WORLD);
            rc = MPI_Send(&(fim), 1, MPI_INT, i, tag, MPI_COMM_WORLD);
            //printf("Enviando A\n");
            //fflush(stdout);
            rc = MPI_Send((A+inicio*tam), (fim-inicio)*tam, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
            //printf("Enviando B\n");
            //fflush(stdout);
            rc = MPI_Send((B+inicio), (fim-inicio), MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
            /*for(j = inicio; j < fim; ++j)
            {
                rc = MPI_Send(A.dados[j], A.n, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
                rc = MPI_Send(B.dados[j], 1, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
            }*/
            //printf("Enviando x\n");
            //fflush(stdout);
            rc = MPI_Send((x), tam, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
            /*for(j = 0; j < x.m; ++j)
            {
                rc = MPI_Send(x.dados[j], 1, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
            }*/
        }
        free(A);
        free(B);
        rc = MPI_Recv(x, tam, MPI_DOUBLE, numTasks-1, tag, MPI_COMM_WORLD, &Stat);
        /*for(j = 0; j < x.m; ++j)
        {
            rc = MPI_Recv(x.dados[j], 1, MPI_DOUBLE, numTasks-1, tag, MPI_COMM_WORLD, &Stat);
        }*/
//    printf("Rank = %d\n", rank);
//    imprimirMatriz("Matriz A", A);

        //rc = MPI_Recv(&(A.m), 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &Stat);

        gettimeofday(&t2, NULL);
        t2MPI = MPI_Wtime();

        //imprimirMatriz("Vetor x final", x);
        //printf("Tempo = %lf s\n", t2MPI-t1MPI);
        printf("\ntime = %lf s\n\n", calcularTempoGasto(t1, t2));
    }
    else
    {
        //printf("Vou dormir processo %d!\n", rank);
        //fflush(stdout);
        //sleep(1000);
        //printf("Acordei processo %d!\n", rank);
        //fflush(stdout);
        rc = MPI_Recv(&(tam), 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &Stat);
//        //printf("rank = %d - receber inicio\n", rank);
        rc = MPI_Recv(&(inicio), 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &Stat);
//        //printf("rank = %d - receber fim\n", rank);
        rc = MPI_Recv(&(fim), 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &Stat);

        //printf("Vou dormir processo %d!\n", rank);
        //fflush(stdout);
        //sleep(2000);
        //printf("Acordei processo %d!\n", rank);
        //fflush(stdout);

        int bloco_rank = fim-inicio;
        A = (double*)malloc((bloco_rank)*tam*sizeof(double));
        B = (double*)malloc((bloco_rank)*sizeof(double));
        x = (double*)malloc(tam*sizeof(double));
        x_prox = (double*)malloc(tam*sizeof(double));

//        printf("rank = %d - receber A\n", rank);
        rc = MPI_Recv((A), (bloco_rank)*tam, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &Stat);
//        printf("rank = %d - receber B\n", rank);
        rc = MPI_Recv((B), (bloco_rank), MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &Stat);

//        printf("rank = %d - receber x\n", rank);
        rc = MPI_Recv(x, tam, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &Stat);
//        printf("rank = %d - recebeu tudo\n", rank);

        for(k = 1; k < 1000000; k++)
        {
//            printf("rank = %d - k = %d\n", rank, k);
//            imprimirMatriz("Vetor x", x);
            sign = 0;
            ptAuxXProx = (x_prox+inicio);
            ptAuxB = (B);
            for(i = 0; i < (bloco_rank); ++i, ++ptAuxXProx, ++ptAuxB)
            {
                //ti
                *ptAuxXProx = *ptAuxB;
                soma = 0;
                ptAuxA = (A+i*tam+i+inicio+1);
                ptAuxX = (x+inicio+i+1);
                for(j = inicio+i+1; j < tam; ++j, ++ptAuxA, ++ptAuxX)
                {
                    soma += ((*ptAuxA) * (*ptAuxX));
                }
            *ptAuxXProx -= soma;
            }
            //printf("Task %d - k = %d - sign: %d\n", rank, k, sign);

            if(rank > 1)
            {
                rc = MPI_Recv(x_prox, inicio, MPI_DOUBLE, rank-1, k, MPI_COMM_WORLD, &Stat);
                /*for(j = 0; j < inicio; ++j)
                {
                    rc = MPI_Recv(x_prox.dados[j], 1, MPI_DOUBLE, rank-1, k, MPI_COMM_WORLD, &Stat);
                }*/
                rc = MPI_Recv(&(sign), 1, MPI_INT, rank-1, k, MPI_COMM_WORLD, &Stat);
            }

            ptAuxXProx = (x_prox+inicio);
            ptAuxB = (B);
            for(i = 0; i < bloco_rank; ++i, ++ptAuxXProx, ++ptAuxB)
            {
                //zi
                soma = 0;
                ptAuxA = (A+i*tam);
                ptAuxX = (x_prox);
                for(j = 0; j < i+inicio; ++j, ++ptAuxA, ++ptAuxX)
                {
                    soma += ((*ptAuxA) * (*ptAuxX));
                }
            *ptAuxXProx -= soma;

                //x = 1/A[i][i]*x
                *ptAuxXProx /= *ptAuxA;
            }

            if(!sign)
            {
                ptAuxXProx = (x_prox+inicio);
                ptAuxX = (x+inicio);
                for(i = inicio; i < fim; ++i, ++ptAuxXProx, ++ptAuxX)
                {
                    aux = (*ptAuxXProx) - (*ptAuxX);
                    //printf("i: %d\nx_prox-x = %lf\n", i, aux);
                    //if((aux > TOL) || (aux < -TOL))
                    if(fabs(aux) > TOL)
                    {
                        sign = 1;
                        break;
                    }
                }
            }

            //printf("Task %d - k = %d - sign: %d\n", rank, k, sign);

            if(rank+1 != numTasks)
            {
                rc = MPI_Send(x_prox, fim, MPI_DOUBLE, rank+1, k, MPI_COMM_WORLD);
                /*for(j = 0; j < fim; ++j)
                {
                    rc = MPI_Send(x_prox.dados[j], 1, MPI_DOUBLE, rank+1, k, MPI_COMM_WORLD);
                }*/
                rc = MPI_Send(&(sign), 1, MPI_INT, rank+1, k, MPI_COMM_WORLD);
                //x = x_prox;

                rc = MPI_Recv(&(sign), 1, MPI_INT, numTasks-1, tag, MPI_COMM_WORLD, &Stat);
                if(sign)
                {
                    rc = MPI_Recv(x, tam, MPI_DOUBLE, numTasks-1, tag, MPI_COMM_WORLD, &Stat);
                	/*for(j = 0; j < x.m; ++j)
	                {
	                    rc = MPI_Recv(x.dados[j], 1, MPI_DOUBLE, numTasks-1, tag, MPI_COMM_WORLD, &Stat);
	                }*/
                }
                else
                {
                    break;
                }
            }
            else
            {
                //printf("k = %d sign = %d\n", k, sign);
                //imprimirMatriz("Vetor x", x_prox);
                ptAuxX = x;
                ptAuxXProx = x_prox;
                for(i = 0; i < tam; ++i, ++ptAuxX, ++ptAuxXProx)
                {
                    *ptAuxX = *ptAuxXProx;
                }

                for(i = 1; i < rank; ++i)
                {
                       rc = MPI_Send(&(sign), 1, MPI_INT, i, tag, MPI_COMM_WORLD);
                }
                if(sign)
                {
                    for(i = 1; i < rank; ++i)
                    {
                        rc = MPI_Send(x, tam, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
                        /*for(j = 0; j < x.m; ++j)
                        {
                            rc = MPI_Send(x.dados[j], 1, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
                        }*/
                    }
                }
                else
                {
                    rc = MPI_Send(x, tam, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
                    /*for(j = 0; j < x.m; ++j)
                    {
                        rc = MPI_Send(x.dados[j], 1, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
                    }*/
                    break;
                }
            }
        }
        free(A);
        free(B);
        free(x_prox);
    }


    free(x);
    printf("Rank = %d - k = %d\n", rank, k);
//    imprimirMatriz("Matriz A", A, tam);

    //x(k+1) = D^-1(B - L*x(k+1) - U*x(k))

    MPI_Finalize();

    return 0;
}
