#!/bin/bash
gcc JOR_modificado_5000.c -o JOR_modificado_5000
echo "Jacobi Richardson"
echo "N = 5000" | tee jor_modificado.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a jor_modificado.txt
	COUNTER=$((COUNTER+1))
	./JOR_modificado_5000 | tee -a jor_modificado.txt
done
gcc JOR_modificado_10000.c -o JOR_modificado_10000
echo "Jacobi Richardson"
echo "N = 10000" | tee jor_modificado.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a jor_modificado.txt
	COUNTER=$((COUNTER+1))
	./JOR_modificado_10000 | tee -a jor_modificado.txt
done
gcc JOR_modificado_15000.c -o JOR_modificado_15000
echo "Jacobi Richardson"
echo "N = 15000" | tee jor_modificado.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a jor_modificado.txt
	COUNTER=$((COUNTER+1))
	./JOR_modificado_15000 | tee -a jor_modificado.txt
done
gcc JOR_modificado_20000.c -o JOR_modificado_20000
echo "Jacobi Richardson"
echo "N = 20000" | tee jor_modificado.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a jor_modificado.txt
	COUNTER=$((COUNTER+1))
	./JOR_modificado_20000 | tee -a jor_modificado.txt
done
echo "Resultado"
echo "N = 2000" | tee jor_modificado_resul.txt
gcc JOR_modificado_resul.c -o JOR_modificado_resul
./JOR_modificado_resul | tee -a jor_modificado_resul.txt
