#!/bin/bash
gcc JacobiRichardson_5000.c -o JacobiRichardson_5000
echo "Jacobi Richardson"
echo "N = 5000" | tee jacobiRichardson.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a jacobiRichardson.txt
	COUNTER=$((COUNTER+1))
	./JacobiRichardson_5000 | tee -a jacobiRichardson.txt
done
gcc JacobiRichardson_10000.c -o JacobiRichardson_10000
echo "Jacobi Richardson"
echo "N = 10000" | tee jacobiRichardson.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a jacobiRichardson.txt
	COUNTER=$((COUNTER+1))
	./JacobiRichardson_10000 | tee -a jacobiRichardson.txt
done
gcc JacobiRichardson_15000.c -o JacobiRichardson_15000
echo "Jacobi Richardson"
echo "N = 15000" | tee jacobiRichardson.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a jacobiRichardson.txt
	COUNTER=$((COUNTER+1))
	./JacobiRichardson_15000 | tee -a jacobiRichardson.txt
done
gcc JacobiRichardson_20000.c -o JacobiRichardson_20000
echo "Jacobi Richardson"
echo "N = 20000" | tee jacobiRichardson.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a jacobiRichardson.txt
	COUNTER=$((COUNTER+1))
	./JacobiRichardson_20000 | tee -a jacobiRichardson.txt
done
echo "Resultado"
echo "N = 2000" | tee jacobiRichardson_resul.txt
gcc JacobiRichardson_resul.c -o JacobiRichardson_resul
./JacobiRichardson_resul | tee -a jacobiRichardson_resul.txt
