#!/bin/bash
gcc SOR_5000.c -o SOR_5000
echo "Jacobi Richardson"
echo "N = 5000" | tee sor.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a sor.txt
	COUNTER=$((COUNTER+1))
	./SOR_5000 | tee -a sor.txt
done
gcc SOR_10000.c -o SOR_10000
echo "Jacobi Richardson"
echo "N = 10000" | tee sor.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a sor.txt
	COUNTER=$((COUNTER+1))
	./SOR_10000 | tee -a sor.txt
done
gcc SOR_15000.c -o SOR_15000
echo "Jacobi Richardson"
echo "N = 15000" | tee sor.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a sor.txt
	COUNTER=$((COUNTER+1))
	./SOR_15000 | tee -a sor.txt
done
gcc SOR_20000.c -o SOR_20000
echo "Jacobi Richardson"
echo "N = 20000" | tee sor.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER | tee -a sor.txt
	COUNTER=$((COUNTER+1))
	./SOR_20000 | tee -a sor.txt
done
echo "Resultado"
echo "N = 2000" | tee sor_resul.txt
gcc SOR_resul.c -o SOR_resul
./SOR_resul | tee -a sor_resul.txt
