/*
Matriz que converge:
4.00x1 + 0.24x2 - 0.08x3 = 8.00
0.09x1 + 3.00x2 - 0.15x3 = 9.00
0.04x1 - 0.08x2 + 4.00x3 = 20.00
*/

#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#define TOL 0.0000000001
#define TRUE -1
#define FALSE 0

typedef struct m
{
    int m, n; //matriz mxn
    double dados[200][200];
} matriz;

typedef struct timeval sTime;

matriz multiplicacaoMatrizes(matriz A, matriz B)
{
    int i, j, k;
    matriz M;
    M.m = A.m;
    M.n = B.n;

    for(i = 0; i < M.m; i++)
    {
        for(j = 0; j < M.n; j++)
        {
            M.dados[i][j] = 0;
            for(k = 0; k < A.n; k++)
            {
                M.dados[i][j] += (A.dados[i][k]*B.dados[k][j]);
            }
        }
    }

    return M;
}

void imprimirMatriz(char *nome, matriz A)
{
	int i, j;
	printf("%s\n", nome);
	for(i = 0; i < A.m; i++)
	{
		for(j = 0; j < A.n; j++)
		{
			printf("%lf ", A.dados[i][j]);
		}
		printf("\n");
	}
}

int testarIgualdade(matriz A, matriz B)
{
    int i;
    for(i = 0; i < A.n; i++)
    {
		printf("dif: %lf\n", fabs(A.dados[i][0] - B.dados[i][0]));
        if(fabs(A.dados[i][0] - B.dados[i][0]) > TOL)
        {
            return FALSE;
        }
    }
    return TRUE;
}

matriz calcularJacobiRichardson(matriz x, matriz A, matriz B)
{
	int i, j;
    double soma;
	matriz x_jr;
	x_jr.m = x.m;
    x_jr.n = x.n;
	for(i = 0; i < A.m; ++i)
    {
        //ti
        x_jr.dados[i][0] = B.dados[i][0];
        soma = 0;
        for(j = i+1; j < A.n; j++)
        {
            soma += (A.dados[i][j] * x.dados[j][0]);
        }
        x_jr.dados[i][0] -= soma;

        //zi
        soma = 0;
        for(j = 0; j < i; j++)
        {
            soma += (A.dados[i][j] * x.dados[j][0]);
        }
        x_jr.dados[i][0] -= soma;

        //x = 1/A[i][i]*x
        x_jr.dados[i][0] /= A.dados[i][i];
    }
	return x_jr;
}

matriz calcularGaussSeidel(matriz x, matriz A, matriz B)
{
	int i, j;
  double soma;
	matriz x_gs;
	x_gs.m = x.m;
    x_gs.n = x.n;
	for(i = 0; i < A.m; ++i)
    {
        //ti
        x_gs.dados[i][0] = B.dados[i][0];
        soma = 0;
        for(j = i+1; j < A.n; j++)
        {
            soma += (A.dados[i][j] * x.dados[j][0]);
        }
        x_gs.dados[i][0] -= soma;

        //zi
        soma = 0;
        for(j = 0; j < i; j++)
        {
            soma += (A.dados[i][j] * x_gs.dados[j][0]);
        }
        x_gs.dados[i][0] -= soma;

        //x = 1/A[i][i]*x
        x_gs.dados[i][0] /= A.dados[i][i];
    }
	return x_gs;
}

double calcularTempoGasto(sTime tI, sTime tF)
{
    double result = (tF.tv_sec - tI.tv_sec);
    result += ((tF.tv_usec - tI.tv_usec) / 1000000.0 ); //us para segundos;
    return result;
}

int main()
{
    int i, j, k;
    matriz A, B, x, res, x_prox, x_gs, x_jr;
    double w = 0.95, aux;
    sTime t1, t2;
    int retScan;
    int sign;
    FILE *arquivo;

    gettimeofday(&t1, NULL);

    //retScan = scanf("%d", &(A.m));
    arquivo = fopen("matriz2000x2000.dat", "r");
	if(!arquivo)
    {
        return 0;
    }
	retScan = fscanf(arquivo, "%d", &(A.m));

    A.n = A.m;
    x.m = B.m = A.m;
    x.n = B.n = 1;
    for(i = 0; i < A.m; i++)
    {
        for(j = 0; j < A.n; j++)
        {
            retScan = fscanf(arquivo, "%lf", &(A.dados[i][j]));
        }
        retScan = fscanf(arquivo, "%lf", &B.dados[i][0]);
        x.dados[i][0] = 0;
    }

    fclose(arquivo);


    //x(k+1) = D^-1*B - D^-1*L*x(k+1) - D^-1*U*x(k)

    for(k = 1; k < 1000000; k++)
    {
        //printf("k = %d\n", k);
        //imprimirMatriz("Vetor x", x);

        x_prox.m = x.m;
        x_prox.n = x.n;
        x_jr = calcularJacobiRichardson(x, A, B);
        x_gs = calcularGaussSeidel(x, A, B);

        for(i = 0; i < x_prox.m; ++i)
        {
            x_prox.dados[i][0] = (1-w)*x.dados[i][0] + w*x_gs.dados[i][0];
        }

        sign = 0;

        for(i = 0; i < x.m; ++i)
        {
            aux = x_prox.dados[i][0] - x.dados[i][0];
            //printf("i: %d\nx_prox-x = %lf\n", i, aux);
            //if((aux > TOL) || (aux < -TOL))
            if(!sign)
            {
                if(fabs(aux) > TOL)
                {
                    sign = 1;
                }
            }
        }

        x = x_prox;

        if(!sign)
        {
            break;
        }
    }

    gettimeofday(&t2, NULL);
    //printf("K = %d\n", k);
    //imprimirMatriz("Vetor x final", x);
    printf("\ntime = %lf s\n\n", calcularTempoGasto(t1, t2));

    return 0;
}
