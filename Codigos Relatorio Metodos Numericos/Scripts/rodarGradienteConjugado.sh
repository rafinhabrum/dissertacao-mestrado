#!/bin/bash
gcc GradienteConjugado.c -o GradienteConjugado
echo "Gradiente Conjugado"
echo "N = 2000" > gradienteConjugado.txt
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo The counter is $COUNTER
	echo The counter is $COUNTER >> gradienteConjugado.txt
	COUNTER=$((COUNTER+1))
	./GradienteConjugado >> gradienteConjugado.txt
done
echo "Resultado"
echo "N = 2000" > gradienteConjugado_resul.txt
gcc GradienteConjugado_resul.c -o GradienteConjugado_resul
./GradienteConjugado_resul >> gradienteConjugado_resul.txt
