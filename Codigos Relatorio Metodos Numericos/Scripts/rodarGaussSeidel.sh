#!/bin/bash
mpicc.mpich GaussSeidel_distribuido.c -o GaussSeidel_distribuido
echo "Gauss Seidel distribuido"
echo "N = 2000" > gaussSeidel_distribuido.txt
COUNTER=1
while [  $COUNTER -lt 21 ]; do
	echo The counter is $COUNTER
	echo The counter is $COUNTER >> gaussSeidel_distribuido.txt
	COUNTER=$((COUNTER+1))
	mpirun -n 4 -machines 10.15.1.132 ./GaussSeidel_distribuido >> gaussSeidel_distribuido.txt
done
echo "Resultado"
echo "N = 2000" > gaussSeidel_distribuido_resul.txt
mpicc.mpich GaussSeidel_distribuido_resul.c -o GaussSeidel_distribuido_resul
mpirun -n 4 -machines 10.15.1.132 ./GaussSeidel_distribuido_resul >> gaussSeidel_distribuido_resul.txt
gcc GaussSeidel.c -o GaussSeidel
echo "Gauss Seidel"
echo "N = 2000" > gaussSeidel.txt
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo The counter is $COUNTER
	echo The counter is $COUNTER >> gaussSeidel.txt
	COUNTER=$((COUNTER+1))
	./GaussSeidel >> gaussSeidel.txt
done
echo "Resultado"
echo "N = 2000" > gaussSeidel_resul.txt
gcc GaussSeidel_resul.c -o GaussSeidel_resul
./GaussSeidel_resul >> gaussSeidel_resul.txt
