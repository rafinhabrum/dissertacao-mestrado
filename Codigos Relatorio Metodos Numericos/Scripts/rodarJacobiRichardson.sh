#!/bin/bash
gcc JacobiRichardson.c -o JacobiRichardson
echo "Jacobi Richardson"
echo "N = 2000" > jacobiRichardson.txt
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo The counter is $COUNTER
	echo The counter is $COUNTER >> jacobiRichardson.txt
	COUNTER=$((COUNTER+1))
	./JacobiRichardson >> jacobiRichardson.txt
done
echo "Resultado"
echo "N = 2000" > jacobiRichardson_resul.txt
gcc JacobiRichardson_resul.c -o JacobiRichardson_resul
./JacobiRichardson_resul >> jacobiRichardson_resul.txt
