/*
Matriz que converge:
4.00x1 + 0.24x2 - 0.08x3 = 8.00
0.09x1 + 3.00x2 - 0.15x3 = 9.00
0.04x1 - 0.08x2 + 4.00x3 = 20.00
*/

#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include "mpi.h"
#define TOL 0.0000000001
#define TRUE -1
#define FALSE 0

typedef struct m
{
    int m, n; //matriz mxn
    double dados[250][250];
} matriz;

typedef struct timeval sTime;

matriz multiplicacaoMatrizes(matriz A, matriz B)
{
    int i, j, k;
    matriz M;
    M.m = A.m;
    M.n = B.n;

    for(i = 0; i < M.m; i++)
    {
        for(j = 0; j < M.n; j++)
        {
            M.dados[i][j] = 0;
            for(k = 0; k < A.n; k++)
            {
                M.dados[i][j] += (A.dados[i][k]*B.dados[k][j]);
            }
        }
    }

    return M;
}

void imprimirMatriz(char *nome, matriz A)
{
	int i, j;
	printf("%s\n", nome);
	for(i = 0; i < A.m; i++)
	{
		for(j = 0; j < A.n; j++)
		{
			printf("%lf ", A.dados[i][j]);
		}
		printf("\n");
	}
}

double calcularTempoGasto(sTime tI, sTime tF)
{
    double result = (tF.tv_sec - tI.tv_sec);
    result += ((tF.tv_usec - tI.tv_usec) / 1000000.0 ); //us para segundos;
    return result;
}

int main(int argc, char *argv[])
{
    int i, j, k;
    matriz A, B, x, res, x_prox;
    double soma, aux;
    sTime t1, t2;
    double t1MPI, t2MPI;
    int numTasks, rank, rc, inicio, fim, bloco, g;
    int tag = 1, retScan;
    int sign;
    MPI_Status Stat;
FILE *arquivo;

    rc = MPI_Init(&argc, &argv);
    if(rc != MPI_SUCCESS)
    {
        fprintf(stderr, "Erro ao iniciar programa MPI.\n");
        MPI_Abort(MPI_COMM_WORLD, rc);
    }

    MPI_Comm_size(MPI_COMM_WORLD, &numTasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    gettimeofday(&t1, NULL);
    t1MPI = MPI_Wtime();

    if(rank == 0)
    {
        //retScan = scanf("%d", &(A.m));
	arquivo = fopen("matriz2000x2000.dat", "r");
	if(!arquivo)
{
	return 0;
}
	retScan = fscanf(arquivo, "%d", &(A.m));
        A.n = A.m;
        x.m = B.m = A.m;
        x.n = B.n = 1;
        for(i = 0; i < A.m; i++)
        {
            for(j = 0; j < A.n; j++)
            {
                retScan = fscanf(arquivo, "%lf", &(A.dados[i][j]));
            }
            retScan = fscanf(arquivo, "%lf", &B.dados[i][0]);
            x.dados[i][0] = 0;
        }

fclose(arquivo);

        g = numTasks-1;
        bloco = A.m/g;
        for(i = 1; i < numTasks; ++i)
        {
            rc = MPI_Send(&(A.m), 1, MPI_INT, i, tag, MPI_COMM_WORLD);
            inicio = bloco * (i-1);
            fim = (i+1 == numTasks)?A.m:inicio+bloco;
            rc = MPI_Send(&(inicio), 1, MPI_INT, i, tag, MPI_COMM_WORLD);
            rc = MPI_Send(&(fim), 1, MPI_INT, i, tag, MPI_COMM_WORLD);
            for(j = inicio; j < fim; ++j)
            {
                rc = MPI_Send(A.dados[j], A.n, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
                rc = MPI_Send(B.dados[j], 1, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
            }
            for(j = 0; j < x.m; ++j)
            {
                rc = MPI_Send(x.dados[j], 1, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
            }
        }

        for(j = 0; j < x.m; ++j)
        {
            rc = MPI_Recv(x.dados[j], 1, MPI_DOUBLE, numTasks-1, tag, MPI_COMM_WORLD, &Stat);
        }
//    printf("Rank = %d\n", rank);
//    imprimirMatriz("Matriz A", A);

        //rc = MPI_Recv(&(A.m), 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &Stat);

        gettimeofday(&t2, NULL);
        t2MPI = MPI_Wtime();

        imprimirMatriz("Vetor x final", x);
        printf("Tempo = %lf s\n", t2MPI-t1MPI);
        //printf("\ntime = %lf s\n\n", calcularTempoGasto(t1, t2));
    }
    else
    {
        rc = MPI_Recv(&(A.m), 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &Stat);
        A.n = A.m;
        x.m = B.m = A.m;
        x.n = B.n = 1;
        rc = MPI_Recv(&(inicio), 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &Stat);
        rc = MPI_Recv(&(fim), 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &Stat);
        for(j = inicio; j < fim; ++j)
        {
            rc = MPI_Recv(A.dados[j], A.n, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &Stat);
            rc = MPI_Recv(B.dados[j], 1, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &Stat);
        }
        for(j = 0; j < x.m; ++j)
        {
            rc = MPI_Recv(x.dados[j], 1, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &Stat);
        }


        x_prox.m = x.m;
        x_prox.n = x.n;
        for(k = 1; k < 1000000; k++)
        {
            /*printf("k = %d\n", k);
            imprimirMatriz("Vetor x", x);
            res = multiplicacaoMatrizes(A, x);
            imprimirMatriz("Resultado:", res);
            /*if(testarIgualdade(res, B))
            {
                break;
            }*/
//            printf("rank = %d - k = %d\n", rank, k);
//            imprimirMatriz("Vetor x", x);
            sign = 0;
            for(i = inicio; i < fim; ++i)
            {
                //ti
                x_prox.dados[i][0] = B.dados[i][0];
                soma = 0;
                for(j = i+1; j < A.n; j++)
                {
                    soma += (A.dados[i][j] * x.dados[j][0]);
                }
                x_prox.dados[i][0] -= soma;
            }
            //printf("Task %d - k = %d - sign: %d\n", rank, k, sign);

            if(rank > 1)
            {
                for(j = 0; j < inicio; ++j)
                {
                    rc = MPI_Recv(x_prox.dados[j], 1, MPI_DOUBLE, rank-1, k, MPI_COMM_WORLD, &Stat);
                }
                rc = MPI_Recv(&(sign), 1, MPI_INT, rank-1, k, MPI_COMM_WORLD, &Stat);
            }

            for(i = inicio; i < fim; ++i)
            {
                //zi
                soma = 0;
                for(j = 0; j < i; j++)
                {
                    soma += (A.dados[i][j] * x_prox.dados[j][0]);
                }
                x_prox.dados[i][0] -= soma;

                //x = 1/A[i][i]*x
                x_prox.dados[i][0] /= A.dados[i][i];
            }

            if(!sign)
            {
                for(i = inicio; i < fim; ++i)
                {
                    aux = x_prox.dados[i][0] - x.dados[i][0];
                    //printf("i: %d\nx_prox-x = %lf\n", i, aux);
                    //if((aux > TOL) || (aux < -TOL))
                    if(fabs(aux) > TOL)
                    {
                        sign = 1;
                        break;
                    }
                }
            }

            //printf("Task %d - k = %d - sign: %d\n", rank, k, sign);

            if(rank+1 != numTasks)
            {
                for(j = 0; j < fim; ++j)
                {
                    rc = MPI_Send(x_prox.dados[j], 1, MPI_DOUBLE, rank+1, k, MPI_COMM_WORLD);
                }
                rc = MPI_Send(&(sign), 1, MPI_INT, rank+1, k, MPI_COMM_WORLD);
                //x = x_prox;

                rc = MPI_Recv(&(sign), 1, MPI_INT, numTasks-1, tag, MPI_COMM_WORLD, &Stat);
                if(sign)
                {
                	for(j = 0; j < x.m; ++j)
	                {
	                    rc = MPI_Recv(x.dados[j], 1, MPI_DOUBLE, numTasks-1, tag, MPI_COMM_WORLD, &Stat);
	                }
                }
                else
                {
                    break;
                }
            }
            else
            {
                //printf("k = %d sign = %d\n", k, sign);
                //imprimirMatriz("Vetor x", x_prox);
                x = x_prox;
                for(i = 1; i < rank; ++i)
                {
                       rc = MPI_Send(&(sign), 1, MPI_INT, i, tag, MPI_COMM_WORLD);
                }
                if(sign)
                {
                    for(i = 1; i < rank; ++i)
                    {
                        for(j = 0; j < x.m; ++j)
                        {
                            rc = MPI_Send(x.dados[j], 1, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
                        }
                    }
                }
                else
                {
                    for(j = 0; j < x.m; ++j)
                    {
                        rc = MPI_Send(x.dados[j], 1, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
                    }
                    break;
                }
            }
        }
    }


//    printf("Rank = %d\n", rank);
//    imprimirMatriz("Matriz A", A);

    //x(k+1) = D^-1(B - L*x(k+1) - U*x(k))

    MPI_Finalize();

    return 0;
}
