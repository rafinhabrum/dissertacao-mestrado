/*Algoritmo pseudocodigo GMRES
função GMRES(Matriz de coeficientes: A; Vetor de valores independentes: b; Chute inicial: x0)
	$r_0$ ← $b - A \times x_0$ //Se o chute inicial for o vetor nulo, $r_0$ recebe só $b$
	$e_1$ ← $\|r_0\|_2$
	$v_1$ ← $\frac{r_0}{e_1}$
	tol ← $e_1$
	para j ← 1 até m faça
		$w_j$ ← $A \times v_j$
		para i ← 1 até j faça
			$h_{i,j}$ ← $ \langle w_j, v_i \rangle $
			$w_j$ ← $w_j - h_{i,j} \times v_i$
		$h_{j+1,j}$ ← $\|w_j\|_2$
		$v_{j+1}$ ← $\frac{w_j}{h_{j+1,j}}$
		para k ← 1 até j−1 faça
			aux ← $h_{k,j}$
			$h_{k,j}$ ← $c_k \times \textnormal{aux} + s_k \times h_{k+1,j}$
			$h_{k+1,j}$ ← $-s_k \times \textnormal{aux} + c_k \times h_{k+1,j}$
		$ra_j$ ← $\sqrt{h_{j,j}^2 + h_{j+1,j}^2}$
		$c_j$ ← $\frac{h_{j,j}}{ra_j}$
		$s_j$ ← $\frac{h_{j+1,j}}{ra_j}$
		$h_{j,j}$ ← $ra_j$
		$h_{j+1,j}$ ← 0}
		$e_{j+1}$ ← $-s_j \times e_j$
		$e_j$ ← $c_j \times e_j$
		tol ← $|e_j|$
		se tol < TOLERANCIA então
			m ← j
			//dar break
	para j ← m até 1 passo −1 faça
		$y_j$ ← $\frac{e_j - \sum_{t = j+1}^{m} (h_{j,t} \times y_t)}{h_{j,j}}$
	$x_m$ ← $x_0 + V_m \times y$ //Se o chute inicial for o vetor nulo, não precisa adicioná-lo
	retorna $x_m$
*/

/* Formato Compressed Sparse Row - CSR (p. 113 do Saad)
Usa 3 arrays para armazenar a matriz esparsa:
-> o AA para os valores numéricos (não-nulos) da matriz A na ordem por linha, os valores da primeira
linha seguidos dos valores da segunda linha... (a princípio do tipo double) -> tamanho Nz (qtde de valores não nulos)
-> o JA para armazenar o índice da coluna de cada um dos valores de AA (sempre do tipo inteiro) -> tamanho Nz
-> o IA, de tamanho N+1, para armazenar onde cada linha da matriz começa nos dois arrays acima. A
posição N+1 aponta para Nz+1 (sempre do tipo inteiro)
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NZ 101
#define NMATRIZ 11

#define MAX_ITER 10
#define TOLERANCIA 0.000001
#define TOL_NORMA 0.00001

double produtoInternoVetorial(double *a, double *b, int size);
void produtoMatrizEsparsaVetor(double AA[], int IA[], int JA[], double X[], double Y[]);
double normaVetorial(double *vec, int size);
void imprimirVetorD(double *, int size, char *nome);
void imprimirVetorI(int *a, int size, char *nome);
void resolverSistemaTriangular(double H[MAX_ITER+2][MAX_ITER+1], double vec[], int size, double vecSol[]);
void GMRES(double AA[], int IA[], int JA[], double b[], double x0[], double x_sol[]);

int main(){
    double AA[NZ];
    int JA[NZ], IA[NMATRIZ+1];
    double B[NMATRIZ], x0[NMATRIZ], x_sol[NMATRIZ];

    int i;

    //ler vetor AA
    for(i = 1; i < NZ; ++i)
    {
        scanf("%lf", &AA[i]);
    }
    //imprimirVetorD(AA, NZ, "AA");
    //ler vetor JA
    for(i = 1; i < NZ; ++i)
    {
        scanf("%d", &JA[i]);
    }
    //imprimirVetorI(JA, NZ, "JA");
    //ler vetor IA
    for(i = 1; i <= NMATRIZ; ++i)
    {
        scanf("%d", &IA[i]);
    }
    //imprimirVetorI(IA, NMATRIZ+1, "IA");
	//ler vetor B
    for(i = 1; i < NMATRIZ; ++i)
    {
        scanf("%lf", &B[i]);
        x0[i] = 0;
    }

    GMRES(AA, IA, JA, B, x0, x_sol);
    
    imprimirVetorD(x_sol, NMATRIZ, "solução");

    return 0;
}

double produtoInternoVetorial(double *a, double *b, int size)
{
    double result = 0;
    for(int i = 1; i < size; ++i)
    {
		//printf("a[%d] = %lf -- b[%d] = %lf\n", i, a[i], i, b[i]);
        result += (a[i]*b[i]);
        //printf("result = %lf\n\n", result);
    }
    return result;
}

void imprimirVetorD(double *a, int size, char *nome)
{
	printf("Vetor %s\n", nome);
    for(int i = 1; i < size; ++i)
    {
        printf("%.7lf\n", a[i]);
    }
    printf("\n");
}

void imprimirVetorI(int *a, int size, char *nome)
{
	printf("Vetor %s\n", nome);
    for(int i = 1; i < size; ++i)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
}

void produtoMatrizEsparsaVetor(double AA[NZ], int IA[NMATRIZ], int JA[NZ], double X[NMATRIZ], double Y[NMATRIZ])
{
    double auxAA[NMATRIZ], auxX[NMATRIZ];
    int k1, k2, i;

    for(i = 1; i < NMATRIZ; ++i)
    {
        k1 = IA[i];
        k2 = IA[i+1];
        //printf("k1 = %d | k2 = %d\n", k1, k2);
        for(int j = k1; j < k2; ++j)
        {
            auxAA[j-k1+1] = AA[j];
            auxX[j-k1+1] = X[JA[j]];
        }
        //imprimirVetorD(auxAA, k2-k1+1, "auxAA");
        //imprimirVetorD(auxX, k2-k1+1, "auxX");
        Y[i] = produtoInternoVetorial(auxAA, auxX, k2-k1+1);
        //printf("Y[%d] = %lf\n", i, Y[i]);
    }
}

double normaVetorial(double vec[NMATRIZ], int size)
{
	int i;
	double norma = 0;
	for(i = 1; i < size; ++i)
	{
		norma += (vec[i]*vec[i]);
	}
	norma = sqrt(norma);
	return norma;
}

void resolverSistemaTriangular(double H[MAX_ITER+2][MAX_ITER+1], double vec[MAX_ITER+2], int size, double vecSol[NMATRIZ]) //FALTA TESTAR
{
    int i, j;

    for(i = size; i > 0; --i)
    {
		//printf("\n\ni = %d\n", i);
        vecSol[i] = vec[i];
        //printf("vecSol[%d] = vec[%d] = %lf\n", i, i, vec[i]);
        //printf("vecSol[%d] = %lf\n", i, vecSol[i]);
        for(j = i+1; j <= size; ++j)
        {
			//printf("j = %d\n", j);
			//printf("H[%d][%d] = %lf e vecSol[%d] = %lf\n", i, j, H[i][j], j, vecSol[j]);
			vecSol[i] -= H[i][j] * vecSol[j];
			//printf("vecSol[%d] = %lf\n", i, vecSol[i]);
        }
		//printf("H[%d][%d] = %lf e vecSol[%d] = %lf\n", i, i, H[i][i], i, vecSol[i]);
		vecSol[i] /= H[i][i];
		//printf("vecSol[%d] = %lf\n\n", i, vecSol[i]);
    }
}

void GMRES(double AA[NZ], int IA[NMATRIZ], int JA[NZ], double b[NMATRIZ], double x0[NMATRIZ], double x_sol[NMATRIZ]) //x0 TEM QUE SER VETOR NULO!!
{
	double r0[NMATRIZ], e[MAX_ITER+2], w[NMATRIZ], c[MAX_ITER+1], s[MAX_ITER+1];
	double tol, ra, aux;
	double Vt[MAX_ITER+2][NMATRIZ];
	double V[NMATRIZ][MAX_ITER+2];
	double H[MAX_ITER+2][MAX_ITER+1];
	double Y[MAX_ITER+1], b_aux[NMATRIZ];
		
	int i, j, k, i_Aux, m = MAX_ITER;
	
	
	//~ $r_0$ ← $b - A \times x_0$ //Se o chute inicial for o vetor nulo, $r_0$ recebe só $b$
	//r0 = b;
	for(i_Aux = 1; i_Aux < NMATRIZ; ++i_Aux)
	{
		r0[i_Aux] = b[i_Aux];
	}
	/*imprimirVetorD(r0, NMATRIZ, "r0");
	printf("\n");*/
	
	//~ $e_1$ ← $\|r_0\|_2$
	e[1] = normaVetorial(r0, NMATRIZ);
	//printf("e1 = %lf\n\n", e[1]);
	
	//~ $v_1$ ← $\frac{r_0}{e_1}$
	for(i_Aux = 1; i_Aux < NMATRIZ; ++i_Aux)
	{
		Vt[1][i_Aux] = V[i_Aux][1] = r0[i_Aux]/e[1];
	}
	
	//~ tol ← $e_1$
	tol = e[1];
	
	//~ para j ← 1 até m faça
	for(j = 1; j <= MAX_ITER; ++j)
	{
aqui
		/*imprimirVetorD(Vt[j], NMATRIZ, "v");
		printf("\n");*/
		
		//~ $w_j$ ← $A \times v_j$
		produtoMatrizEsparsaVetor(AA, IA, JA, Vt[j], w);
		/*imprimirVetorD(w, NMATRIZ, "w");
		printf("\n");*/
			
		//~ para i ← 1 até j faça
		for(i = 1; i <= j; ++i)
		{
			//~ $h_{i,j}$ ← $ \langle w_j, v_i \rangle $
			H[i][j] = produtoInternoVetorial(w, Vt[i], NMATRIZ);
			//printf("H[%d][%d] = %lf\n\n", i, j, H[i][j]);
	
			//~ $w_j$ ← $w_j - h_{i,j} \times v_i$
			for(i_Aux = 1; i_Aux < NMATRIZ; ++i_Aux)
			{
				w[i_Aux] = w[i_Aux] - H[i][j] * Vt[i][i_Aux];
			}
			/*printf("i = %d - ", i);
			imprimirVetorD(w, NMATRIZ, "w ortogonalizado com i vetores");
			printf("\n");*/
	
		}
		//~ $h_{j+1,j}$ ← $\|w_j\|_2$
		H[j+1][j] = normaVetorial(w, NMATRIZ);
		//printf("H[%d][%d] = %lf\n\n", j+1, j, H[j+1][j]);
	
		//~ $v_{j+1}$ ← $\frac{w_j}{h_{j+1,j}}$
		for(i_Aux = 1; i_Aux < NMATRIZ; ++i_Aux)
		{
			Vt[j+1][i_Aux] = V[i_Aux][j+1] = w[i_Aux]/H[j+1][j];
		}
	
		//~ para k ← 1 até j−1 faça
		for(k = 1; k < j; ++k)
		{
			//~ aux ← $h_{k,j}$
			aux = H[k][j];
			
			//~ $h_{k,j}$ ← $c_k \times \textnormal{aux} + s_k \times h_{k+1,j}$
			H[k][j] = c[k]*aux + s[k]*H[k+1][j];
			
			//~ $h_{k+1,j}$ ← $-s_k \times \textnormal{aux} + c_k \times h_{k+1,j}$
			H[k+1][j] = (-s[k])*aux + c[k]*H[k+1][j];
			
		}
		
		/*printf("j = %d\n", j);
		printf("Matriz H modificada no passo j\n");
		for(i = 1; i < j+2; ++i)
		{
			imprimirVetorD(H[i], j+1, "");
		}
		printf("\n");*/
		
		//~ $ra_j$ ← $\sqrt{h_{j,j}^2 + h_{j+1,j}^2}$
		ra = sqrt(H[j][j]*H[j][j] + H[j+1][j]*H[j+1][j]);
		//ra = sqrt(H_aux[j][j]*H_aux[j][j] + H_aux[j+1][j]*H_aux[j+1][j]);
		//printf("ra = %lf\n\n", ra);
	
		//~ $c_j$ ← $\frac{h_{j,j}}{ra_j}$
		c[j] = H[j][j]/ra;
		//c[j] = H_aux[j][j]/ra;
		//printf("c[%d] = %lf\n\n", j, c[j]);
	
		//~ $s_j$ ← $\frac{h_{j+1,j}}{ra_j}$
		s[j] = H[j+1][j]/ra;
		//s[j] = H_aux[j+1][j]/ra;
		//printf("s[%d] = %lf\n\n", j, s[j]);
	
		//~ $h_{j,j}$ ← $ra_j$
		H[j][j] = ra;
		//printf("H[%d][%d] = %lf\n\n", j, j, H[j][j]);
	
		//~ $h_{j+1,j}$ ← 0}
		H[j+1][j] = 0;
		//printf("H[%d][%d] = %lf\n\n", j+1, j, H[j+1][j]);
	
		//~ $e_{j+1}$ ← $-s_j \times e_j$
		e[j+1] = -s[j]*e[j];
		//printf("e[%d] = %lf\n\n", j+1, e[j+1]);
	
		//~ $e_j$ ← $c_j \times e_j$
		e[j] = c[j]*e[j];
		//printf("e[%d] = %lf\n\n", j, e[j]);
	
		//~ tol ← $|e_{j+1}|$
		tol = fabs(e[j+1]);
		//printf("tol = %.10lf\n\n", tol);
			
		//~ se tol < TOLERANCIA então
		if(tol < TOLERANCIA)
		//if(j > NMATRIZ)
		{
			//~ m ← j
			m = j;
			break;
			
		}
	}
	
	/*printf("Matriz H modificada\n");
	for(i = 1; i <= MAX_ITER+2; ++i)
	{
		imprimirVetorD(H[i], MAX_ITER+2, "");
	}
	printf("\n");*/
	
	//~ para j ← m até 1 passo −1 faça
		//~ $y_j$ ← $\frac{e_j - \sum_{t = j+1}^{m} (h_{j,t} \times y_t)}{h_{j,j}}$
	resolverSistemaTriangular(H, e, m, Y);
	
	/*imprimirVetorD(Y, m+1, "solução SL auxiliar");
	printf("\n");*/
	
	/*printf("Matriz V\n");
	for(i = 1; i < NMATRIZ; ++i)
	{
		imprimirVetorD(V[i], MAX_ITER+2, "");
	}
	printf("\n");*/
	
	/*printf("Matriz Vt\n");
	for(i = 1; i <= MAX_ITER+1; ++i)
	{
		imprimirVetorD(Vt[i], NMATRIZ, "");
	}
	printf("\n");*/
	
	//imprimirVetorD(Y, MAX_ITER+2, "Y");
		
	//~ $x_m$ ← $x_0 + V_m \times y$ //Se o chute inicial for o vetor nulo, não precisa adicioná-lo
	for(i_Aux = 1; i_Aux < NMATRIZ; ++i_Aux)
	{
		x_sol[i_Aux] = produtoInternoVetorial(V[i_Aux], Y, m+1);
	}
	printf("m = %d\n", m);
	
	produtoMatrizEsparsaVetor(AA, IA, JA, x_sol, b_aux);
	
	//imprimirVetorD(b_aux, NMATRIZ, "b_aux");
	
	int count = 0;
    for (i_Aux = 1; i_Aux < NMATRIZ; ++i_Aux)
    {
		if((fabs(b_aux[i_Aux] - b[i_Aux]) < TOL_NORMA) && (fabs(b[i_Aux] - b_aux[i_Aux]) < TOL_NORMA))
      {
        count++;
      }
      b_aux[i_Aux] -= b[i_Aux];
    }
      printf("count = %d\nnormaVetorial = %.10lf\n", count, normaVetorial(b_aux, NMATRIZ));
      
      //imprimirVetorD(b_aux, NMATRIZ, "b_aux");
	
	//~ retorna $x_m$
	//imprimirVetorD(x_sol, NMATRIZ, "solução");
}
