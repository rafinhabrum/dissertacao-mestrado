/*Algoritmo pseudocodigo RGMRES
função RGMRES(Matriz de coeficientes: A; Vetor de valores independentes: b; Chute inicial: x0)
  enquanto solução $x_m$ não convergir faça
  	$r_0$ ← $b - A \times x_0$
  	$e_1$ ← $\|r_0\|_2$
  	$v_1$ ← $\frac{r_0}{e_1}$
  	tol ← $e_1$
  	para j ← 1 até m faça
  		$w_j$ ← $A \times v_j$
  		para i ← 1 até j faça
  			$h_{i,j}$ ← $ \langle w_j, v_i \rangle $
  			$w_j$ ← $w_j - h_{i,j} \times v_i$
  		$h_{j+1,j}$ ← $\|w_j\|_2$
  		$v_{j+1}$ ← $\frac{w_j}{h_{j+1,j}}$
  		para k ← 1 até j−1 faça
  			aux ← $h_{k,j}$
  			$h_{k,j}$ ← $c_k \times \textnormal{aux} + s_k \times h_{k+1,j}$
  			$h_{k+1,j}$ ← $-s_k \times \textnormal{aux} + c_k \times h_{k+1,j}$
  		$ra_j$ ← $\sqrt{h_{j,j}^2 + h_{j+1,j}^2}$
  		$c_j$ ← $\frac{h_{j,j}}{ra_j}$
  		$s_j$ ← $\frac{h_{j+1,j}}{ra_j}$
  		$h_{j,j}$ ← $ra_j$
  		$h_{j+1,j}$ ← 0
  		$e_{j+1}$ ← $-s_j \times e_j$
  		$e_j$ ← $c_j \times e_j$
  		tol ← $|e_j|$
  		se tol < TOLERANCIA então
  			m ← j
  			//dar break
  	para j ← m até 1 passo −1 faça
  		$y_j$ ← $\frac{e_j - \sum_{t = j+1}^{m} (h_{j,t} \times y_t)}{h_{j,j}}$
  	$x_m$ ← $x_0 + V_m \times y$
    $x_0$ ← $x_m$
	retorna $x_m$
*/

/* Formato Compressed Sparse Row - CSR (p. 113 do Saad)
Usa 3 arrays para armazenar a matriz esparsa:
-> o AA para os valores numéricos (não-nulos) da matriz A na ordem por linha, os valores da primeira
linha seguidos dos valores da segunda linha... (a princípio do tipo double) -> tamanho Nz (qtde de valores não nulos)
-> o JA para armazenar o índice da coluna de cada um dos valores de AA (sempre do tipo inteiro) -> tamanho Nz
-> o IA, de tamanho N+1, para armazenar onde cada linha da matriz começa nos dois arrays acima. A
posição N+1 aponta para Nz+1 (sempre do tipo inteiro)
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mytime.h"

//#define TOLERANCIA 1e-20
#define TOLERANCIA 1e-15

double produtoInternoVetorial(double *a, double *b, int size);
void produtoMatrizEsparsaVetor(double *AA, int *IA, int *JA, double *X, double **Y); //Y e alterado dentro da funcao
double normaVetorial(double *vec, int size);
void imprimirVetorD(double *, int size, char *nome);
void imprimirVetorI(int *a, int size, char *nome);
void resolverSistemaTriangular(double *H, int rowsH, int colsH, double *vec, int size, double **vecSol); //vecSol e alterado dentro da funcao
void RGMRES(double *AA, int *IA, int *JA, double *b, double *x0, double **x_sol); //x_sol e alterado dentro da funcao

int nz, nmatriz, max_iter;
double *auxAA, *auxX;

double tempo;

int main(){
    double *AA;
    int *JA, *IA;
    double *B, *x0, *x_sol;

    int i;

    scanf("%d %d %d\n", &nmatriz, &nz, &max_iter);

    AA = (double *)malloc(nz*sizeof(double));
    JA = (int *)malloc(nz*sizeof(int));
    IA = (int *)malloc((nmatriz+1)*sizeof(int));
    B = (double *)malloc(nmatriz*sizeof(double));
    x0 = (double *)calloc(nmatriz,sizeof(double));
    x_sol = (double *)calloc(nmatriz,sizeof(double));
    auxAA = (double *)malloc(nmatriz*sizeof(double));
    auxX = (double *)malloc(nmatriz*sizeof(double));

    if((!AA) || (!JA) || (!IA) || (!B) || (!x0) || (!x_sol) ||(!auxAA) || (!auxX))
    {
		fprintf(stderr, "Erro ao alocar\n");
		exit(2);
	}

    for(i = 0; i < nz; ++i)
    {
        scanf("%lg\n", (AA+i));
    }
    for(i = 0; i < nz; ++i)
    {
        scanf("%d\n", (JA+i));
    }
    for(i = 0; i <= nmatriz; ++i)
    {
        scanf("%d\n", (IA+i));
    }
    for(i = 0; i < nmatriz; ++i)
    {
        scanf("%lg\n", (B+i));
    }

    RGMRES(AA, IA, JA, B, x0, &x_sol);

    //imprimirVetorD(x_sol, nmatriz, "solução");

    printf("%lf s;\n", tempo);

    free(AA);
    free(JA);
    free(IA);
    free(B);
    free(x0);
    free(x_sol);
    free(auxAA);
    free(auxX);

    return 0;
}

double produtoInternoVetorial(double *a, double *b, int size)
{
    double result = 0;
    for(int i = 0; i < size; ++i)
    {
        result += ((*(a+i))*(*(b+i)));
    }
    return result;
}

void imprimirVetorD(double *a, int size, char *nome)
{
	printf("Vetor %s\n", nome);
    for(int i = 0; i < size; ++i)
    {
        printf("%lg\n", *(a+i));
    }
    printf("\n");
}

void imprimirVetorI(int *a, int size, char *nome)
{
	printf("Vetor %s\n", nome);
    for(int i = 0; i < size; ++i)
    {
        printf("%d\n", *(a+i));
    }
    printf("\n");
}

void produtoMatrizEsparsaVetor(double *AA, int *IA, int *JA, double *X, double **Y) //Y e alterado dentro da funcao
{
    //double *auxAA, *auxX;
    int k1, k2, i;
    int j, aux_j;

    //auxAA = (double *)malloc(nmatriz*sizeof(double));
    //auxX = (double *)malloc(nmatriz*sizeof(double));

    /*if((!auxAA) || (!auxX))
	{
		fprintf(stderr, "Erro ao alocar\n");
		exit(2);
	}*/

    for(i = 0; i < nmatriz; ++i)
    {
        k1 = *(IA+i);
        k2 = *(IA+i+1);
        for(j = k1, aux_j = 0; j < k2; ++j, ++aux_j)
        {
            *(auxAA+aux_j) = *(AA+j);
            *(auxX+aux_j) = *(X+(*(JA+j)));
        }
        *((*Y)+i) = produtoInternoVetorial(auxAA, auxX, k2-k1);
    }

    //free(auxAA);
    //free(auxX);
    //auxAA = NULL;
    //auxX = NULL;
    /*if(auxAA || auxX)
    {
		fprintf(stderr, "Nao desalocou!!!\n");
		exit(3);
	}*/
}

double normaVetorial(double *vec, int size)
{
	int i;
	double norma = 0;
	for(i = 0; i < size; ++i)
	{
		norma += ((*(vec+i))*(*(vec+i)));
	}
	norma = sqrt(norma);
	return norma;
}

void resolverSistemaTriangular(double *H, int rowsH, int colsH, double *vec, int size, double **vecSol) //vecSol e alterado dentro da funcao
{
    int i, j;

    for(i = size; i >= 0; --i)
    {
        *((*vecSol)+i) = *(vec+i);
        for(j = i+1; j <= size; ++j)
        {
            *((*vecSol)+i) -= ((*(H+(i*colsH+j))) * (*((*vecSol)+j)));
        }
		*((*vecSol)+i) /= *(H+(i*colsH+i));
    }
}

void RGMRES(double *AA, int *IA, int *JA, double *b, double *x0, double **x_sol) //x_sol e alterado dentro da funcao
{
	double *r0, *e, *w, *c, *s;
	double tol, ra, aux;
	double *Vt;
	double *V;
	double *H;
	double *y, *b_aux;
	int rowsH, colsH, rowsV, colsV, rowsVt, colsVt;
	int convergiu = 0;
	int count;

	int i, j, k, i_Aux, m = max_iter;

	r0 = (double *)malloc(nmatriz*sizeof(double));
	e = (double *)malloc((max_iter+2)*sizeof(double));
	w = (double *)malloc(nmatriz*sizeof(double));
	c = (double *)malloc((max_iter+1)*sizeof(double));
	s = (double *)malloc((max_iter+1)*sizeof(double));
	rowsVt = max_iter+2;
	colsVt = nmatriz;
	Vt = (double *)malloc(rowsVt*colsVt*sizeof(double));
	rowsV = nmatriz;
	colsV = max_iter+2;
	V = (double *)malloc(rowsV*colsV*sizeof(double));
	rowsH = max_iter+2;
	colsH = max_iter+1;
	H = (double *)malloc(rowsH*colsH*sizeof(double));
	y = (double *)malloc((max_iter+1)*sizeof(double));
	b_aux = (double *)malloc(nmatriz*sizeof(double));
	int iter = 1;

	if((!r0) || (!e) || (!w) || (!c) || (!s) || (!Vt) || (!V) || (!H) || (!y) || (!b_aux))
	{
		fprintf(stderr, "Erro ao alocar\n");
		exit(2);
	}

	//fprintf(stderr, "sizeof double = %ld\n", sizeof(double));
	tic(&tempo, TIME_s);

	while(!convergiu)
	{
		//fprintf(stderr, "iter = %d\n", iter);
		//imprimirVetorD(x0, nmatriz, "x0");
		produtoMatrizEsparsaVetor(AA, IA, JA, x0, &r0);
		for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			*(r0+i_Aux) *= (-1);
			*(r0+i_Aux) += *(b+i_Aux);
		}
		*e = normaVetorial(r0, nmatriz);
		for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			*(Vt+i_Aux) = *(V+(i_Aux*colsV)) = *(r0+i_Aux)/(*e);
		}
		tol = *e;
		for(j = 0; j <= max_iter; ++j)
		{
			produtoMatrizEsparsaVetor(AA, IA, JA, (Vt+(j*colsVt)), &w);
			for(i = 0; i <= j; ++i)
			{
				*(H+(i*colsH+j)) = produtoInternoVetorial(w, (Vt+(i*colsVt)), nmatriz);
				for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
				{
					*(w+i_Aux) = *(w+i_Aux) - (*(H+(i*colsH+j))) * (*(Vt+(i*colsVt+i_Aux)));
				}
			}
			*(H+((j+1)*colsH+j)) = normaVetorial(w, nmatriz);
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*(Vt+((j+1)*colsVt+i_Aux)) = *(V+(i_Aux*colsV+(j+1))) = (*(w+i_Aux))/(*(H+((j+1)*colsH+j)));
			}

			for(k = 0; k < j; ++k)
			{
				aux = *(H+(k*colsH+j));
				*(H+(k*colsH+j)) = (*(c+k))*aux + (*(s+k))*(*(H+((k+1)*colsH+j)));
				*(H+((k+1)*colsH+j)) = (-(*(s+k)))*aux + (*(c+k))*(*(H+((k+1)*colsH+j)));
			}
			ra = sqrt(((*(H+(j*colsH+j)))*(*(H+(j*colsH+j)))) + ((*(H+((j+1)*colsH+j)))*(*(H+((j+1)*colsH+j)))));
			*(c+j) = (*(H+(j*colsH+j)))/ra;
			*(s+j) = (*(H+((j+1)*colsH+j)))/ra;
			*(H+(j*colsH+j)) = ra;
			*(H+((j+1)*colsH+j)) = 0;
			*(e+(j+1)) = -(*(s+j))*(*(e+j));
			*(e+j) = (*(c+j))*(*(e+j));
			tol = fabs(*(e+j+1));
			if(tol < TOLERANCIA)
			{
				m = j;
				break;
			}
		}

		resolverSistemaTriangular(H, rowsH, colsH, e, m, &y);

		for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			*((*x_sol) + i_Aux) = *(x0 + i_Aux) + produtoInternoVetorial((V+(i_Aux*colsV)), y, m+1);
		}

		for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			*(x0 + i_Aux) = *((*x_sol) + i_Aux);
		}

		count = 0;
		produtoMatrizEsparsaVetor(AA, IA, JA, (*x_sol), &b_aux);
		for (i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			if((fabs((*(b_aux+i_Aux)) - (*(b+i_Aux))) < TOLERANCIA) && (fabs((*(b+i_Aux)) - (*(b_aux+i_Aux))) < TOLERANCIA))
			{
				count++;
			}
			*(b_aux+i_Aux) -= *(b+i_Aux);
		}
		//imprimirVetorD(b_aux, nmatriz, "b_aux");
		double norma_b_aux = normaVetorial(b_aux, nmatriz);
		//printf("normaVetorial = %.32lf\n", norma_b_aux);
		if((count == nmatriz) || (norma_b_aux < TOLERANCIA))
		{
			convergiu = 1;
		}
		if(iter == 3000)
		{
			//printf("Limite de iteracoes\n");
			break;
		}
		iter++;
	}
	//printf("count = %d\n", count);
	//printf("iter = %d\n m = %d\n", iter, m);

	tac(&tempo, TIME_s);

	free(r0);
	free(e);
	free(w);
	free(c);
	free(s);
	free(Vt);
	free(V);
	free(H);
	free(y);
	free(b_aux);
}
