#!/bin/bash
gcc mytime.c HBGMRES_par_static.c -o HBGMRES_par_static -lm -O2 -fopenmp
gcc mytime.c HBGMRES_par_dinamic.c -o HBGMRES_par_dinamic -lm -O2 -fopenmp
echo "Trocou o teste - add20"
echo "HBGMRES_par_static" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < add20.dat >> tempos_add20.dat
done
echo "Trocou o teste - cavity05"
echo "HBGMRES_par_static" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < cavity05.dat >> tempos_cavity05.dat
done
echo "Trocou o teste - cavity10"
echo "HBGMRES_par_static" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < cavity10.dat >> tempos_cavity10.dat
done
echo "Trocou o teste - circuit_2"
echo "HBGMRES_par_static" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < circuit_2.dat >> tempos_circuit_2.dat
done
echo "Trocou o teste - comsol"
echo "HBGMRES_par_static" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < comsol.dat >> tempos_comsol.dat
done
echo "Trocou o teste - flowmeter5"
echo "HBGMRES_par_static" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "Trocou o teste - fpga_trans_01"
echo "HBGMRES_par_static" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "Trocou o teste - fpga_trans_02"
echo "HBGMRES_par_static" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "Trocou o teste - raefsky1"
echo "HBGMRES_par_static" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < raefsky1.dat >> tempos_raefsky1.dat
done
echo "Trocou o teste - raefsky2"
echo "HBGMRES_par_static" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < raefsky2.dat >> tempos_raefsky2.dat
done
echo "Trocou o teste - sherman4"
echo "HBGMRES_par_static" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < sherman4.dat >> tempos_sherman4.dat
done
echo "Trocou o teste - chipcool0"
echo "HBGMRES_par_static" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < chipcool0.dat >> tempos_chipcool0.dat
done
echo "Trocou o teste - memplus"
echo "HBGMRES_par_static" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < memplus.dat >> tempos_memplus.dat
done
echo "Trocou o teste - wang3"
echo "HBGMRES_par_static" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < wang3.dat >> tempos_wang3.dat
done
echo "Trocou o teste - matrix-new_3"
echo "HBGMRES_par_static" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_static < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
echo "Trocou o teste - add20"
echo "HBGMRES_par_dinamic" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < add20.dat >> tempos_add20.dat
done
echo "Trocou o teste - cavity05"
echo "HBGMRES_par_dinamic" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < cavity05.dat >> tempos_cavity05.dat
done
echo "Trocou o teste - cavity10"
echo "HBGMRES_par_dinamic" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < cavity10.dat >> tempos_cavity10.dat
done
echo "Trocou o teste - circuit_2"
echo "HBGMRES_par_dinamic" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < circuit_2.dat >> tempos_circuit_2.dat
done
echo "Trocou o teste - comsol"
echo "HBGMRES_par_dinamic" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < comsol.dat >> tempos_comsol.dat
done
echo "Trocou o teste - flowmeter5"
echo "HBGMRES_par_dinamic" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "Trocou o teste - fpga_trans_01"
echo "HBGMRES_par_dinamic" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "Trocou o teste - fpga_trans_02"
echo "HBGMRES_par_dinamic" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "Trocou o teste - raefsky1"
echo "HBGMRES_par_dinamic" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < raefsky1.dat >> tempos_raefsky1.dat
done
echo "Trocou o teste - raefsky2"
echo "HBGMRES_par_dinamic" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < raefsky2.dat >> tempos_raefsky2.dat
done
echo "Trocou o teste - sherman4"
echo "HBGMRES_par_dinamic" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < sherman4.dat >> tempos_sherman4.dat
done
echo "Trocou o teste - chipcool0"
echo "HBGMRES_par_dinamic" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < chipcool0.dat >> tempos_chipcool0.dat
done
echo "Trocou o teste - memplus"
echo "HBGMRES_par_dinamic" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < memplus.dat >> tempos_memplus.dat
done
echo "Trocou o teste - wang3"
echo "HBGMRES_par_dinamic" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
#	./HBGMRES_par_dinamic < wang3.dat >> tempos_wang3.dat
done
echo "Trocou o teste - matrix-new_3"
echo "HBGMRES_par_dinamic" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par_dinamic < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
