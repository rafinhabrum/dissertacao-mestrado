 /*Algoritmo pseudocodigo HBGMRES
função HBGMRES(Matriz de coeficientes: A; Vetor de valores independentes: b; Chute inicial: x0)
  $l$ ← 1
  enquanto solução $x_m$ não convergir faça
  	$r_0$ ← $b - A \times x_0$
  	$e_1$ ← $\|r_0\|_2$
  	$v_1$ ← $\frac{r_0}{e_1}$
  	tol ← $e_1$
  	para j ← 1 até m faça
  		$w_j$ ← $A \times v_j$
  		para i ← 1 até j faça
  			$h_{i,j}$ ← $ \langle w_j, v_i \rangle $
  			$w_j$ ← $w_j - h_{i,j} \times v_i$
  		$h_{j+1,j}$ ← $\|w_j\|_2$
  		$v_{j+1}$ ← $\frac{w_j}{h_{j+1,j}}$
  		para k ← 1 até j−1 faça
  			aux ← $h_{k,j}$
  			$h_{k,j}$ ← $c_k \times \textnormal{aux} + s_k \times h_{k+1,j}$
  			$h_{k+1,j}$ ← $-s_k \times \textnormal{aux} + c_k \times h_{k+1,j}$
  		$ra_j$ ← $\sqrt{h_{j,j}^2 + h_{j+1,j}^2}$
  		$c_j$ ← $\frac{h_{j,j}}{ra_j}$
  		$s_j$ ← $\frac{h_{j+1,j}}{ra_j}$
  		$h_{j,j}$ ← $ra_j$
  		$h_{j+1,j}$ ← 0
  		$e_{j+1}$ ← $-s_j \times e_j$
  		$e_j$ ← $c_j \times e_j$
  		tol ← $|e_j|$
  		se tol < TOLERANCIA então
  			m ← j
  			//dar break
  	se l igual a 1 então
  		$d$ ← $x_0^{l}$
  	senão
		$d$ ← $x_0^{l} - x_0^{l-1}$
	$\tilde{d}$ ← $V_m^T \times d$
    $g$ ← $d - V_m \times \tilde{d}$
    se g != 0 então
		$\tilde{v}_{m+1}$ ← $\frac{g}{\|g\|_2}$
		$\tilde{V}_{m+1}$ ← $[V_k$  $\tilde{v}_{m+1}]$
		$a$ ← $A \times \tilde{v}_{m+1}$
		$\hat{a}$ ← $a$
  		para i ← 1 até m+1 faça
			$h_{i,m+1}$ ← $ \langle \hat{a}, v_i \rangle $
			$\hat{a}$ ← $\hat{a} - h_{i,m+1} \times v_i$
  		$h_{m+2,m+1}$ ← $\|\hat{a}\|_2$
		$he$ ← $a - \tilde{V}_{m+1} \times \hat{a}$
		se he != 0 então
			para k ← 1 até m faça
				aux ← $h_{k,{m+1}}$
				$h_{k,{m+1}}$ ← $c_k \times \textnormal{aux} + s_k \times h_{k+1,{m+1}}$
				$h_{k+1,{m+1}}$ ← $-s_k \times \textnormal{aux} + c_k \times h_{k+1,{m+1}}$
			$r_{m+1}$ ← $\sqrt{h_{{m+1},{m+1}}^2 + h_{{m+2},{m+1}}^2}$
			$c_{m+1}$ ← $\frac{h_{{m+1},{m+1}}}{r_{m+1}}$
			$s_{m+1}$ ← $\frac{h_{{m+2},{m+1}}}{r_{m+1}}$
			$h_{{m+1},{m+1}}$ ← $ra_j$
			$h_{{m+2},{m+1}}$ ← 0
			$e_{{m+2}}$ ← $-s_{m+1} \times e_{m+1}$
			$e_{m+1}$ ← $c_{m+1} \times e_{m+1}$
  		para j ← m+1 até 1 passo −1 faça
			$y_j$ ← $\frac{e_j - \sum_{t = j+1}^{m+1} (h_{j,t} \times y_t)}{h_{j,j}}$
		$x_m$ ← $x_0 + \tilde{V}_{m+1} \times y$
	senão
		para j ← m até 1 passo −1 faça
			$y_j$ ← $\frac{e_j - \sum_{t = j+1}^{m} (h_{j,t} \times y_t)}{h_{j,j}}$
		$x_m$ ← $x_0 + V_m \times y$
    $x_0^{l+1}$ ← $x_m$
    $l$ ← $l+1$
  retorna $x_m$
*/

/* Formato Compressed Sparse Row - CSR (p. 113 do Saad)
Usa 3 arrays para armazenar a matriz esparsa:
-> o AA para os valores numéricos (não-nulos) da matriz A na ordem por linha, os valores da primeira
linha seguidos dos valores da segunda linha... (a princípio do tipo double) -> tamanho Nz (qtde de valores não nulos)
-> o JA para armazenar o índice da coluna de cada um dos valores de AA (sempre do tipo inteiro) -> tamanho Nz
-> o IA, de tamanho N+1, para armazenar onde cada linha da matriz começa nos dois arrays acima. A
posição N+1 aponta para Nz+1 (sempre do tipo inteiro)
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mytime.h"

#define TOLERANCIA 0.000001

double produtoInternoVetorial(double *a, double *b, int size);
void produtoMatrizEsparsaVetor(double *AA, int *IA, int *JA, double *X, double **Y); //Y e alterado dentro da funcao
double normaVetorial(double *vec, int size);
void imprimirVetorD(double *, int size, char *nome);
void imprimirVetorI(int *a, int size, char *nome);
void resolverSistemaTriangular(double *H, int rowsH, int colsH, double *vec, int size, double **vecSol); //vecSol e alterado dentro da funcao
void HBGMRES(double *AA, int *IA, int *JA, double *b, double *x0_atual, double **x_sol); //x_sol e alterado dentro da funcao

int nz, nmatriz, max_iter;

double tempo;

int main(){
    double *AA; //vetor com os valores da matriz esparsa A
    int *JA, *IA; //vetores com os indices da matriz esparsa A
    double *B, *x0, *x_sol; //vetor com os valores independentes, valor inicial e solucao final

    int i;

    scanf("%d %d %d\n", &nz, &nmatriz, &max_iter);

    AA = (double *)malloc(nz*sizeof(double));
    JA = (int *)malloc(nz*sizeof(int));
    IA = (int *)malloc((nmatriz+1)*sizeof(int));
    B = (double *)malloc(nmatriz*sizeof(double));
    x0 = (double *)calloc(nmatriz,sizeof(double));
    x_sol = (double *)calloc(nmatriz,sizeof(double));

    for(i = 0; i < nz; ++i)
    {
        scanf("%lf", (AA+i));
    }
    for(i = 0; i < nz; ++i)
    {
        scanf("%d", (JA+i));
        *(JA+i) = (*(JA+i))-1;
    }
    for(i = 0; i <= nmatriz; ++i)
    {
        scanf("%d", (IA+i));
        (*(IA+i))--;
    }
    for(i = 0; i < nmatriz; ++i)
    {
        scanf("%lf", (B+i));
    }

    HBGMRES(AA, IA, JA, B, x0, &x_sol);

    imprimirVetorD(x_sol, nmatriz, "solução");

    printf("Tempo = %lf s\n", tempo);

    free(AA);
    free(JA);
    free(IA);
    free(B);
    free(x0);
    free(x_sol);

    return 0;
}

double produtoInternoVetorial(double *a, double *b, int size)
{
    double result = 0;
    for(int i = 0; i < size; ++i)
    {
        result += ((*(a+i))*(*(b+i)));
    }
    return result;
}

void imprimirVetorD(double *a, int size, char *nome)
{
	printf("Vetor %s\n", nome);
    for(int i = 0; i < size; ++i)
    {
        printf("%.7lf\n", *(a+i));
    }
    printf("\n");
}

void imprimirVetorI(int *a, int size, char *nome)
{
	printf("Vetor %s\n", nome);
    for(int i = 0; i < size; ++i)
    {
        printf("%d ", *(a+i));
    }
    printf("\n");
}

void produtoMatrizEsparsaVetor(double *AA, int *IA, int *JA, double *X, double **Y) //Y e alterado dentro da funcao
{
    double *auxAA, *auxX;
    int k1, k2, i;
    int j, aux_j;

    auxAA = (double *)malloc(nmatriz*sizeof(double));
    auxX = (double *)malloc(nmatriz*sizeof(double));

    for(i = 0; i < nmatriz; ++i)
    {
        k1 = *(IA+i);
        k2 = *(IA+i+1);
        for(j = k1, aux_j = 0; j < k2; ++j, ++aux_j)
        {
            *(auxAA+aux_j) = *(AA+j);
            *(auxX+aux_j) = *(X+(*(JA+j)));
        }
        *((*Y)+i) = produtoInternoVetorial(auxAA, auxX, k2-k1);
    }

    free(auxAA);
    free(auxX);
}

double normaVetorial(double *vec, int size)
{
	int i;
	double norma = 0;
	for(i = 0; i < size; ++i)
	{
		norma += ((*(vec+i))*(*(vec+i)));
	}
	norma = sqrt(norma);
	return norma;
}

void resolverSistemaTriangular(double *H, int rowsH, int colsH, double *vec, int size, double **vecSol) //vecSol e alterado dentro da funcao
{
    int i, j;

    for(i = size; i >= 0; --i)
    {
        *((*vecSol)+i) = *(vec+i);
        for(j = i+1; j <= size; ++j)
        {
            *((*vecSol)+i) -= ((*(H+(i*colsH+j))) * (*((*vecSol)+j)));
        }
		*((*vecSol)+i) /= *(H+(i*colsH+i));
    }
}

void HBGMRES(double *AA, int *IA, int *JA, double *b, double *x0_atual, double **x_sol) //x_sol e alterado dentro da funcao
{
	double *r0; //residuo
  double *e; //vetor com os valores do SL auxiliar
  double *w; //vetor atual da base sendo calculado
  double *c; //vetor com as constantes das rotacoes de Givens
  double *s; //vetor com as constantes das rotacoes de Givens
	double *x0_anterior; //vetor com o valor inicial do ciclo anterior
  double *d; //diferenca entre dois valores iniciais
  double *dTil; //projecao da diferenca na base V
  double *g; //diferenca entre d e a projecao de dTil
	double *a; //vetor resultado de A vezes g
  double *aHat; //vetor a ortogonal a base V
  double *he; //diferenca de a com a projecao de aHat
	double tol, ra, aux; //valores para parar um ciclo antes da iteracao final e auxiliares
	double *Vt; //matriz V transposta
	double *V; //matriz V base do subespaço gerado pelos vetores w_i ortogonais
	double *H; //matriz de Hessenberg do SL auxiliar
	double *y; //vetor solucao do SL auxiliar
  double *b_aux; //vetor para testar convergencia
	int rowsH, colsH, rowsV, colsV, rowsVt, colsVt; //valores de colunas e linhas das matrizes
	int convergiu = 0;
	int count;

	int i, j, k, i_Aux, m = max_iter;

	r0 = (double *)malloc(nmatriz*sizeof(double));
	e = (double *)malloc((max_iter+3)*sizeof(double));
	w = (double *)malloc(nmatriz*sizeof(double));
	c = (double *)malloc((max_iter+2)*sizeof(double));
	s = (double *)malloc((max_iter+2)*sizeof(double));
	x0_anterior = (double *)malloc(nmatriz*sizeof(double));
	d = (double *)malloc(nmatriz*sizeof(double));
	dTil = (double *)malloc((max_iter+1)*sizeof(double));
	g = (double *)malloc(nmatriz*sizeof(double));
	a = (double *)malloc(nmatriz*sizeof(double));
	aHat = (double *)malloc(nmatriz*sizeof(double));
	he = (double *)malloc(nmatriz*sizeof(double));
	rowsVt = max_iter+2;
	colsVt = nmatriz;
	Vt = (double *)malloc(rowsVt*colsVt*sizeof(double));
	rowsV = nmatriz;
	colsV = max_iter+2;
	V = (double *)malloc(rowsV*colsV*sizeof(double));
	rowsH = max_iter+3;
	colsH = max_iter+2;
	H = (double *)malloc(rowsH*colsH*sizeof(double));
	y = (double *)malloc((max_iter+2)*sizeof(double));
	b_aux = (double *)malloc(nmatriz*sizeof(double));
	int iter = 1;

  //~ $l$ ← 1
	int l = 1;

	tic(&tempo, TIME_s);

  //~ enquanto solução $x_m$ não convergir faça
	while(!convergiu)
	{
		//~ $r_0$ ← $b - A \times x_0$
		produtoMatrizEsparsaVetor(AA, IA, JA, x0_atual, &r0);
		for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			*(r0+i_Aux) *= (-1);
			*(r0+i_Aux) += *(b+i_Aux);
		}

		//~ $e_1$ ← $\|r_0\|_2$
		*e = normaVetorial(r0, nmatriz);

    //~ $v_1$ ← $\frac{r_0}{e_1}$
		for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			*(Vt+i_Aux) = *(V+(i_Aux*colsV)) = *(r0+i_Aux)/(*e);
		}

    //~ tol ← $e_1$
		tol = *e;

    //~ para j ← 1 até m faça
		for(j = 0; j <= max_iter; ++j)
		{
      //~ $w_j$ ← $A \times v_j$
			produtoMatrizEsparsaVetor(AA, IA, JA, (Vt+(j*colsVt)), &w);

      //~ para i ← 1 até j faça
			for(i = 0; i <= j; ++i)
			{
        //~ $h_{i,j}$ ← $ \langle w_j, v_i \rangle $
				*(H+(i*colsH+j)) = produtoInternoVetorial(w, (Vt+(i*colsVt)), nmatriz);

        //~ $w_j$ ← $w_j - h_{i,j} \times v_i$
				for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
				{
					*(w+i_Aux) = *(w+i_Aux) - (*(H+(i*colsH+j))) * (*(Vt+(i*colsVt+i_Aux)));
				}
			}

  		//~ $h_{j+1,j}$ ← $\|w_j\|_2$
			*(H+((j+1)*colsH+j)) = normaVetorial(w, nmatriz);

      //~ $v_{j+1}$ ← $\frac{w_j}{h_{j+1,j}}$
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*(Vt+((j+1)*colsVt+i_Aux)) = *(V+(i_Aux*colsV+(j+1))) = (*(w+i_Aux))/(*(H+((j+1)*colsH+j)));
			}

      //~ para k ← 1 até j−1 faça
			for(k = 0; k < j; ++k)
			{
        //~ aux ← $h_{k,j}$
				aux = *(H+(k*colsH+j));

        //~ $h_{k,j}$ ← $c_k \times \textnormal{aux} + s_k \times h_{k+1,j}$
  			*(H+(k*colsH+j)) = (*(c+k))*aux + (*(s+k))*(*(H+((k+1)*colsH+j)));

        //~ $h_{k+1,j}$ ← $-s_k \times \textnormal{aux} + c_k \times h_{k+1,j}$
  			*(H+((k+1)*colsH+j)) = (-(*(s+k)))*aux + (*(c+k))*(*(H+((k+1)*colsH+j)));
			}

      //~ $ra_j$ ← $\sqrt{h_{j,j}^2 + h_{j+1,j}^2}$
  		ra = sqrt(((*(H+(j*colsH+j)))*(*(H+(j*colsH+j)))) + ((*(H+((j+1)*colsH+j)))*(*(H+((j+1)*colsH+j)))));

      //~ $c_j$ ← $\frac{h_{j,j}}{ra_j}$
  		*(c+j) = (*(H+(j*colsH+j)))/ra;

      //~ $s_j$ ← $\frac{h_{j+1,j}}{ra_j}$
  		*(s+j) = (*(H+((j+1)*colsH+j)))/ra;

      //~ $h_{j,j}$ ← $ra_j$
  		*(H+(j*colsH+j)) = ra;

      //~ $h_{j+1,j}$ ← 0}
  		*(H+((j+1)*colsH+j)) = 0;

      //~ $e_{j+1}$ ← $-s_j \times e_j$
  		*(e+(j+1)) = -(*(s+j))*(*(e+j));

      //~ $e_j$ ← $c_j \times e_j$
  		*(e+j) = (*(c+j))*(*(e+j));

      //~ tol ← $|e_{j+1}|$
  		tol = fabs(*(e+j+1));

      //~ se tol < TOLERANCIA então
  		if(tol < TOLERANCIA)
			{
        //~ m ← j
  			m = j;
				break;
			}
		}

    //~ se l igual a 1 então
  	if(l == 1)
		{
      //~ $d$ ← $x_0^{l}$
		  for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*(d+i_Aux) = *(x0_atual+i_Aux);
			}
			l++;
		}
    //~ senão
		else
		{
      //~ $d$ ← $x_0^{l} - x_0^{l-1}$
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*(d+i_Aux) = *(x0_atual+i_Aux) - *(x0_anterior+i_Aux);
			}
		}

    //~ $\tilde{d}$ ← $V_m^T \times d$
		for(i_Aux = 0; i_Aux <= m; ++i_Aux)
		{
			*(dTil+i_Aux) = produtoInternoVetorial((Vt+(i_Aux*colsVt)), d, nmatriz+1);
		}

    //~ $g$ ← $d - V_m \times \tilde{d}$
		for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			*(g+i_Aux) = *(d+i_Aux) - produtoInternoVetorial((V+(i_Aux*colsV)), dTil, m+1);
		}

    //~ se g != 0 então
		double norma = normaVetorial(g, nmatriz);
		if(fabs(norma) > TOLERANCIA)
		{
      //~ $\tilde{v}_{m+1}$ ← $\frac{g}{\|g\|_2}$
      //~ $\tilde{V}_{m+1}$ ← $[V_k$  $\tilde{v}_{m+1}]$
      for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*(Vt+((m+1)*colsVt+i_Aux)) = *(V+(i_Aux*colsV+(m+1))) = (*(g+i_Aux))/norma;
			}

      //~ $a$ ← $A \times \tilde{v}_{m+1}$
			produtoMatrizEsparsaVetor(AA, IA, JA, (Vt+((m+1)*colsVt)), &a);

      //~ $\hat{a}$ ← $a$
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*(aHat+i_Aux) = *(a+i_Aux);
			}

      //~ para i ← 1 até m+1 faça
			for(i = 0; i <= (m+1); ++i)
			{
        //~ $h_{i,m+1}$ ← $ \langle \hat{a}, v_i \rangle $
				*(H+(i*colsH+(m+1))) = produtoInternoVetorial(aHat, (Vt+(i*colsVt)), nmatriz);

        //~ $\hat{a}$ ← $\hat{a} - h_{i,m+1} \times v_i$
				for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
				{
					*(aHat+i_Aux) = *(aHat+i_Aux) - (*(H+(i*colsH+(m+1)))) * (*(Vt+(i*colsVt+i_Aux)));
				}
			}

      //~ $h_{m+2,m+1}$ ← $\|\hat{a}\|_2$
			*(H+((m+2)*colsH+(m+1))) = normaVetorial(aHat, nmatriz);

      //~ $he$ ← $a - \tilde{V}_{m+1} \times \hat{a}$
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*(he+i_Aux) = *(a+i_Aux) - produtoInternoVetorial((V+(i_Aux*colsV)), aHat, m+2);
			}

      //~ se he != 0 então
			if(fabs(normaVetorial(he, nmatriz)) > TOLERANCIA)
			{
        //~ para k ← 1 até m faça
				for(k = 0; k <= m; ++k)
				{
          //~ aux ← $h_{k,{m+1}}$
					aux = *(H+(k*colsH+(m+1)));

          //~ $h_{k,{m+1}}$ ← $c_k \times \textnormal{aux} + s_k \times h_{k+1,{m+1}}$
		      *(H+(k*colsH+(m+1))) = (*(c+k))*aux + (*(s+k))*(*(H+((k+1)*colsH+(m+1))));

          //~ $h_{k+1,{m+1}}$ ← $-s_k \times \textnormal{aux} + c_k \times h_{k+1,{m+1}}$
          *(H+((k+1)*colsH+(m+1))) = (-(*(s+k)))*aux + (*(c+k))*(*(H+((k+1)*colsH+(m+1))));
				}

        //~ $r_{m+1}$ ← $\sqrt{h_{{m+1},{m+1}}^2 + h_{{m+2},{m+1}}^2}$
        ra = sqrt(((*(H+((m+1)*colsH+(m+1))))*(*(H+((m+1)*colsH+(m+1))))) + (*(H+(((m+2)*colsH+(m+1))))*(*(H+(((m+2)*colsH+(m+1)))))));

        //~ $c_{m+1}$ ← $\frac{h_{{m+1},{m+1}}}{r_{m+1}}$
      	*(c+(m+1)) = (*(H+((m+1)*colsH+(m+1))))/ra;

        //~ $s_{m+1}$ ← $\frac{h_{{m+2},{m+1}}}{r_{m+1}}$
				*(s+(m+1)) = (*(H+((m+2)*colsH+(m+1))))/ra;

        //~ $h_{{m+1},{m+1}}$ ← $ra_j$
        *(H+((m+1)*colsH+(m+1))) = ra;

        //~ $h_{{m+2},{m+1}}$ ← 0
				*(H+((m+2)*colsH+(m+1))) = 0;

        //~ $e_{{m+2}}$ ← $-s_{m+1} \times e_{m+1}$
				*(e+(m+2)) = -(*(s+(m+1)))*(*(e+(m+1)));

        //~ $e_{m+1}$ ← $c_{m+1} \times e_{m+1}$
				*(e+(m+1)) = (*(c+(m+1)))*(*(e+(m+1)));
			}

      //~ para j ← m+1 até 1 passo −1 faça
			//~ $y_j$ ← $\frac{e_j - \sum_{t = j+1}^{m+1} (h_{j,t} \times y_t)}{h_{j,j}}$
			resolverSistemaTriangular(H, rowsH, colsH, e, m+1, &y);

      //~ $x_m$ ← $x_0 + \tilde{V}_{m+1} \times y$
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*((*x_sol) + i_Aux) = *(x0_atual + i_Aux) + produtoInternoVetorial((V+(i_Aux*colsV)), y, m+2);
			}
		}
    //~ senão
		else
		{
      //~ para j ← m até 1 passo −1 faça
			//~ $y_j$ ← $\frac{e_j - \sum_{t = j+1}^{m} (h_{j,t} \times y_t)}{h_{j,j}}$
			resolverSistemaTriangular(H, rowsH, colsH, e, m, &y);

      //~ $x_m$ ← $x_0 + V_m \times y$
			for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
			{
				*((*x_sol) + i_Aux) = *(x0_atual + i_Aux) + produtoInternoVetorial((V+(i_Aux*colsV)), y, m+1);
			}
		}

    //~ $x_0^{l+1}$ ← $x_m$
    //~ $l$ ← $l+1$
		for(i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			*(x0_anterior + i_Aux) = *(x0_atual + i_Aux);
			*(x0_atual + i_Aux) = *((*x_sol) + i_Aux);
		}

    //testando quantos valores de A vezes x_sol sao iguais a b, se todos forem iguais, para o metodo
		count = 0;
		produtoMatrizEsparsaVetor(AA, IA, JA, (*x_sol), &b_aux);
		for (i_Aux = 0; i_Aux < nmatriz; ++i_Aux)
		{
			if((fabs((*(b_aux+i_Aux)) - (*(b+i_Aux))) < TOLERANCIA) && (fabs((*(b+i_Aux)) - (*(b_aux+i_Aux))) < TOLERANCIA))
			{
				count++;
			}
			*(b_aux+i_Aux) -= *(b+i_Aux);
		}
		if(count == nmatriz)
		{
			convergiu = 1;
		}
		iter++;
	}
	printf("iter = %d\n m = %d\n", iter, m);

	tac(&tempo, TIME_s);

	free(r0);
	free(e);
	free(w);
	free(c);
	free(s);
	free(x0_anterior);
	free(d);
	free(dTil);
	free(g);
	free(a);
	free(aHat);
	free(he);
	free(Vt);
	free(V);
	free(H);
	free(y);
	free(b_aux);
}
