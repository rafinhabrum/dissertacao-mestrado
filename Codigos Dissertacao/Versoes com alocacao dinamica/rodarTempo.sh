#!/bin/bash
gcc mytime.c GMRES.c -o GMRES -lm -O2
gcc mytime.c RGMRES.c -o RGMRES -lm -O2
gcc mytime.c FGMRES.c -o FGMRES -lm -O2
gcc mytime.c alphaGMRES.c -o alphaGMRES -lm -O2
gcc mytime.c HBGMRES.c -o HBGMRES -lm -O2
gcc mytime.c HBGMRES_par.c -o HBGMRES_par -lm -O2 -fopenmp
echo "GMRES" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
	./GMRES < add20.dat >> tempos_add20.dat
done
echo "RGMRES" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < add20.dat >> tempos_add20.dat
done
echo "FGMRES" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < add20.dat >> tempos_add20.dat
done
echo "alphaGMRES" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < add20.dat >> tempos_add20.dat
done
echo "HBGMRES" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < add20.dat >> tempos_add20.dat
done
echo "Trocou o teste - cavity05"
echo "GMRES" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
	./GMRES < cavity05.dat >> tempos_cavity05.dat
done
echo "RGMRES" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < cavity05.dat >> tempos_cavity05.dat
done
echo "FGMRES" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < cavity05.dat >> tempos_cavity05.dat
done
echo "alphaGMRES" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < cavity05.dat >> tempos_cavity05.dat
done
echo "HBGMRES" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < cavity05.dat >> tempos_cavity05.dat
done
echo "Trocou o teste - cavity10"
echo "GMRES" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
	./GMRES < cavity10.dat >> tempos_cavity10.dat
done
echo "RGMRES" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < cavity10.dat >> tempos_cavity10.dat
done
echo "FGMRES" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < cavity10.dat >> tempos_cavity10.dat
done
echo "alphaGMRES" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < cavity10.dat >> tempos_cavity10.dat
done
echo "HBGMRES" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < cavity10.dat >> tempos_cavity10.dat
done
echo "Trocou o teste - circuit_2"
echo "GMRES" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
	./GMRES < circuit_2.dat >> tempos_circuit_2.dat
done
echo "RGMRES" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < circuit_2.dat >> tempos_circuit_2.dat
done
echo "FGMRES" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < circuit_2.dat >> tempos_circuit_2.dat
done
echo "alphaGMRES" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < circuit_2.dat >> tempos_circuit_2.dat
done
echo "HBGMRES" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < circuit_2.dat >> tempos_circuit_2.dat
done
echo "Trocou o teste - comsol"
echo "GMRES" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
	./GMRES < comsol.dat >> tempos_comsol.dat
done
echo "RGMRES" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < comsol.dat >> tempos_comsol.dat
done
echo "FGMRES" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < comsol.dat >> tempos_comsol.dat
done
echo "alphaGMRES" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < comsol.dat >> tempos_comsol.dat
done
echo "HBGMRES" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < comsol.dat >> tempos_comsol.dat
done
echo "Trocou o teste - flowmeter5"
echo "GMRES" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
	./GMRES < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "RGMRES" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "FGMRES" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "alphaGMRES" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "HBGMRES" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "Trocou o teste - fpga_trans_01"
echo "GMRES" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
	./GMRES < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "RGMRES" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "FGMRES" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "alphaGMRES" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "HBGMRES" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "Trocou o teste - fpga_trans_02"
echo "GMRES" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
	./GMRES < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "RGMRES" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "FGMRES" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "alphaGMRES" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "HBGMRES" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "Trocou o teste - raefsky1"
echo "GMRES" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
	./GMRES < raefsky1.dat >> tempos_raefsky1.dat
done
echo "RGMRES" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < raefsky1.dat >> tempos_raefsky1.dat
done
echo "FGMRES" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < raefsky1.dat >> tempos_raefsky1.dat
done
echo "alphaGMRES" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < raefsky1.dat >> tempos_raefsky1.dat
done
echo "HBGMRES" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < raefsky1.dat >> tempos_raefsky1.dat
done
echo "Trocou o teste - raefsky2"
echo "GMRES" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
	./GMRES < raefsky2.dat >> tempos_raefsky2.dat
done
echo "RGMRES" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < raefsky2.dat >> tempos_raefsky2.dat
done
echo "FGMRES" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < raefsky2.dat >> tempos_raefsky2.dat
done
echo "alphaGMRES" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < raefsky2.dat >> tempos_raefsky2.dat
done
echo "HBGMRES" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < raefsky2.dat >> tempos_raefsky2.dat
done
echo "Trocou o teste - sherman4"
echo "GMRES" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
	./GMRES < sherman4.dat >> tempos_sherman4.dat
done
echo "RGMRES" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < sherman4.dat >> tempos_sherman4.dat
done
echo "FGMRES" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < sherman4.dat >> tempos_sherman4.dat
done
echo "alphaGMRES" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < sherman4.dat >> tempos_sherman4.dat
done
echo "HBGMRES" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < sherman4.dat >> tempos_sherman4.dat
done
echo "Trocou o teste - chipcool0"
echo "RGMRES" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < chipcool0.dat >> tempos_chipcool0.dat
done
echo "FGMRES" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < chipcool0.dat >> tempos_chipcool0.dat
done
echo "alphaGMRES" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < chipcool0.dat >> tempos_chipcool0.dat
done
echo "HBGMRES" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < chipcool0.dat >> tempos_chipcool0.dat
done
echo "Trocou o teste - memplus"
echo "GMRES" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
	./GMRES < memplus.dat >> tempos_memplus.dat
done
echo "RGMRES" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < memplus.dat >> tempos_memplus.dat
done
echo "FGMRES" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < memplus.dat >> tempos_memplus.dat
done
echo "alphaGMRES" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < memplus.dat >> tempos_memplus.dat
done
echo "HBGMRES" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < memplus.dat >> tempos_memplus.dat
done
echo "Trocou o teste - wang3"
echo "RGMRES" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < wang3.dat >> tempos_wang3.dat
done
echo "FGMRES" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < wang3.dat >> tempos_wang3.dat
done
echo "alphaGMRES" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < wang3.dat >> tempos_wang3.dat
done
echo "HBGMRES" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < wang3.dat >> tempos_wang3.dat
done
echo "Trocou o teste - matrix-new_3"
echo "RGMRES" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
	./RGMRES < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
echo "FGMRES" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
	./FGMRES < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
echo "alphaGMRES" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
	./alphaGMRES < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
echo "HBGMRES" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
echo "Trocou o teste - add20"
echo "HBGMRES_par" | tee -a tempos_add20.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_add20.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < add20.dat >> tempos_add20.dat
done
echo "Trocou o teste - cavity05"
echo "HBGMRES_par" | tee -a tempos_cavity05.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity05.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < cavity05.dat >> tempos_cavity05.dat
done
echo "Trocou o teste - cavity10"
echo "HBGMRES_par" | tee -a tempos_cavity10.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_cavity10.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < cavity10.dat >> tempos_cavity10.dat
done
echo "Trocou o teste - circuit_2"
echo "HBGMRES_par" | tee -a tempos_circuit_2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_circuit_2.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < circuit_2.dat >> tempos_circuit_2.dat
done
echo "Trocou o teste - comsol"
echo "HBGMRES_par" | tee -a tempos_comsol.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_comsol.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < comsol.dat >> tempos_comsol.dat
done
echo "Trocou o teste - flowmeter5"
echo "HBGMRES_par" | tee -a tempos_flowmeter5.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_flowmeter5.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < flowmeter5.dat >> tempos_flowmeter5.dat
done
echo "Trocou o teste - fpga_trans_01"
echo "HBGMRES_par" | tee -a tempos_fpga_trans_01.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_01.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < fpga_trans_01.dat >> tempos_fpga_trans_01.dat
done
echo "Trocou o teste - fpga_trans_02"
echo "HBGMRES_par" | tee -a tempos_fpga_trans_02.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_fpga_trans_02.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < fpga_trans_02.dat >> tempos_fpga_trans_02.dat
done
echo "Trocou o teste - raefsky1"
echo "HBGMRES_par" | tee -a tempos_raefsky1.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky1.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < raefsky1.dat >> tempos_raefsky1.dat
done
echo "Trocou o teste - raefsky2"
echo "HBGMRES_par" | tee -a tempos_raefsky2.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_raefsky2.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < raefsky2.dat >> tempos_raefsky2.dat
done
echo "Trocou o teste - sherman4"
echo "HBGMRES_par" | tee -a tempos_sherman4.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_sherman4.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < sherman4.dat >> tempos_sherman4.dat
done
echo "Trocou o teste - chipcool0"
echo "HBGMRES_par" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < chipcool0.dat >> tempos_chipcool0.dat
done
echo "Trocou o teste - memplus"
echo "HBGMRES_par" | tee -a tempos_memplus.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_memplus.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < memplus.dat >> tempos_memplus.dat
done
echo "Trocou o teste - wang3"
echo "HBGMRES_par" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < wang3.dat >> tempos_wang3.dat
done
echo "Trocou o teste - matrix-new_3"
echo "HBGMRES_par" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
	./HBGMRES_par < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
echo "Trocou o teste - chipcool0"
echo "GMRES" | tee -a tempos_chipcool0.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_chipcool0.dat
	COUNTER=$((COUNTER+1))
	./GMRES < chipcool0.dat >> tempos_chipcool0.dat
done
echo "Trocou o teste - wang3"
echo "GMRES" | tee -a tempos_wang3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_wang3.dat
	COUNTER=$((COUNTER+1))
	./GMRES < wang3.dat >> tempos_wang3.dat
done
echo "Trocou o teste - matrix-new_3"
echo "GMRES" | tee -a tempos_matrix-new_3.dat
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
  echo -n "$COUNTER; " >> tempos_matrix-new_3.dat
	COUNTER=$((COUNTER+1))
	./GMRES < matrix-new_3.dat >> tempos_matrix-new_3.dat
done
