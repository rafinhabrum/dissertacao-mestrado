#!/bin/bash
echo "GMRES - add20"
./GMRES < add20.dat > vetor_sol_add20_GMRES.dat
echo "RGMRES - add20"
./RGMRES < add20.dat > vetor_sol_add20_RGMRES.dat
echo "FGMRES - add20"
./FGMRES < add20.dat > vetor_sol_add20_FGMRES.dat
echo "alphaGMRES - add20"
./alphaGMRES < add20.dat > vetor_sol_add20_alphaGMRES.dat
echo "HBGMRES - add20"
./HBGMRES < add20.dat > vetor_sol_add20_HBGMRES.dat
echo "Trocou o teste - cavity05"
echo "GMRES - cavity05"
./GMRES < cavity05.dat > vetor_sol_cavity05_GMRES.dat
echo "RGMRES - cavity05"
./RGMRES < cavity05.dat > vetor_sol_cavity05_RGMRES.dat
echo "FGMRES - cavity05"
./FGMRES < cavity05.dat > vetor_sol_cavity05_FGMRES.dat
echo "alphaGMRES - cavity05"
./alphaGMRES < cavity05.dat > vetor_sol_cavity05_alphaGMRES.dat
echo "HBGMRES - cavity05"
./HBGMRES < cavity05.dat > vetor_sol_cavity05_HBGMRES.dat
echo "Trocou o teste - cavity10"
echo "GMRES - cavity10"
./GMRES < cavity10.dat > vetor_sol_cavity10_GMRES.dat
echo "RGMRES - cavity10"
./RGMRES < cavity10.dat > vetor_sol_cavity10_RGMRES.dat
echo "FGMRES - cavity10"
./FGMRES < cavity10.dat > vetor_sol_cavity10_FGMRES.dat
echo "alphaGMRES - cavity10"
./alphaGMRES < cavity10.dat > vetor_sol_cavity10_alphaGMRES.dat
echo "HBGMRES - cavity10"
./HBGMRES < cavity10.dat > vetor_sol_cavity10_HBGMRES.dat
echo "Trocou o teste - chipcool0"
echo "GMRES - chipcool0"
./GMRES < chipcool0.dat > vetor_sol_chipcool0_GMRES.dat
echo "RGMRES - chipcool0"
./RGMRES < chipcool0.dat > vetor_sol_chipcool0_RGMRES.dat
echo "FGMRES - chipcool0"
./FGMRES < chipcool0.dat > vetor_sol_chipcool0_FGMRES.dat
echo "alphaGMRES - chipcool0"
./alphaGMRES < chipcool0.dat > vetor_sol_chipcool0_alphaGMRES.dat
echo "HBGMRES - chipcool0"
./HBGMRES < chipcool0.dat > vetor_sol_chipcool0_HBGMRES.dat
echo "Trocou o teste - circuit_2"
echo "GMRES - circuit_2"
./GMRES < circuit_2.dat > vetor_sol_circuit_2_GMRES.dat
echo "RGMRES - circuit_2"
./RGMRES < circuit_2.dat > vetor_sol_circuit_2_RGMRES.dat
echo "FGMRES - circuit_2"
./FGMRES < circuit_2.dat > vetor_sol_circuit_2_FGMRES.dat
echo "alphaGMRES - circuit_2"
./alphaGMRES < circuit_2.dat > vetor_sol_circuit_2_alphaGMRES.dat
echo "HBGMRES - circuit_2"
./HBGMRES < circuit_2.dat > vetor_sol_circuit_2_HBGMRES.dat
echo "Trocou o teste - comsol"
echo "GMRES - comsol"
./GMRES < comsol.dat > vetor_sol_comsol_GMRES.dat
echo "RGMRES - comsol"
./RGMRES < comsol.dat > vetor_sol_comsol_RGMRES.dat
echo "FGMRES - comsol"
./FGMRES < comsol.dat > vetor_sol_comsol_FGMRES.dat
echo "alphaGMRES - comsol"
./alphaGMRES < comsol.dat > vetor_sol_comsol_alphaGMRES.dat
echo "HBGMRES - comsol"
./HBGMRES < comsol.dat > vetor_sol_comsol_HBGMRES.dat
echo "Trocou o teste - flowmeter5"
echo "GMRES - flowmeter5"
./GMRES < flowmeter5.dat > vetor_sol_flowmeter5_GMRES.dat
echo "RGMRES - flowmeter5"
./RGMRES < flowmeter5.dat > vetor_sol_flowmeter5_RGMRES.dat
echo "FGMRES - flowmeter5"
./FGMRES < flowmeter5.dat > vetor_sol_flowmeter5_FGMRES.dat
echo "alphaGMRES - flowmeter5"
./alphaGMRES < flowmeter5.dat > vetor_sol_flowmeter5_alphaGMRES.dat
echo "HBGMRES - flowmeter5"
./HBGMRES < flowmeter5.dat > vetor_sol_flowmeter5_HBGMRES.dat
echo "Trocou o teste - fpga_trans_01"
echo "GMRES - fpga_trans_01"
./GMRES < fpga_trans_01.dat > vetor_sol_fpga_trans_01_GMRES.dat
echo "RGMRES - fpga_trans_01"
./RGMRES < fpga_trans_01.dat > vetor_sol_fpga_trans_01_RGMRES.dat
echo "FGMRES - fpga_trans_01"
./FGMRES < fpga_trans_01.dat > vetor_sol_fpga_trans_01_FGMRES.dat
echo "alphaGMRES - fpga_trans_01"
./alphaGMRES < fpga_trans_01.dat > vetor_sol_fpga_trans_01_alphaGMRES.dat
echo "HBGMRES - fpga_trans_01"
./HBGMRES < fpga_trans_01.dat > vetor_sol_fpga_trans_01_HBGMRES.dat
echo "Trocou o teste - fpga_trans_02"
echo "GMRES - fpga_trans_02"
./GMRES < fpga_trans_02.dat > vetor_sol_fpga_trans_02_GMRES.dat
echo "RGMRES - fpga_trans_02"
./RGMRES < fpga_trans_02.dat > vetor_sol_fpga_trans_02_RGMRES.dat
echo "FGMRES - fpga_trans_02"
./FGMRES < fpga_trans_02.dat > vetor_sol_fpga_trans_02_FGMRES.dat
echo "alphaGMRES - fpga_trans_02"
./alphaGMRES < fpga_trans_02.dat > vetor_sol_fpga_trans_02_alphaGMRES.dat
echo "HBGMRES - fpga_trans_02"
./HBGMRES < fpga_trans_02.dat > vetor_sol_fpga_trans_02_HBGMRES.dat
echo "Trocou o teste - matrix-new_3"
echo "GMRES - matrix-new_3"
./GMRES < matrix-new_3.dat > vetor_sol_matrix-new_3_GMRES.dat
echo "RGMRES - matrix-new_3"
./RGMRES < matrix-new_3.dat > vetor_sol_matrix-new_3_RGMRES.dat
echo "FGMRES - matrix-new_3"
./FGMRES < matrix-new_3.dat > vetor_sol_matrix-new_3_FGMRES.dat
echo "alphaGMRES - matrix-new_3"
./alphaGMRES < matrix-new_3.dat > vetor_sol_matrix-new_3_alphaGMRES.dat
echo "HBGMRES - matrix-new_3"
./HBGMRES < matrix-new_3.dat > vetor_sol_matrix-new_3_HBGMRES.dat
echo "Trocou o teste - memplus"
echo "GMRES - memplus"
./GMRES < memplus.dat > vetor_sol_memplus_GMRES.dat
echo "RGMRES - memplus"
./RGMRES < memplus.dat > vetor_sol_memplus_RGMRES.dat
echo "FGMRES - memplus"
./FGMRES < memplus.dat > vetor_sol_memplus_FGMRES.dat
echo "alphaGMRES - memplus"
./alphaGMRES < memplus.dat > vetor_sol_memplus_alphaGMRES.dat
echo "HBGMRES - memplus"
./HBGMRES < memplus.dat > vetor_sol_memplus_HBGMRES.dat
echo "Trocou o teste - raefsky1"
echo "GMRES - raefsky1"
./GMRES < raefsky1.dat > vetor_sol_raefsky1_GMRES.dat
echo "RGMRES - raefsky1"
./RGMRES < raefsky1.dat > vetor_sol_raefsky1_RGMRES.dat
echo "FGMRES - raefsky1"
./FGMRES < raefsky1.dat > vetor_sol_raefsky1_FGMRES.dat
echo "alphaGMRES - raefsky1"
./alphaGMRES < raefsky1.dat > vetor_sol_raefsky1_alphaGMRES.dat
echo "HBGMRES - raefsky1"
./HBGMRES < raefsky1.dat > vetor_sol_raefsky1_HBGMRES.dat
echo "Trocou o teste - raefsky2"
echo "GMRES - raefsky2"
./GMRES < raefsky2.dat > vetor_sol_raefsky2_GMRES.dat
echo "RGMRES - raefsky2"
./RGMRES < raefsky2.dat > vetor_sol_raefsky2_RGMRES.dat
echo "FGMRES - raefsky2"
./FGMRES < raefsky2.dat > vetor_sol_raefsky2_FGMRES.dat
echo "alphaGMRES - raefsky2"
./alphaGMRES < raefsky2.dat > vetor_sol_raefsky2_alphaGMRES.dat
echo "HBGMRES - raefsky2"
./HBGMRES < raefsky2.dat > vetor_sol_raefsky2_HBGMRES.dat
echo "Trocou o teste - sherman4"
echo "GMRES - sherman4"
./GMRES < sherman4.dat > vetor_sol_sherman4_GMRES.dat
echo "RGMRES - sherman4"
./RGMRES < sherman4.dat > vetor_sol_sherman4_RGMRES.dat
echo "FGMRES - sherman4"
./FGMRES < sherman4.dat > vetor_sol_sherman4_FGMRES.dat
echo "alphaGMRES - sherman4"
./alphaGMRES < sherman4.dat > vetor_sol_sherman4_alphaGMRES.dat
echo "HBGMRES - sherman4"
./HBGMRES < sherman4.dat > vetor_sol_sherman4_HBGMRES.dat
echo "Trocou o teste - wang3"
echo "GMRES - wang3"
./GMRES < wang3.dat > vetor_sol_wang3_GMRES.dat
echo "RGMRES - wang3"
./RGMRES < wang3.dat > vetor_sol_wang3_RGMRES.dat
echo "FGMRES - wang3"
./FGMRES < wang3.dat > vetor_sol_wang3_FGMRES.dat
echo "alphaGMRES - wang3"
./alphaGMRES < wang3.dat > vetor_sol_wang3_alphaGMRES.dat
echo "HBGMRES - wang3"
./HBGMRES < wang3.dat > vetor_sol_wang3_HBGMRES.dat
