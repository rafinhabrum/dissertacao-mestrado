/* Algoritmo em FORTRAN-90 para produto de matriz por vetor usando o formato CSR de armazenamento
DO I=1, N
  K1 = IA(I)
  K2 = IA(I+1)-1
  Y(I) = DOTPRODUCT(A(K1:K2),X(JA(K1:K2)))
ENDDO
*/
/* Formato Compressed Sparse Row - CSR (p. 113 do Saad)
Usa 3 arrays para armazenar a matriz esparsa:
-> o AA para os valores numéricos (não-nulos) da matriz A na ordem por linha, os valores da primeira
linha seguidos dos valores da segunda linha... (a princípio do tipo double) -> tamanho Nz (qtde de valores não nulos)
-> o JA para armazenar o índice da coluna de cada um dos valores de AA (sempre do tipo inteiro) -> tamanho Nz
-> o IA, de tamanho N+1, para armazenar onde cada linha da matriz começa nos dois arrays acima. A
posição N+1 aponta para Nz+1 (sempre do tipo inteiro)
*/

#include <stdio.h>
#include <string.h>

#define NZ 13
#define NMATRIZ 6

double produtoVetorial(double *a, double *b, int size)
{
    int result = 0;
    for(int i = 1; i <= size; ++i)
    {
        result += (a[i]*b[i]);
    }
    return result;
}

void imprimirVetor(double *a, int size)
{
    for(int i = 1; i <= size; ++i)
    {
        printf("%.0lf ", a[i]);
    }
    printf("\n");
}

int main(){
    double AA[NZ];
    int JA[NZ], IA[NMATRIZ+1];
    double X[NMATRIZ], Y[NMATRIZ];
    double auxAA[NMATRIZ], auxX[NMATRIZ];

    int i;
    int k1, k2;

    double mat[NMATRIZ][NMATRIZ];
    int row, indexIA;

    //ler vetor AA
    for(i = 1; i < NZ; ++i)
    {
        scanf("%lf", &AA[i]);
    }
    //ler vetor JR
    for(i = 1; i < NZ; ++i)
    {
        scanf("%d", &JA[i]);
    }
    //ler vetor JC
    for(i = 1; i <= NMATRIZ; ++i)
    {
        scanf("%d", &IA[i]);
    }

    //ler vetor X
    for(i = 1; i < NMATRIZ; ++i)
    {
        scanf("%lf", &X[i]);
    }

    for(i = 1; i < NMATRIZ; ++i)
    {
        k1 = IA[i];
        k2 = IA[i+1];
        //printf("k1 = %d | k2 = %d\n", k1, k2);
        for(int j = k1; j < k2; ++j)
        {
            auxAA[j-k1+1] = AA[j];
            auxX[j-k1+1] = X[JA[j]];
        }
        //imprimirVetor(auxAA, k2-k1);
        //imprimirVetor(auxX, k2-k1);
        Y[i] = produtoVetorial(auxAA, auxX, k2-k1);
    }

    printf("Vetor Y:\n");

    for(i = 1; i < NMATRIZ; ++i)
    {
        printf("%.0lf ", Y[i]);
    }

    printf("\n");
}
