import numpy as np
import matplotlib.pylab as plt
import scipy.sparse as sparse

f = open("cavity05.dat", "r")
line = f.readline()
numbers = [int(s) for s in line.split() if s.isdigit()]
indptr = np.zeros(numbers[0]+1)
indices = np.zeros(numbers[1])
data = np.zeros(numbers[1])
for i in range(numbers[1]):
    line = f.readline()
    data[i] = float(line)
for i in range(numbers[1]):
    line = f.readline()
    indices[i] = int(line)
for i in range(numbers[0] + 1):
    line = f.readline()
    indptr[i] = int(line)
B = sparse.csr_matrix((data, indices, indptr))

C = B.tocoo()

## create a sparse diagonal matrix with ones on the diagonal
#A = sparse.eye(100)
## visualize the sparse matrix with Spy
plt.spy(B)

plt.spy(C)

f.close()
plt.show()