import numpy as np
import matplotlib.pylab as plt
import scipy.sparse as sparse

f = open("cavity05_coordinate.dat", "r")
line = f.readline()
numbers = [int(s) for s in line.split() if s.isdigit()]
row = np.zeros(numbers[1])
col = np.zeros(numbers[1])
data = np.zeros(numbers[1])
for i in range(numbers[1]):
    line = f.readline()
    data[i] = float(line)
for i in range(numbers[1]):
    line = f.readline()
    col[i] = int(line)
for i in range(numbers[1]):
    line = f.readline()
    row[i] = int(line)

C = sparse.coo_matrix((data, (row, col)))

B = C.tocsr()
## create a sparse diagonal matrix with ones on the diagonal
#A = sparse.eye(100)
## visualize the sparse matrix with Spy
#plt.spy(C)
plt.spy(B)
f.close()
plt.show()