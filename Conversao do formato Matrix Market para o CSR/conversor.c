/*
*   Matrix Market I/O example program
*
*   Read a real (non-complex) sparse matrix from a Matrix Market (v. 2.0) file.
*   and copies it to stdout.  This porgram does nothing useful, but
*   illustrates common usage of the Matrix Matrix I/O routines.
*   (See http://math.nist.gov/MatrixMarket for details.)
*
*   Usage:  a.out [filename] > output
*
*
*   NOTES:
*
*   1) Matrix Market files are always 1-based, i.e. the index of the first
*      element of a matrix is (1,1), not (0,0) as in C.  ADJUST THESE
*      OFFSETS ACCORDINGLY offsets accordingly when reading and writing
*      to files.
*
*   2) ANSI C requires one to use the "l" format modifier when reading
*      double precision floating point numbers in scanf() and
*      its variants.  For example, use "%lf", "%lg", or "%le"
*      when reading doubles, otherwise errors will occur.
*/

#include <stdio.h>
#include <stdlib.h>
#include "mmio.h"

void imprimirVetorD(double *, int size, char *nome);
void imprimirVetorI(int *a, int size, char *nome);
void imprimirMatrizCheia(double **mat, int M);

int main(int argc, char *argv[])
{
    int ret_code;
    MM_typecode matcode;
    FILE *f;
    int M, N, nz;
    int i, *I, *J;
    double *val;
    double *AA;
    int *IA, *JA;
    int i_atual, j_atual, trocar;
    int index, k, l;

    if (argc < 2)
	{
		fprintf(stderr, "Usage: %s [martix-market-filename]\n", argv[0]);
		exit(1);
	}
    else
    {
        if ((f = fopen(argv[1], "r")) == NULL)
            exit(1);
    }

    if (mm_read_banner(f, &matcode) != 0)
    {
        printf("Could not process Matrix Market banner.\n");
        exit(1);
    }


    /*  This is how one can screen matrix types if their application */
    /*  only supports a subset of the Matrix Market data types.      */

    if (mm_is_complex(matcode) && mm_is_matrix(matcode) &&
            mm_is_sparse(matcode) )
    {
        printf("Sorry, this application does not support ");
        printf("Market Market type: [%s]\n", mm_typecode_to_str(matcode));
        exit(1);
    }

    /* find out size of sparse matrix .... */

    if ((ret_code = mm_read_mtx_crd_size(f, &M, &N, &nz)) !=0)
        exit(1);


    /* reseve memory for matrices */

    I = (int *) malloc(nz * sizeof(int));
    J = (int *) malloc(nz * sizeof(int));
    val = (double *) malloc(nz * sizeof(double));


    /* NOTE: when reading in doubles, ANSI C requires the use of the "l"  */
    /*   specifier as in "%lg", "%lf", "%le", otherwise errors will occur */
    /*  (ANSI C X3.159-1989, Sec. 4.9.6.2, p. 136 lines 13-15)            */

    for (i=0; i<nz; i++)
    {
        fscanf(f, "%d %d %lg\n", &I[i], &J[i], &val[i]);
        I[i]--;  /* adjust from 1-based to 0-based */
        J[i]--;
    }

    if (f !=stdin) fclose(f);

    /************************/
    /* now write out matrix */
    /************************/

    /*imprimirVetorD(val, nz, "val");

    imprimirVetorI(I, nz, "I");

    imprimirVetorI(J, nz, "J");*/
    
    /*printf("%d %d %d\n", M, nz, 30);
    
    for(i = 0; i < nz; ++i)
    {
		printf("%lg\n", val[i]);
	}
	//printf("aqui");
	//printf("\n");
	for(i = 0; i < nz; ++i)
    {
		printf("%d\n", J[i]);
	}
	//printf("aqui");
	for(i = 0; i < nz; ++i)
    {
		printf("%d\n", I[i]);
	}
	
	return 0;*/


    if(M != N)
    {
      printf("Matriz nao quadrada!!!");
    }

    AA = (double *) malloc(nz*sizeof(double));
    JA = (int *) malloc(nz*sizeof(int));
    IA = (int *) calloc(M+1, sizeof(int));

    for(i = 0; i < nz; ++i)
    {
      JA[i] = -1;
    }

    for(index = 0; index < nz; ++index)
    {
      i_atual = I[index];

      IA[M]++;

      for(k = M-1; k > i_atual; --k)
      {
        if(IA[k] != IA[k+1])
        {
          for(l = IA[k+1]-1; l > IA[k]; --l)
          {
            JA[l] = JA[l-1];
            AA[l] = AA[l-1];
          }
        }
        IA[k]++;
      }

      /*if(index == 6)
      {
        printf("\nErro!!!\n");
        printf("index = %d\n", index);
        imprimirVetorD(AA, nz, "AA");
        imprimirVetorI(JA, nz, "JA");
        imprimirVetorI(IA, M+1, "IA");
      }*/

      j_atual = J[index];

      for(k = IA[i_atual]; k < IA[i_atual+1]; ++k)
      {
        if((JA[k] == -1) || (JA[k] > j_atual))
        {
          trocar = k;
          //printf("\ntrocar = %d\n", trocar);
          break;
        }
      }

      if(k == IA[i_atual+1])
      {
        trocar = IA[i_atual+1]-1;
        //printf("\nvai colocar no ultimo - trocar = %d\n", trocar);
      }
      if(JA[trocar] != -1)
      {
        for(k = IA[i_atual+1]-1; k > trocar; --k)
        {
          JA[k] = JA[k-1];
          AA[k] = AA[k-1];
        }
      }

      JA[trocar] = j_atual;
      AA[trocar] = val[index];

      /*printf("index = %d\n", index);
      imprimirVetorD(AA, nz, "AA");
      imprimirVetorI(JA, nz, "JA");
      imprimirVetorI(IA, M+1, "IA");*/

    }

    /*imprimirVetorD(AA, nz, "AA");
    imprimirVetorI(JA, nz, "JA");
    imprimirVetorI(IA, M+1, "IA");

    double **mat = (double**)malloc(M*sizeof(double *));
    for(i = 0; i < M; i++)
    {
      mat[i] = (double*)calloc(M,sizeof(double));
    }


    int row, indexIA;
    row = 0;
    indexIA = 1;
    for(i = 0; i < nz; ++i)
    {
        if(IA[indexIA] == i)
        {
            row++;
            indexIA++;
        }
        mat[row][JA[i]] = AA[i];
    }

    for(i = 0; i < M; ++i)
    {
        for(int j = 0; j < M; ++j)
        {
            printf("%lf ", mat[i][j]);
        }
        printf("\n");
    }*/
    
    printf("%d %d %d\n", M, nz, 30);
    
    for(i = 0; i < nz; ++i)
    {
		printf("%lg\n", AA[i]);
	}
	//printf("aqui");
	//printf("\n");
	for(i = 0; i < nz; ++i)
    {
		printf("%d\n", JA[i]);
	}
	//printf("aqui");
	for(i = 0; i <= M; ++i)
    {
		printf("%d\n", IA[i]);
	}


	return 0;
}

void imprimirVetorD(double *a, int size, char *nome)
{
	printf("Vetor %s\n", nome);
    for(int i = 0; i < size; ++i)
    {
        printf("%.7lf\n", a[i]);
    }
    printf("\n");
}

void imprimirVetorI(int *a, int size, char *nome)
{
	printf("Vetor %s\n", nome);
    for(int i = 0; i < size; ++i)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
}

void imprimirMatrizCheia(double **mat, int M)
{
    int i, j;
    for(i = 0; i < M; ++i)
    {
        for(j = 0; j < M; ++j)
        {
            printf("%lf ", mat[i][j]);
        }
        printf("\n");
    }
}
