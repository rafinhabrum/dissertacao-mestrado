#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*Criacao de matriz cheia com notacao de matriz esparsa CSR (vetor AA, vetor JA e vetor IA)

Formato Compressed Sparse Row - CSR (p. 113 do Saad)
Usa 3 arrays para armazenar a matriz esparsa:
-> o AA para os valores numéricos (não-nulos) da matriz A na ordem por linha, os valores da primeira
linha seguidos dos valores da segunda linha... (a princípio do tipo double) -> tamanho Nz (qtde de valores não nulos)
-> o JA para armazenar o índice da coluna de cada um dos valores de AA (sempre do tipo inteiro) -> tamanho Nz
-> o IA, de tamanho N+1, para armazenar onde cada linha da matriz começa nos dois arrays acima. A
posição N+1 aponta para Nz+1 (sempre do tipo inteiro) */

typedef struct  matriz {
    int m, n;
    double **dados;
} matriz;

void imprimirVetorD(double *dados, int size)
{
    int i, j;
    for(i = 0; i < size; i++)
    {
		printf("%lf ", dados[i]);
    }
    printf("\n");
}

void imprimirVetorI(int *dados, int size)
{
    int i, j;
    for(i = 0; i < size; i++)
    {
		printf("%d ", dados[i]);
    }
    printf("\n");
}

void imprimirVetorAA(matriz mat)
{
    int i, j;
    for(i = 0; i < mat.m; i++)
    {
        for(j = 0; j < mat.n; j++)
        {
            printf("%lf ", mat.dados[i][j]);
        }
    }
    printf("\n");
}

void imprimirVetorJA(int tam)
{
    int i, j;
    for(i = 0; i < tam; i++)
    {
        for(j = 1; j <= tam; j++)
        {
            printf("%d ", j);
        }
    }
    printf("\n");
}

void imprimirVetorIA(int tam)
{
    int i, j;
    for(i = 1; i <= (tam*tam); i+=tam)
    {
		printf("%d ", i);
    }
    printf("%d \n", i);
}

void imprimirVetorB(double* B, int tam)
{
    int i, j;
    for(i = 0; i < tam; i++)
    {
		printf("%lf ", B[i]);
    }
    printf("\n");
}

matriz multiplicarMatrizes(matriz R, matriz Q)
{
    int i, j, k;
    matriz result;

    if(R.n != Q.m)
    {
        fprintf(stderr, "Matrizes imcompativeis: A com ordem %dx%d e B com ordem %dx%d.\n", R.m, R.n, Q.m, Q.n);
        return;
    }
    result.m = R.m;
    result.n = Q.n;
    result.dados = (double **)malloc(result.n*sizeof(double*));
    for(i = 0; i < result.n; i++)
    {
        *(result.dados + i) = (double *)malloc(result.m*sizeof(double));
    }
    
    #pragma omp parallel for private(j, k)
    for(i = 0; i < result.m; i++)
    {
        for(j = 0; j < result.n; j++)
        {
            *(*(result.dados + j) + i) = 0;
            for(k = 0; k < R.n; k++)
            {
                *(*(result.dados+j)+i) += ((*(*(R.dados+i)+k))*(*(*(Q.dados+k)+j)));
            }
        }
    }
    return result;
}

void multiplicarMatrizPorVetor(matriz R, double *Q, double *result)
{
    int i, j, k;
	
	#pragma omp parallel for private(k)
    for(i = 0; i < R.m; i++)
    {
      *(result + i) = 0;
      for(k = 0; k < R.n; k++)
      {
          *(result+i) += ((*(*(R.dados+i)+k))*(*(Q+k)));
      }
    }
}

int main()
{
    matriz mat;
    int i, j;
    double *variaveis;
    double *vetorX;
    double *AA;
    int tamAA = 0;
    int *JA, *IA;

    srand(1);

    //scanf("%d %d", &mat.m, &mat.n);
    mat.m = mat.n = 200;

    mat.dados = (double**)malloc(mat.m*sizeof(double*));
    for(i = 0; i < mat.m; i++)
    {
        *(mat.dados + i) = (double *)calloc((mat.n),sizeof(double));
    }
    variaveis = (double *)malloc((mat.n)*sizeof(double));
    vetorX = (double *)malloc((mat.n)*sizeof(double));
    
    fprintf(stderr, "Populando a matriz\n");

    #pragma omp parallel for
    for(i = 0; i < mat.m; i++)
    {
		for(j = 0; j < mat.m; j++)
		{
		//scanf("%lf", (*(mat.dados + i) + j));
		//if((i*i)%2)
		//{
			//fprintf(stderr, "i = %d - j = %d\n", i, j);
			if((j == (i)) || (i > 2) && (j == (i-2)) || (i+2 < mat.m) && (j == (i+2)))
			{
				*(*(mat.dados + i) + j) = rand()%10 + 1;
			}
			else
			{
				*(*(mat.dados + i) + j) = 0;
			}
			//*(*(mat.dados + i) + j) = rand()%10;
		//}
		}
        *(vetorX + i) = 1;
    }
    
    fprintf(stderr, "Achando o vetor b\n");

    multiplicarMatrizPorVetor(mat, vetorX, variaveis);

    //printf("%d\n", mat.m);
    
    AA = (double *)malloc((mat.m*mat.m)*sizeof(double));
    JA = (int *)malloc((mat.m*mat.m)*sizeof(int));
    IA = (int *)malloc((mat.m+1)*sizeof(int));
    
    //~ printf("aqui?\n");
    
    //~ mat.dados[0][0] = 1;
    //~ mat.dados[0][3] = 2;
    //~ mat.dados[1][0] = 3;
    //~ mat.dados[1][1] = 4;
    //~ mat.dados[1][3] = 5;
    //~ mat.dados[2][0] = 6;
    //~ mat.dados[2][2] = 7;
    //~ mat.dados[2][3] = 8;
    //~ mat.dados[2][4] = 9;
    //~ mat.dados[3][2] = 10;
    //~ mat.dados[3][3] = 11;
    //~ mat.dados[4][4] = 12;
    
    fprintf(stderr, "Criando o esquema CSR\n");
    
    IA[0] = 1;
    for(i = 0; i < mat.m; i++)
    {
		for(j = 0; j < mat.m; j++)
		{
			if(mat.dados[i][j] != 0)
			{
				//~ printf("%lf \n", mat.dados[i][j]);
				AA[tamAA] = mat.dados[i][j];
				JA[tamAA++] = j+1;
			}
		}
		IA[i+1] = tamAA+1;
	}
	
	fprintf(stderr, "Imprimindo...\n");
    
    printf("%d %d\n", tamAA+1, mat.m+1);
    imprimirVetorD(AA, tamAA);
    imprimirVetorI(JA, tamAA);
    imprimirVetorI(IA, mat.m+1);
    imprimirVetorD(variaveis, mat.m);
	
	fprintf(stderr, "Liberando a memória...!\n");

    for(i = 0; i < mat.m; i++)
    {
        free(mat.dados[i]);
    }
    free(mat.dados);
    free(variaveis);
    free(AA);
    free(JA);
    free(IA);

    return 0;
}
