#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Só tem decomposicao Cholesky se a matriz for simetrica definida positiva, ou seja, se os elementos da diagonal forem positivos (por ser simétrica).
A decomposicao de Cholesky é A = GGt, onde G é triangular inferior, com os elementos calculados como no caderno.
O sistema linear é calculado como Gy=b e Gtx = y*/

typedef struct  matriz {
    int m, n;
    double **dados;
} matriz;

void imprimirMatriz(matriz mat, double *B)
{
    int i, j;
    for(i = 0; i < mat.m; i++)
    {
        //for(j = 0; j < n-1; j++)
        for(j = 0; j < mat.n; j++)//for(j = 0; j < mat.n+1; j++)
        {
            //printf("%s %lf*x%d ", ((j)?"+":""), dados[i][j], j);
            printf("%lf ", mat.dados[i][j]);
        }
        printf("%lf\n", B[i]);
        //printf("= %lf\n", dados[i][j]);
    }
}

matriz multiplicarMatrizes(matriz R, matriz Q)
{
    int i, j, k;
    matriz result;

    if(R.n != Q.m)
    {
        fprintf(stderr, "Matrizes imcompativeis: A com ordem %dx%d e B com ordem %dx%d.\n", R.m, R.n, Q.m, Q.n);
        return;
    }
    result.m = R.m;
    result.n = Q.n;
    result.dados = (double **)malloc(result.n*sizeof(double*));
    for(i = 0; i < result.n; i++)
    {
        *(result.dados + i) = (double *)malloc(result.m*sizeof(double));
    }
    #pragma omp parallel for private(j, k)
    for(i = 0; i < result.m; i++)
    {
        for(j = 0; j < result.n; j++)
        {
            *(*(result.dados + j) + i) = 0;
            for(k = 0; k < R.n; k++)
            {
                *(*(result.dados+j)+i) += ((*(*(R.dados+i)+k))*(*(*(Q.dados+k)+j)));
            }
        }
    }
    return result;
}

void multiplicarMatrizPorVetor(matriz R, double *Q, double *result)
{
    int i, j, k;

    for(i = 0; i < R.m; i++)
    {
      *(result + i) = 0;
	  #pragma omp parallel for private(k)
      for(k = 0; k < R.n; k++)
      {
          *(result+i) += ((*(*(R.dados+i)+k))*(*(Q+k)));
      }
    }
}

int main()
{
    matriz mat, matT;
    int i, j;
    double *variaveis;
    double *vetorX;

    srand(1);

    //scanf("%d %d", &mat.m, &mat.n);
    matT.m = matT.n = mat.m = mat.n = 200;

    mat.dados = (double**)malloc(mat.m*sizeof(double*));
    matT.dados = (double**)malloc(matT.m*sizeof(double*));
    for(i = 0; i < mat.m; i++)
    {
        *(mat.dados + i) = (double *)malloc((mat.n)*sizeof(double));
        *(matT.dados + i) = (double *)malloc((matT.n)*sizeof(double));
    }
    variaveis = (double *)malloc((mat.n)*sizeof(double));
    vetorX = (double *)malloc((mat.n)*sizeof(double));
	
    #pragma omp parallel for private(j)
    for(i = 0; i < mat.m; i++)
    {
        for(j = 0; j < mat.n; j++)
        {
            //scanf("%lf", (*(mat.dados + i) + j));
            *(*(mat.dados + i) + j) = rand()%100;
            *(*(matT.dados + j) + i) = *(*(mat.dados + i) + j);
        }
        *(vetorX + i) = 1;
    }

    mat = multiplicarMatrizes(mat, matT);

	#pragma omp parallel for
    for(i = 0; i < mat.m; i++)
    {
        mat.dados[i][i] *= 1500;
    }

    multiplicarMatrizPorVetor(mat, vetorX, variaveis);

    printf("%d\n", mat.m);
    imprimirMatriz(mat, variaveis);

    for(i = 0; i < mat.m; i++)
    {
        free(mat.dados[i]);
        free(matT.dados[i]);
    }
    free(mat.dados);
    free(matT.dados);
    free(variaveis);

    return 0;
}
