#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*Criacao de matriz cheia com notacao de matriz esparsa CSR (vetor AA, vetor JA e vetor IA)

Formato Compressed Sparse Row - CSR (p. 113 do Saad)
Usa 3 arrays para armazenar a matriz esparsa:
-> o AA para os valores numéricos (não-nulos) da matriz A na ordem por linha, os valores da primeira
linha seguidos dos valores da segunda linha... (a princípio do tipo double) -> tamanho Nz (qtde de valores não nulos)
-> o JA para armazenar o índice da coluna de cada um dos valores de AA (sempre do tipo inteiro) -> tamanho Nz
-> o IA, de tamanho N+1, para armazenar onde cada linha da matriz começa nos dois arrays acima. A
posição N+1 aponta para Nz+1 (sempre do tipo inteiro) */

typedef struct  matriz {
    int m, n;
    double **dados;
} matriz;

void imprimirVetorAA(matriz mat)
{
    int i, j;
    for(i = 0; i < mat.m; i++)
    {
        for(j = 0; j < mat.n; j++)
        {
            printf("%lf ", mat.dados[i][j]);
        }
    }
    printf("\n");
}

void imprimirVetorJA(int tam)
{
    int i, j;
    for(i = 0; i < tam; i++)
    {
        for(j = 1; j <= tam; j++)
        {
            printf("%d ", j);
        }
    }
    printf("\n");
}

void imprimirVetorIA(int tam)
{
    int i, j;
    for(i = 1; i <= (tam*tam); i+=tam)
    {
		printf("%d ", i);
    }
    printf("%d \n", i);
}

void imprimirVetorB(double* B, int tam)
{
    int i, j;
    for(i = 0; i < tam; i++)
    {
		printf("%lf ", B[i]);
    }
    printf("\n");
}

matriz multiplicarMatrizes(matriz R, matriz Q)
{
    int i, j, k;
    matriz result;

    if(R.n != Q.m)
    {
        fprintf(stderr, "Matrizes imcompativeis: A com ordem %dx%d e B com ordem %dx%d.\n", R.m, R.n, Q.m, Q.n);
        return;
    }
    result.m = R.m;
    result.n = Q.n;
    result.dados = (double **)malloc(result.n*sizeof(double*));
    for(i = 0; i < result.n; i++)
    {
        *(result.dados + i) = (double *)malloc(result.m*sizeof(double));
    }
    
    #pragma omp parallel for private(j, k)
    for(i = 0; i < result.m; i++)
    {
        for(j = 0; j < result.n; j++)
        {
            *(*(result.dados + j) + i) = 0;
            for(k = 0; k < R.n; k++)
            {
                *(*(result.dados+j)+i) += ((*(*(R.dados+i)+k))*(*(*(Q.dados+k)+j)));
            }
        }
    }
    return result;
}

void multiplicarMatrizPorVetor(matriz R, double *Q, double *result)
{
    int i, j, k;
	
	#pragma omp parallel for private(k)
    for(i = 0; i < R.m; i++)
    {
      *(result + i) = 0;
      for(k = 0; k < R.n; k++)
      {
          *(result+i) += ((*(*(R.dados+i)+k))*(*(Q+k)));
      }
    }
}

int main()
{
    matriz mat, matT;
    int i, j;
    double *variaveis;
    double *vetorX;

    srand(1);

    //scanf("%d %d", &mat.m, &mat.n);
    matT.m = matT.n = mat.m = mat.n = 5000;

    mat.dados = (double**)malloc(mat.m*sizeof(double*));
    matT.dados = (double**)malloc(matT.m*sizeof(double*));
    for(i = 0; i < mat.m; i++)
    {
        *(mat.dados + i) = (double *)malloc((mat.n)*sizeof(double));
        *(matT.dados + i) = (double *)malloc((matT.n)*sizeof(double));
    }
    variaveis = (double *)malloc((mat.n)*sizeof(double));
    vetorX = (double *)malloc((mat.n)*sizeof(double));


    #pragma omp parallel for private(j)
    for(i = 0; i < mat.m; i++)
    {
        for(j = 0; j < mat.n; j++)
        {
            //scanf("%lf", (*(mat.dados + i) + j));
            *(*(mat.dados + i) + j) = rand()%100;
            *(*(matT.dados + j) + i) = *(*(mat.dados + i) + j);
        }
        *(vetorX + i) = 1;
    }

    mat = multiplicarMatrizes(mat, matT);

    /*for(i = 0; i < mat.m; i++)
    {
        mat.dados[i][i] *= 1500;
    }*/

    multiplicarMatrizPorVetor(mat, vetorX, variaveis);

    //printf("%d\n", mat.m);
    imprimirVetorAA(mat);
    imprimirVetorJA(mat.m);
    imprimirVetorIA(mat.m);
    imprimirVetorB(variaveis, mat.m);

    for(i = 0; i < mat.m; i++)
    {
        free(mat.dados[i]);
        free(matT.dados[i]);
    }
    free(mat.dados);
    free(matT.dados);
    free(variaveis);

    return 0;
}
