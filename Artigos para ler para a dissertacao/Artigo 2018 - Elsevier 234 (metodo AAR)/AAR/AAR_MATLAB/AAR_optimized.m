function [x] = AAR(A,b,x_guess,tol,max_iter,omega,beta,m,p,L,U)
% Preconditioned Alternating Anderson-Richardson (AAR) MATLAB code
% Copyright (C) 2018 Material Physics & Mechanics Group at Georgia Tech.
% Authors: Phanisri Pradeep Pratapa, Phanish Suryanarayana
% (In collaboration with John E. Pask.)
% Last Modified: 26 March 2018  
% Solves the system Ax = b with ILU preconditioner
% Inputs: A       : square matrix (size N x N),
%         b       : right hand side column vector (size N x 1),
%         x_guess : initial guess (of size b),
%         tol     : convergence tolerance
%         max_iter: maximum number of iterations
%         omega   : relaxation parameter (for Richardson update)
%         beta    : extrapolation parameter (for Anderson update)
%         m       : Anderson history, no. of previous iterations to be considered in extrapolation
%         p       : Perform Anderson extrapolation at every p th iteration
% Output: x       : solution vector
 
N=length(b);
DX=zeros(N,m);
DF=zeros(N,m);
nb = 1/norm(b) ;

x_prev = x_guess;
relres = tol+1;
count = 1;
while count<=max_iter && relres > tol
    
    res1 = (b-A*x_prev) ;
    res = U\(L\(res1)); 
    
    % STORE HISTORY
    if count>1
        DX(:,mod(count-2,m)+1) = x_prev-Xold;
        DF(:,mod(count-2,m)+1) = res-Fold;
    end
    Xold = x_prev;
    Fold = res;
    
    % UPDATE ITERATE
    if rem(count,p)~=0   % RICHARDSON UPDATE   
        x_new = x_prev + omega*res;      
    else % ANDERSON UPDATE, apply every p iters
        x_new = x_prev + beta*res - (DX + beta*DF)*(pinv(DF'*DF)*(DF'*res));
        relres = norm(res1)/nb;
    end
    
    x = x_prev;
    x_prev = x_new;
    count = count + 1;
end


end
