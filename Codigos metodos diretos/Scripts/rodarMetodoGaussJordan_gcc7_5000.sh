#!/bin/bash
echo "Método de Gauss-Jordan"
gcc-7 --version >> metodoGaussJordan_gcc7.txt
gcc-7 MetodoGaussJordan.c -o MetodoGaussJordan -lm
echo "N = 5000 - padrao" >> metodoGaussJordan_gcc7.txt
echo "N = 5000 - padrao"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_gcc7.txt
done
gcc-7 MetodoGaussJordan.c -o MetodoGaussJordan -lm -O
echo "N = 5000 - O" >> metodoGaussJordan_gcc7.txt
echo "N = 5000 - O"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_gcc7.txt
done
gcc-7 MetodoGaussJordan.c -o MetodoGaussJordan -lm -O1
echo "N = 5000 - O1" >> metodoGaussJordan_gcc7.txt
echo "N = 5000 - O1"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_gcc7.txt
done
gcc-7 MetodoGaussJordan.c -o MetodoGaussJordan -lm -O2
echo "N = 5000 - O2" >> metodoGaussJordan_gcc7.txt
echo "N = 5000 - O2"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_gcc7.txt
done
gcc-7 MetodoGaussJordan.c -o MetodoGaussJordan -lm -O3
echo "N = 5000 - O3" >> metodoGaussJordan_gcc7.txt
echo "N = 5000 - O3"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_gcc7.txt
done
gcc-7 MetodoGaussJordan.c -o MetodoGaussJordan -lm -Ofast
echo "N = 5000 - Ofast" >> metodoGaussJordan_gcc7.txt
echo "N = 5000 - Ofast"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_gcc7.txt
done
gcc-7 MetodoGaussJordan.c -o MetodoGaussJordan -lm -Og
echo "N = 5000 - Og" >> metodoGaussJordan_gcc7.txt
echo "N = 5000 - Og"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_gcc7.txt
done
gcc-7 MetodoGaussJordan.c -o MetodoGaussJordan -lm -Os
echo "N = 5000 - Os" >> metodoGaussJordan_gcc7.txt
echo "N = 5000 - Os"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_gcc7.txt
done
