#!/bin/bash
echo "Metodo de Gauss Jordan"
gcc --version >> metodoGaussJordan_resultados.txt
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm
echo "N = 5000 - padrao" >> metodoGaussJordan_resultados.txt
echo "N = 5000 - padrao"
./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_resultados.txt
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -O
echo "N = 5000 - O" >> metodoGaussJordan_resultados.txt
echo "N = 5000 - O"
./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_resultados.txt
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -O1
echo "N = 5000 - O1" >> metodoGaussJordan_resultados.txt
echo "N = 5000 - O1"
./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_resultados.txt
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -O2
echo "N = 5000 - O2" >> metodoGaussJordan_resultados.txt
echo "N = 5000 - O2"
./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_resultados.txt
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -O3
echo "N = 5000 - O3" >> metodoGaussJordan_resultados.txt
echo "N = 5000 - O3"
./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_resultados.txt
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -Ofast
echo "N = 5000 - Ofast" >> metodoGaussJordan_resultados.txt
echo "N = 5000 - Ofast"
./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_resultados.txt
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -Og
echo "N = 5000 - Og" >> metodoGaussJordan_resultados.txt
echo "N = 5000 - Og"
./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_resultados.txt
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -Os
echo "N = 5000 - Os" >> metodoGaussJordan_resultados.txt
echo "N = 5000 - Os"
./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan_resultados.txt
