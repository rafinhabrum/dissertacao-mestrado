#!/bin/bash
echo "Decomposicao Cholesky"
gcc --version >> decomposicaoCholesky.txt
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm
echo "N = 5000" >> decomposicaoCholesky.txt
echo "N = 5000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky.txt
done
echo "N = 10000" >> decomposicaoCholesky.txt
echo "N = 10000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky < matriz10000x10000.dat >> decomposicaoCholesky.txt
done
echo "N = 15000" >> decomposicaoCholesky.txt
echo "N = 15000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky < matriz15000x15000.dat >> decomposicaoCholesky.txt
done
echo "N = 20000" >> decomposicaoCholesky.txt
echo "N = 20000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky < matriz20000x20000.dat >> decomposicaoCholesky.txt
done
