#!/bin/bash
echo "Decomposicao QR"
gcc --version >> decomposicaoQRGramSchmidt.txt
gcc DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm
echo "N = 5000" >> decomposicaoQRGramSchmidt.txt
echo "N = 5000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt.txt
done
echo "N = 10000" >> decomposicaoQRGramSchmidt.txt
echo "N = 10000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoQRGramSchmidt < matriz10000x10000.dat >> decomposicaoQRGramSchmidt.txt
done
echo "N = 15000" >> decomposicaoQRGramSchmidt.txt
echo "N = 15000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoQRGramSchmidt < matriz15000x15000.dat >> decomposicaoQRGramSchmidt.txt
done
echo "N = 20000" >> decomposicaoQRGramSchmidt.txt
echo "N = 20000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoQRGramSchmidt < matriz20000x20000.dat >> decomposicaoQRGramSchmidt.txt
done
