#!/bin/bash
echo "Eliminação Gaussiana"
gcc --version >> eliminacaoGaussiana.txt
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm
echo "N = 5000 - padrao" >> eliminacaoGaussiana.txt
echo "N = 5000 - padrao"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana.txt
done
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -O
echo "N = 5000 - O" >> eliminacaoGaussiana.txt
echo "N = 5000 - O"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana.txt
done
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -O1
echo "N = 5000 - O1" >> eliminacaoGaussiana.txt
echo "N = 5000 - O1"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana.txt
done
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -O2
echo "N = 5000 - O2" >> eliminacaoGaussiana.txt
echo "N = 5000 - O2"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana.txt
done
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -O3
echo "N = 5000 - O3" >> eliminacaoGaussiana.txt
echo "N = 5000 - O3"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana.txt
done
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -Ofast
echo "N = 5000 - Ofast" >> eliminacaoGaussiana.txt
echo "N = 5000 - Ofast"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana.txt
done
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -Og
echo "N = 5000 - Og" >> eliminacaoGaussiana.txt
echo "N = 5000 - Og"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana.txt
done
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -Os
echo "N = 5000 - Os" >> eliminacaoGaussiana.txt
echo "N = 5000 - Os"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana.txt
done
