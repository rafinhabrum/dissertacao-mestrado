#!/bin/bash
echo "Decomposicao LDU"
gcc --version >> decomposicaoLDU_resultados.txt
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm
echo "N = 5000 - padrao" >> decomposicaoLDU_resultados.txt
echo "N = 5000 - padrao"
./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU_resultados.txt
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -O
echo "N = 5000 - O" >> decomposicaoLDU_resultados.txt
echo "N = 5000 - O"
./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU_resultados.txt
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -O1
echo "N = 5000 - O1" >> decomposicaoLDU_resultados.txt
echo "N = 5000 - O1"
./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU_resultados.txt
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -O2
echo "N = 5000 - O2" >> decomposicaoLDU_resultados.txt
echo "N = 5000 - O2"
./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU_resultados.txt
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -O3
echo "N = 5000 - O3" >> decomposicaoLDU_resultados.txt
echo "N = 5000 - O3"
./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU_resultados.txt
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -Ofast
echo "N = 5000 - Ofast" >> decomposicaoLDU_resultados.txt
echo "N = 5000 - Ofast"
./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU_resultados.txt
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -Og
echo "N = 5000 - Og" >> decomposicaoLDU_resultados.txt
echo "N = 5000 - Og"
./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU_resultados.txt
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -Os
echo "N = 5000 - Os" >> decomposicaoLDU_resultados.txt
echo "N = 5000 - Os"
./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU_resultados.txt
