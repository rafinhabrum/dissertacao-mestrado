#!/bin/bash
echo "Decomposicao QR"
gcc --version >> decomposicaoQRGramSchmidt.txt
gcc DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm
echo "N = 5000 - padrao" >> decomposicaoQRGramSchmidt.txt
echo "N = 5000 - padrao"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt.txt
done
gcc DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -O
echo "N = 5000 - O" >> decomposicaoQRGramSchmidt.txt
echo "N = 5000 - O"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt.txt
done
gcc DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -O1
echo "N = 5000 - O1" >> decomposicaoQRGramSchmidt.txt
echo "N = 5000 - O1"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt.txt
done
gcc DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -O2
echo "N = 5000 - O2" >> decomposicaoQRGramSchmidt.txt
echo "N = 5000 - O2"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt.txt
done
gcc DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -O3
echo "N = 5000 - O3" >> decomposicaoQRGramSchmidt.txt
echo "N = 5000 - O3"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt.txt
done
gcc DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -Ofast
echo "N = 5000 - Ofast" >> decomposicaoQRGramSchmidt.txt
echo "N = 5000 - Ofast"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt.txt
done
gcc DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -Og
echo "N = 5000 - Og" >> decomposicaoQRGramSchmidt.txt
echo "N = 5000 - Og"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt.txt
done
gcc DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -Os
echo "N = 5000 - Os" >> decomposicaoQRGramSchmidt.txt
echo "N = 5000 - Os"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt.txt
done
