#!/bin/bash
echo "Eliminacao Gaussiana"
gcc --version >> eliminacaoGaussiana.txt
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm
echo "N = 5000" >> eliminacaoGaussiana.txt
echo "N = 5000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana.txt
done
echo "N = 10000" >> eliminacaoGaussiana.txt
echo "N = 10000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./EliminacaoGaussiana < matriz10000x10000.dat >> eliminacaoGaussiana.txt
done
echo "N = 15000" >> eliminacaoGaussiana.txt
echo "N = 15000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./EliminacaoGaussiana < matriz15000x15000.dat >> eliminacaoGaussiana.txt
done
echo "N = 20000" >> eliminacaoGaussiana.txt
echo "N = 20000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./EliminacaoGaussiana < matriz20000x20000.dat >> eliminacaoGaussiana.txt
done
