#!/bin/bash
echo "Eliminacao Gaussiana"
gcc-7 --version >> eliminacaoGaussiana_gcc7_resultados.txt
gcc-7 EliminacaoGaussiana.c -o EliminacaoGaussiana -lm
echo "N = 5000 - padrao" >> eliminacaoGaussiana_gcc7_resultados.txt
echo "N = 5000 - padrao"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_gcc7_resultados.txt
gcc-7 EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -O
echo "N = 5000 - O" >> eliminacaoGaussiana_gcc7_resultados.txt
echo "N = 5000 - O"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_gcc7_resultados.txt
gcc-7 EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -O1
echo "N = 5000 - O1" >> eliminacaoGaussiana_gcc7_resultados.txt
echo "N = 5000 - O1"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_gcc7_resultados.txt
gcc-7 EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -O2
echo "N = 5000 - O2" >> eliminacaoGaussiana_gcc7_resultados.txt
echo "N = 5000 - O2"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_gcc7_resultados.txt
gcc-7 EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -O3
echo "N = 5000 - O3" >> eliminacaoGaussiana_gcc7_resultados.txt
echo "N = 5000 - O3"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_gcc7_resultados.txt
gcc-7 EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -Ofast
echo "N = 5000 - Ofast" >> eliminacaoGaussiana_gcc7_resultados.txt
echo "N = 5000 - Ofast"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_gcc7_resultados.txt
gcc-7 EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -Og
echo "N = 5000 - Og" >> eliminacaoGaussiana_gcc7_resultados.txt
echo "N = 5000 - Og"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_gcc7_resultados.txt
gcc-7 EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -Os
echo "N = 5000 - Os" >> eliminacaoGaussiana_gcc7_resultados.txt
echo "N = 5000 - Os"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_gcc7_resultados.txt
