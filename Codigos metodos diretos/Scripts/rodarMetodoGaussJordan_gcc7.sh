#!/bin/bash
echo "Metodo Gauss Jordan"
gcc-7 --version >> metodoGaussJordan_gcc7.txt
gcc-7 MetodoGaussJordan.c -o MetodoGaussJordan_gcc7 -lm
echo "N = 5000" >> metodoGaussJordan_gcc7.txt
echo "N = 5000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan_gcc7 < matriz5000x5000.dat >> metodoGaussJordan_gcc7.txt
done
echo "N = 10000" >> metodoGaussJordan_gcc7.txt
echo "N = 10000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan_gcc7 < matriz10000x10000.dat >> metodoGaussJordan_gcc7.txt
done
echo "N = 15000" >> metodoGaussJordan_gcc7.txt
echo "N = 15000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan_gcc7 < matriz15000x15000.dat >> metodoGaussJordan_gcc7.txt
done
echo "N = 20000" >> metodoGaussJordan_gcc7.txt
echo "N = 20000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan_gcc7 < matriz20000x20000.dat >> metodoGaussJordan_gcc7.txt
done
