#!/bin/bash
echo "Eliminacao Gaussiana"
gcc --version >> eliminacaoGaussiana_resultados.txt
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm
echo "N = 5000 - padrao" >> eliminacaoGaussiana_resultados.txt
echo "N = 5000 - padrao"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_resultados.txt
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -O
echo "N = 5000 - O" >> eliminacaoGaussiana_resultados.txt
echo "N = 5000 - O"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_resultados.txt
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -O1
echo "N = 5000 - O1" >> eliminacaoGaussiana_resultados.txt
echo "N = 5000 - O1"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_resultados.txt
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -O2
echo "N = 5000 - O2" >> eliminacaoGaussiana_resultados.txt
echo "N = 5000 - O2"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_resultados.txt
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -O3
echo "N = 5000 - O3" >> eliminacaoGaussiana_resultados.txt
echo "N = 5000 - O3"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_resultados.txt
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -Ofast
echo "N = 5000 - Ofast" >> eliminacaoGaussiana_resultados.txt
echo "N = 5000 - Ofast"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_resultados.txt
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -Og
echo "N = 5000 - Og" >> eliminacaoGaussiana_resultados.txt
echo "N = 5000 - Og"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_resultados.txt
gcc EliminacaoGaussiana.c -o EliminacaoGaussiana -lm -Os
echo "N = 5000 - Os" >> eliminacaoGaussiana_resultados.txt
echo "N = 5000 - Os"
./EliminacaoGaussiana < matriz5000x5000.dat >> eliminacaoGaussiana_resultados.txt
