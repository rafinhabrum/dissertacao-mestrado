#!/bin/bash
echo "Decomposicao QR"
gcc-7 --version >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
gcc-7 DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm
echo "N = 5000 - padrao" >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
echo "N = 5000 - padrao"
./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
gcc-7 DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -O
echo "N = 5000 - O" >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
echo "N = 5000 - O"
./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
gcc-7 DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -O1
echo "N = 5000 - O1" >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
echo "N = 5000 - O1"
./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
gcc-7 DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -O2
echo "N = 5000 - O2" >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
echo "N = 5000 - O2"
./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
gcc-7 DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -O3
echo "N = 5000 - O3" >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
echo "N = 5000 - O3"
./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
gcc-7 DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -Ofast
echo "N = 5000 - Ofast" >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
echo "N = 5000 - Ofast"
./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
gcc-7 DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -Og
echo "N = 5000 - Og" >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
echo "N = 5000 - Og"
./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
gcc-7 DecomposicaoQRGramSchmidt.c -o DecomposicaoQRGramSchmidt -lm -Os
echo "N = 5000 - Os" >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
echo "N = 5000 - Os"
./DecomposicaoQRGramSchmidt < matriz5000x5000.dat >> decomposicaoQRGramSchmidt_gcc7_resultados.txt
