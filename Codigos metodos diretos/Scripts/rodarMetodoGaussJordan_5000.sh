#!/bin/bash
echo "Método de Gauss-Jordan"
gcc --version >> metodoGaussJordan.txt
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm
echo "N = 5000 - padrao" >> metodoGaussJordan.txt
echo "N = 5000 - padrao"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan.txt
done
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -O
echo "N = 5000 - O" >> metodoGaussJordan.txt
echo "N = 5000 - O"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan.txt
done
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -O1
echo "N = 5000 - O1" >> metodoGaussJordan.txt
echo "N = 5000 - O1"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan.txt
done
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -O2
echo "N = 5000 - O2" >> metodoGaussJordan.txt
echo "N = 5000 - O2"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan.txt
done
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -O3
echo "N = 5000 - O3" >> metodoGaussJordan.txt
echo "N = 5000 - O3"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan.txt
done
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -Ofast
echo "N = 5000 - Ofast" >> metodoGaussJordan.txt
echo "N = 5000 - Ofast"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan.txt
done
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -Og
echo "N = 5000 - Og" >> metodoGaussJordan.txt
echo "N = 5000 - Og"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan.txt
done
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm -Os
echo "N = 5000 - Os" >> metodoGaussJordan.txt
echo "N = 5000 - Os"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan.txt
done
