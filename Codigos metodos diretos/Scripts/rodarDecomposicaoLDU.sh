#!/bin/bash
echo "Decomposicao LDU"
gcc --version >> decomposicaoLDU.txt
gcc DecomposicaoLDU.c -o DecomposicaoLDU
echo "N = 5000" >> decomposicaoLDU.txt
echo "N = 5000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU.txt
done
echo "N = 10000" >> decomposicaoLDU.txt
echo "N = 10000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLDU < matriz10000x10000.dat >> decomposicaoLDU.txt
done
echo "N = 15000" >> decomposicaoLDU.txt
echo "N = 15000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLDU < matriz15000x15000.dat >> decomposicaoLDU.txt
done
echo "N = 20000" >> decomposicaoLDU.txt
echo "N = 20000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLDU < matriz20000x20000.dat >> decomposicaoLDU.txt
done
