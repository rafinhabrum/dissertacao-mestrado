#!/bin/bash
echo "Decomposicao Cholesky"
gcc-7 --version >> decomposicaoCholesky_gcc7.txt
gcc-7 DecomposicaoCholesky.c -o DecomposicaoCholesky_gcc7 -lm
echo "N = 5000" >> decomposicaoCholesky_gcc7.txt
echo "N = 5000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky_gcc7 < matriz5000x5000.dat >> decomposicaoCholesky_gcc7.txt
done
echo "N = 10000" >> decomposicaoCholesky_gcc7.txt
echo "N = 10000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky_gcc7 < matriz10000x10000.dat >> decomposicaoCholesky_gcc7.txt
done
echo "N = 15000" >> decomposicaoCholesky_gcc7.txt
echo "N = 15000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky_gcc7 < matriz15000x15000.dat >> decomposicaoCholesky_gcc7.txt
done
echo "N = 20000" >> decomposicaoCholesky_gcc7.txt
echo "N = 20000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky_gcc7 < matriz20000x20000.dat >> decomposicaoCholesky_gcc7.txt
done
