#!/bin/bash
echo "Decomposicao Cholesky"
gcc --version >> decomposicaoCholesky_resultados.txt
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm
echo "N = 5000 - padrao" >> decomposicaoCholesky_resultados.txt
echo "N = 5000 - padrao"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_resultados.txt
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -O
echo "N = 5000 - O" >> decomposicaoCholesky_resultados.txt
echo "N = 5000 - O"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_resultados.txt
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -O1
echo "N = 5000 - O1" >> decomposicaoCholesky_resultados.txt
echo "N = 5000 - O1"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_resultados.txt
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -O2
echo "N = 5000 - O2" >> decomposicaoCholesky_resultados.txt
echo "N = 5000 - O2"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_resultados.txt
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -O3
echo "N = 5000 - O3" >> decomposicaoCholesky_resultados.txt
echo "N = 5000 - O3"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_resultados.txt
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -Ofast
echo "N = 5000 - Ofast" >> decomposicaoCholesky_resultados.txt
echo "N = 5000 - Ofast"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_resultados.txt
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -Og
echo "N = 5000 - Og" >> decomposicaoCholesky_resultados.txt
echo "N = 5000 - Og"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_resultados.txt
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -Os
echo "N = 5000 - Os" >> decomposicaoCholesky_resultados.txt
echo "N = 5000 - Os"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_resultados.txt
