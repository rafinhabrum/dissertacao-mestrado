#!/bin/bash
echo "Decomposicao Cholesky"
gcc --version >> decomposicaoCholesky.txt
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm
echo "N = 5000 - padrao" >> decomposicaoCholesky.txt
echo "N = 5000 - padrao"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky.txt
done
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -O
echo "N = 5000 - O" >> decomposicaoCholesky.txt
echo "N = 5000 - O"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky.txt
done
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -O1
echo "N = 5000 - O1" >> decomposicaoCholesky.txt
echo "N = 5000 - O1"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky.txt
done
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -O2
echo "N = 5000 - O2" >> decomposicaoCholesky.txt
echo "N = 5000 - O2"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky.txt
done
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -O3
echo "N = 5000 - O3" >> decomposicaoCholesky.txt
echo "N = 5000 - O3"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky.txt
done
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -Ofast
echo "N = 5000 - Ofast" >> decomposicaoCholesky.txt
echo "N = 5000 - Ofast"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky.txt
done
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -Og
echo "N = 5000 - Og" >> decomposicaoCholesky.txt
echo "N = 5000 - Og"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky.txt
done
gcc DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -Os
echo "N = 5000 - Os" >> decomposicaoCholesky.txt
echo "N = 5000 - Os"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky.txt
done
