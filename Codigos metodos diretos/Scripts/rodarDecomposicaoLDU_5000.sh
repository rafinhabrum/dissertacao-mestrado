#!/bin/bash
echo "Decomposicao LDU"
gcc --version >> decomposicaoLDU.txt
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm
echo "N = 5000 - padrao" >> decomposicaoLDU.txt
echo "N = 5000 - padrao"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU.txt
done
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -O
echo "N = 5000 - O" >> decomposicaoLDU.txt
echo "N = 5000 - O"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU.txt
done
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -O1
echo "N = 5000 - O1" >> decomposicaoLDU.txt
echo "N = 5000 - O1"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU.txt
done
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -O2
echo "N = 5000 - O2" >> decomposicaoLDU.txt
echo "N = 5000 - O2"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU.txt
done
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -O3
echo "N = 5000 - O3" >> decomposicaoLDU.txt
echo "N = 5000 - O3"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU.txt
done
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -Ofast
echo "N = 5000 - Ofast" >> decomposicaoLDU.txt
echo "N = 5000 - Ofast"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU.txt
done
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -Og
echo "N = 5000 - Og" >> decomposicaoLDU.txt
echo "N = 5000 - Og"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU.txt
done
gcc DecomposicaoLDU.c -o DecomposicaoLDU -lm -Os
echo "N = 5000 - Os" >> decomposicaoLDU.txt
echo "N = 5000 - Os"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLDU < matriz5000x5000.dat >> decomposicaoLDU.txt
done
