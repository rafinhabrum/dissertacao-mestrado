#!/bin/bash
echo "Decomposicao LU"
gcc-7 --version >> decomposicaoLU_gcc7.txt
gcc-7 DecomposicaoLU.c -o DecomposicaoLU -lm
echo "N = 5000 - padrao" >> decomposicaoLU_gcc7.txt
echo "N = 5000 - padrao"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU < matriz5000x5000.dat >> decomposicaoLU_gcc7.txt
done
gcc-7 DecomposicaoLU.c -o DecomposicaoLU -lm -O
echo "N = 5000 - O" >> decomposicaoLU_gcc7.txt
echo "N = 5000 - O"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU < matriz5000x5000.dat >> decomposicaoLU_gcc7.txt
done
gcc-7 DecomposicaoLU.c -o DecomposicaoLU -lm -O1
echo "N = 5000 - O1" >> decomposicaoLU_gcc7.txt
echo "N = 5000 - O1"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU < matriz5000x5000.dat >> decomposicaoLU_gcc7.txt
done
gcc-7 DecomposicaoLU.c -o DecomposicaoLU -lm -O2
echo "N = 5000 - O2" >> decomposicaoLU_gcc7.txt
echo "N = 5000 - O2"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU < matriz5000x5000.dat >> decomposicaoLU_gcc7.txt
done
gcc-7 DecomposicaoLU.c -o DecomposicaoLU -lm -O3
echo "N = 5000 - O3" >> decomposicaoLU_gcc7.txt
echo "N = 5000 - O3"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU < matriz5000x5000.dat >> decomposicaoLU_gcc7.txt
done
gcc-7 DecomposicaoLU.c -o DecomposicaoLU -lm -Ofast
echo "N = 5000 - Ofast" >> decomposicaoLU_gcc7.txt
echo "N = 5000 - Ofast"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU < matriz5000x5000.dat >> decomposicaoLU_gcc7.txt
done
gcc-7 DecomposicaoLU.c -o DecomposicaoLU -lm -Og
echo "N = 5000 - Og" >> decomposicaoLU_gcc7.txt
echo "N = 5000 - Og"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU < matriz5000x5000.dat >> decomposicaoLU_gcc7.txt
done
gcc-7 DecomposicaoLU.c -o DecomposicaoLU -lm -Os
echo "N = 5000 - Os" >> decomposicaoLU_gcc7.txt
echo "N = 5000 - Os"
COUNTER=1
while [  $COUNTER -lt 6 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU < matriz5000x5000.dat >> decomposicaoLU_gcc7.txt
done
