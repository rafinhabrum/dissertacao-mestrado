#!/bin/bash
echo "Decomposicao LU"
gcc --version >> decomposicaoLU.txt
gcc DecomposicaoLU.c -o DecomposicaoLU
echo "N = 5000" >> decomposicaoLU.txt
echo "N = 5000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU < matriz5000x5000.dat >> decomposicaoLU.txt
done
echo "N = 10000" >> decomposicaoLU.txt
echo "N = 10000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU < matriz10000x10000.dat >> decomposicaoLU.txt
done
echo "N = 15000" >> decomposicaoLU.txt
echo "N = 15000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU < matriz15000x15000.dat >> decomposicaoLU.txt
done
echo "N = 20000" >> decomposicaoLU.txt
echo "N = 20000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU < matriz20000x20000.dat >> decomposicaoLU.txt
done
