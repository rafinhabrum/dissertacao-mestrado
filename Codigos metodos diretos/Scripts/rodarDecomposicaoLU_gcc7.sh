#!/bin/bash
echo "Decomposicao LU"
gcc-7 --version >> decomposicaoLU_gcc7.txt
gcc-7 DecomposicaoLU.c -o DecomposicaoLU_gcc7
echo "N = 5000" >> decomposicaoLU_gcc7.txt
echo "N = 5000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU_gcc7 < matriz5000x5000.dat >> decomposicaoLU_gcc7.txt
done
echo "N = 10000" >> decomposicaoLU_gcc7.txt
echo "N = 10000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU_gcc7 < matriz10000x10000.dat >> decomposicaoLU_gcc7.txt
done
echo "N = 15000" >> decomposicaoLU_gcc7.txt
echo "N = 15000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU_gcc7 < matriz15000x15000.dat >> decomposicaoLU_gcc7.txt
done
echo "N = 20000" >> decomposicaoLU_gcc7.txt
echo "N = 20000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./DecomposicaoLU_gcc7 < matriz20000x20000.dat >> decomposicaoLU_gcc7.txt
done
