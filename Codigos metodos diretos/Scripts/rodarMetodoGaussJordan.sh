#!/bin/bash
echo "Metodo Gauss Jordan"
gcc --version >> metodoGaussJordan.txt
gcc MetodoGaussJordan.c -o MetodoGaussJordan -lm
echo "N = 5000" >> metodoGaussJordan.txt
echo "N = 5000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz5000x5000.dat >> metodoGaussJordan.txt
done
echo "N = 10000" >> metodoGaussJordan.txt
echo "N = 10000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz10000x10000.dat >> metodoGaussJordan.txt
done
echo "N = 15000" >> metodoGaussJordan.txt
echo "N = 15000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz15000x15000.dat >> metodoGaussJordan.txt
done
echo "N = 20000" >> metodoGaussJordan.txt
echo "N = 20000"
COUNTER=1
while [  $COUNTER -lt 11 ]; do
	echo "Counter = $COUNTER"
	COUNTER=$((COUNTER+1))
	./MetodoGaussJordan < matriz20000x20000.dat >> metodoGaussJordan.txt
done
