#!/bin/bash
echo "Decomposicao Cholesky"
gcc-7 --version >> decomposicaoCholesky_gcc7_resultados.txt
gcc-7 DecomposicaoCholesky.c -o DecomposicaoCholesky -lm
echo "N = 5000 - padrao" >> decomposicaoCholesky_gcc7_resultados.txt
echo "N = 5000 - padrao"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_gcc7_resultados.txt
gcc-7 DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -O
echo "N = 5000 - O" >> decomposicaoCholesky_gcc7_resultados.txt
echo "N = 5000 - O"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_gcc7_resultados.txt
gcc-7 DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -O1
echo "N = 5000 - O1" >> decomposicaoCholesky_gcc7_resultados.txt
echo "N = 5000 - O1"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_gcc7_resultados.txt
gcc-7 DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -O2
echo "N = 5000 - O2" >> decomposicaoCholesky_gcc7_resultados.txt
echo "N = 5000 - O2"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_gcc7_resultados.txt
gcc-7 DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -O3
echo "N = 5000 - O3" >> decomposicaoCholesky_gcc7_resultados.txt
echo "N = 5000 - O3"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_gcc7_resultados.txt
gcc-7 DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -Ofast
echo "N = 5000 - Ofast" >> decomposicaoCholesky_gcc7_resultados.txt
echo "N = 5000 - Ofast"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_gcc7_resultados.txt
gcc-7 DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -Og
echo "N = 5000 - Og" >> decomposicaoCholesky_gcc7_resultados.txt
echo "N = 5000 - Og"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_gcc7_resultados.txt
gcc-7 DecomposicaoCholesky.c -o DecomposicaoCholesky -lm -Os
echo "N = 5000 - Os" >> decomposicaoCholesky_gcc7_resultados.txt
echo "N = 5000 - Os"
./DecomposicaoCholesky < matriz5000x5000.dat >> decomposicaoCholesky_gcc7_resultados.txt
