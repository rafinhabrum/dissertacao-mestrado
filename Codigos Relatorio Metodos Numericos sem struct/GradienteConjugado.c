/*
Matriz que converge:
4.00x1 + 0.24x2 - 0.08x3 = 8.00
0.09x1 + 3.00x2 - 0.15x3 = 9.00
0.04x1 - 0.08x2 + 4.00x3 = 20.00
*/

#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#define TOL 0.0000000001
#define TRUE -1
#define FALSE 0

typedef struct m
{
    int m, n; //matriz mxn
    double dados[200][200];
} matriz;

typedef struct timeval sTime;

matriz multiplicacaoMatrizes(matriz A, matriz B)
{
    int i, j, k;
    matriz M;
    M.m = A.m;
    M.n = B.n;

    for(i = 0; i < M.m; i++)
    {
        for(j = 0; j < M.n; j++)
        {
            M.dados[i][j] = 0;
            for(k = 0; k < A.n; k++)
            {
                M.dados[i][j] += (A.dados[i][k]*B.dados[k][j]);
            }
        }
    }

    return M;
}

void imprimirMatriz(char *nome, matriz A)
{
	int i, j;
	printf("%s\n", nome);
	for(i = 0; i < A.m; i++)
	{
		for(j = 0; j < A.n; j++)
		{
			printf("%lf ", A.dados[i][j]);
		}
		printf("\n");
	}
}

double calcularProdutoInterno(matriz x, matriz y)
{
    int i;
    double res = 0;
    for(i = 0; i < x.m; i++)
    {
        res += (x.dados[i][0] * y.dados[i][0]);
    }
    return res;
}

double calcularTempoGasto(sTime tI, sTime tF)
{
    double result = (tF.tv_sec - tI.tv_sec);
    result += ((tF.tv_usec - tI.tv_usec) / 1000000.0 ); //us para segundos;
    return result;
}

int main()
{
    int i, j, k;
    int sign, retScan;
    matriz A, x, B, r, p, x_prox, Ap, r_prox, dif;
    double q, alpha;
    double aux;
    sTime t1, t2;
    FILE *arquivo;

    gettimeofday(&t1, NULL);

    //retScan = scanf("%d", &(A.m));
    arquivo = fopen("matriz2000x2000.dat", "r");
	if(!arquivo)
    {
        return 0;
    }
	retScan = fscanf(arquivo, "%d", &(A.m));

    A.n = A.m;
    x.m = B.m = A.m;
    x.n = B.n = 1;
    for(i = 0; i < A.m; i++)
    {
        for(j = 0; j < A.n; j++)
        {
            retScan = fscanf(arquivo, "%lf", &(A.dados[i][j]));
        }
        retScan = fscanf(arquivo, "%lf", &B.dados[i][0]);
        x.dados[i][0] = 0;
    }

    fclose(arquivo);


    //imprimirMatriz("Matriz A", A);

    //r0 = A*x0 + b
    //p1 = -r0

    r = multiplicacaoMatrizes(A, x);
    r_prox.m = p.m = r.m;
    r_prox.n = p.n = r.n;
    for(i = 0; i < r.m; i++)
    {
        r.dados[i][0] += B.dados[i][0];
        p.dados[i][0] = -r.dados[i][0];
    }

    //imprimirMatriz("Matriz r", r);
    //imprimirMatriz("Matriz p", p);

    //q1 = <r0, r0> / <Ar0, r0>
    q = calcularProdutoInterno(r,r)/calcularProdutoInterno(multiplicacaoMatrizes(A, r), r);

    //printf("q = %lf\n", q);

    //x_prox = x + q*p
    x_prox.m = x.m;
    x_prox.n = x.n;
    for(i = 0; i < x.m; i++)
    {
        x_prox.dados[i][0] = x.dados[i][0] - q*p.dados[i][0];
    }

    //imprimirMatriz("Matriz x_prox", x_prox);

    //r_prox = r + q*A*p

    Ap = multiplicacaoMatrizes(A, p);
    for(i = 0; i < r.m; i++)
    {
        r_prox.dados[i][0] = r.dados[i][0] + q*Ap.dados[i][0];
    }

    //imprimirMatriz("Matriz r_prox", r_prox);

    x = x_prox;

    for(k = 2; k < 1000000; k++)
    {
        //printf("k = %d\n", k);
        //alpha = <r_prox, r_prox>/<r, r>
        alpha = calcularProdutoInterno(r_prox, r_prox)/calcularProdutoInterno(r,r);
        //printf("alpha = %lf\n", alpha);
        r = r_prox;

        //p = -r + alpha*p
        for(i = 0; i < r.m; i++)
        {
            p.dados[i][0] = -r.dados[i][0] + alpha*p.dados[i][0];
        }

        //q1 = <r, r> / <Ap, p>
        Ap = multiplicacaoMatrizes(A, p);
        q = -calcularProdutoInterno(r,r)/calcularProdutoInterno(Ap, p);

        //x_prox = x + q*p
        for(i = 0; i < x.m; i++)
        {
            x_prox.dados[i][0] = x.dados[i][0] + q*p.dados[i][0];
        }
        //imprimirMatriz("Matriz x_prox", x_prox);

        //r_prox = r + q*A*p
        for(i = 0; i < r.m; i++)
        {
            r_prox.dados[i][0] = r.dados[i][0] - q*Ap.dados[i][0];
        }
        //imprimirMatriz("Matriz r_prox", r_prox);

        sign = 0;

        for(i = 0; i < x.m; ++i)
        {
            aux = x_prox.dados[i][0] - x.dados[i][0];
            //printf("i: %d\nx_prox-x = %lf\n", i, aux);
            //if((aux > TOL) || (aux < -TOL))
            if(!sign)
            {
                if(fabs(aux) > TOL)
                {
                    sign = 1;
                }
            }
        }

        x = x_prox;

        if(!sign)
        {
            break;
        }
    }

    gettimeofday(&t2, NULL);
    //printf("K = %d\n", k);
    //imprimirMatriz("Vetor x final", x);
    printf("\ntime = %lf s\n\n", calcularTempoGasto(t1, t2));
    return 0;
}
