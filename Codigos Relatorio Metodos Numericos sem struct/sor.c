/*
Matriz que converge:
4.00x1 + 0.24x2 - 0.08x3 = 8.00
0.09x1 + 3.00x2 - 0.15x3 = 9.00
0.04x1 - 0.08x2 + 4.00x3 = 20.00
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sys/time.h>
#define TOL 0.0000000001
#define TRUE -1
#define FALSE 0

typedef struct timeval sTime;

void imprimirMatriz(char *nome, double *A, int tam)
{
	int i, j;
	printf("%s\n", nome);
	for(i = 0; i < tam; i++)
	{
		for(j = 0; j < tam; j++)
		{
			printf("%lf ", A[i*tam + j]);
		}
		printf("\n");
	}
}

void imprimirVetor(char *nome, double *A, int tam)
{
	int i, j;
	printf("%s\n", nome);
	for(i = 0; i < tam; i++)
	{
        printf("%lf\n", A[i]);
	}
}

double calcularTempoGasto(sTime tI, sTime tF)
{
    double result = (tF.tv_sec - tI.tv_sec);
    result += ((tF.tv_usec - tI.tv_usec) / 1000000.0 ); //us para segundos;
    return result;
}

int main()
{
    int i, j, k;
    int tam;
    double *A, *B, *x, *x_prox, *x_gs;
    double *ptAuxA, *ptAuxB, *ptAuxX, *ptAuxXProx, *ptAuxXGS;
    double w = 0.95, aux, soma;
    sTime t1, t2;
    int retScan;
    int sign;
    FILE *arquivo;

    gettimeofday(&t1, NULL);

    //retScan = scanf("%d", &(A.m));
    arquivo = fopen("matriz2000x2000.dat", "r");
	if(!arquivo)
    {
        return 0;
    }
	retScan = fscanf(arquivo, "%d", &(tam));

    A = (double*)malloc(tam*tam*sizeof(double));
    B = (double*)malloc(tam*sizeof(double));
    x = (double*)malloc(tam*sizeof(double));
    x_gs = (double*)malloc(tam*sizeof(double));
    x_prox = (double*)malloc(tam*sizeof(double));

    for(i = 0; i < tam; ++i, ++ptAuxB, ++ptAuxX)
    {
        for(j = 0; j < tam; ++j, ++ptAuxA)
        {
            retScan = fscanf(arquivo, "%lf", (ptAuxA));
        }
        retScan = fscanf(arquivo, "%lf", (ptAuxB));
        *ptAuxX = 0;
    }

    fclose(arquivo);


    //x(k+1) = D^-1*B - D^-1*L*x(k+1) - D^-1*U*x(k)

    for(k = 1; k < 1000000; ++k)
    {
        //printf("k = %d\n", k);
        //imprimirMatriz("Vetor x", x);

        ptAuxXGS = x_gs;
        ptAuxB = B;
        for(i = 0; i < tam; ++i, ++ptAuxX, ++ptAuxXGS, ++ptAuxB)
        {
            //ti
            *ptAuxXGS = *ptAuxB;
            soma = 0;
            ptAuxA = (A+i*tam+i+1);
            ptAuxX = (x+i+1);
            for(j = i+1; j < tam; ++j, ++ptAuxA, ++ptAuxX)
            {
                soma += ((*ptAuxA) * (*ptAuxX));
            }
            *ptAuxXGS -= soma;

            //zi
            soma = 0;
            ptAuxA = (A+i*tam);
            ptAuxX = (x_prox);
            for(j = 0; j < i; ++j, ++ptAuxA, ++ptAuxX)
            {
                soma += ((*ptAuxA) * (*ptAuxX));
            }
            *ptAuxXGS -= soma;

            //x = 1/A[i][i]*x
            *ptAuxXGS /= *ptAuxA;
        }
        //PAREI AQUI
        for(i = 0; i < x_prox.m; ++i)
        {
            x_prox.dados[i][0] = (1-w)*x.dados[i][0] + w*x_gs.dados[i][0];
        }

        sign = 0;

        for(i = 0; i < x.m; ++i)
        {
            aux = x_prox.dados[i][0] - x.dados[i][0];
            //printf("i: %d\nx_prox-x = %lf\n", i, aux);
            //if((aux > TOL) || (aux < -TOL))
            if(!sign)
            {
                if(fabs(aux) > TOL)
                {
                    sign = 1;
                }
            }
        }

        x = x_prox;

        if(!sign)
        {
            break;
        }
    }

    gettimeofday(&t2, NULL);
    //printf("K = %d\n", k);
    //imprimirMatriz("Vetor x final", x);
    printf("\ntime = %lf s\n\n", calcularTempoGasto(t1, t2));

    return 0;
}
